﻿CREATE TABLE [dbo].[ConsumerRedeemedPoint] (
    [ConsumerPointRedeemId] INT IDENTITY (1, 1) NOT NULL,
    [ConsumerId]            INT NOT NULL,
    [MerchantId]            INT NOT NULL,
    [Points]                INT NOT NULL,
    [PointId]               INT NOT NULL,
    CONSTRAINT [PK__Consumer__D74A290E3A5860A6] PRIMARY KEY CLUSTERED ([ConsumerPointRedeemId] ASC),
    CONSTRAINT [FK_ConsumerRedeemedPoint_Consumer] FOREIGN KEY ([ConsumerId]) REFERENCES [dbo].[Consumer] ([ConsumerId]),
    CONSTRAINT [FK_ConsumerRedeemedPoint_Merchant] FOREIGN KEY ([MerchantId]) REFERENCES [dbo].[Merchant] ([MerchantId]),
    CONSTRAINT [FK_ConsumerRedeemedPoint_MerchantPoint] FOREIGN KEY ([PointId]) REFERENCES [dbo].[MerchantPoint] ([PointId])
);

