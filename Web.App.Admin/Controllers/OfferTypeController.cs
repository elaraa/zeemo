﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Admin.Controllers
{
    public class OfferTypeController : BaseControllerTemplate<PromotionType, PromotionType, int>
    {
        public OfferTypeController()
        {
            ViewNamePlural = "Promotion Types";
            ViewNameSingular = "Promotion Type";

            PropertiesToUpdate = new Expression<Func<PromotionType, object>>[]
                {
                    x=>x.PromotionTypeName
                };
        }
        protected override Expression<Func<PromotionType, bool>> GetOneExpression(int? id) => id.HasValue ? (x => x.PromotionTypeId == id.Value) : (Expression<Func<PromotionType, bool>>)(x => 1 == 0);

    }
}