﻿

using DB.ORM.DB;
using LinqKit;
using Services.Base.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Services.Repository
{
    public abstract class GenericRepo
    {
        public virtual T Add<T>(T entity) where T: class{ return null; }
        public virtual bool Any<T>(Expression<Func<T, bool>> whereClause) where T : class { return false; }
        public virtual T AddOrUpdate<T>(T entity) where T: class { return null; }
        public virtual T GetSingleLocalDS<T>() where T : class { return null; }
        public virtual T GetSingleDS<T>(Expression<Func<T, bool>> whereClause) where T : class { return null; }
        public virtual T GetSingle<T>(Expression<Func<T, bool>> whereClause, bool safe = false, params string[] childrenToInclude) 
            where T : class { return null; }
        public virtual IQueryable<T> GetLocalDS<T>() where T : class { return null; }
        public virtual bool HasLocalDS<T>() where T : class { return false; }
        public virtual int Delete<T>(Expression<Func<T, bool>> predicate) where T : class { return 0; }
        public virtual int DeleteOne<T>(Expression<Func<T, bool>> whereClause) where T : class { return 0; }
        public new virtual Type GetType() => null;
        
    }
    public class GenericRepository<T>: GenericRepo
        where T : class
    {
        public override Type GetType() => typeof(T);
        private bool disposed = false;// to detect redundant calls
        protected DBModel db;

        public GenericRepository(DBModel _db)
        {
            db = _db;
        }

        public GenericRepository()
        {
        }
        public override IQueryable<T> GetLocalDS<T>() {
            var entities = db.Set<T>().Local.AsQueryable();
            return entities;
        }
        public override bool HasLocalDS<T>()
        {
            var res = db.Set<T>().Local.Any();
            return res;
        }
        public override T GetSingleLocalDS<T>()
        {
            var entity = db.Set<T>().Local.FirstOrDefault();
            return entity;
        }

        public override T GetSingleDS<T>(Expression<Func<T, bool>> whereClause)
        {
            var localEntity = db.Set<T>().Local.AsQueryable().FirstOrDefault(whereClause);
            if (localEntity != null)
                return localEntity;
            else
            {
                var entity = db.Set<T>().AsQueryable().FirstOrDefault(whereClause);
                if (entity != null)
                    return entity;
            }
            return null;
        }
        public override T GetSingle<T>(Expression<Func<T, bool>> whereClause, bool safe = false, params string[] childrenToInclude)
        {
            db.Configuration.LazyLoadingEnabled = !safe;
            db.Configuration.ProxyCreationEnabled = !safe;

            var entity = SecureRetrieve(whereClause, childrenToInclude).FirstOrDefault();
            return entity;
        }
        public override int Delete<T>(Expression<Func<T, bool>> predicate)
        {
            SecureRetrieve<T>(predicate).DeleteFromQuery();
            return db.SaveChanges();
        }
        public override int DeleteOne<T>(Expression<Func<T, bool>> whereClause)
        {
            var xEntity = db.Set<T>().Local.AsQueryable().FirstOrDefault(whereClause);
            if(xEntity != null)
                db.Entry(xEntity).State = EntityState.Detached;

            SecureRetrieve<T>(whereClause).DeleteFromQuery();
            return db.SaveChanges();
        }

        public override bool Any<T>(Expression<Func<T, bool>> whereClause)
        {
            var isExist = db.Set<T>().Any(whereClause);
            return isExist;
        }

        public override T Add<T>(T entity)
        {
            entity = db.Set<T>().Add(entity);
            int res = db.SaveChanges();
            return entity;
        }

        public virtual RunResult AddOrUpdate(T e, bool isCreate )
        {
            try
            {
                db.Set<T>().AddOrUpdateExtension(e);
                int res = db.SaveChanges();
                return new RunResult { Succeeded = true, SuccessDesc = "Done Successfully" };
            }
            catch (Exception ex)
            {
                return new RunResult { ErrorDesc = "DB Error" };
            }
            
        }
        public override T AddOrUpdate<T>(T entity)
        {
            try
            {
                db.Set<T>().AddOrUpdateExtension(entity);
                int res = db.SaveChanges();
                return entity;
            }
            catch (Exception e) {
                return null;
            }
        }
        public IQueryable<T> Get<T>(Expression<Func<T, bool>> whereClause = null, bool safe = false, params string[] childrenToInclude)
              where T : class
        {
            db.Configuration.LazyLoadingEnabled = !safe;
            db.Configuration.ProxyCreationEnabled = !safe;

            return SecureRetrieve<T>(whereClause, childrenToInclude);
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> whereClause = null, bool safe = false, params string[] childrenToInclude)
        {
            db.Configuration.LazyLoadingEnabled = !safe;
            db.Configuration.ProxyCreationEnabled = !safe;

            return SecureRetrieve<T>(whereClause, childrenToInclude);
        }
        private IQueryable<T> SecureRetrieve(Expression<Func<T, bool>> whereClause, params string[] childrenToInclude)
        {
            var entity = db.Set<T>();
            IQueryable<T> result = null;
            foreach (var child in childrenToInclude)
                result = (result ?? entity).Include(child);

            var predicate = PredicateBuilder.New<T>(true);
            if (whereClause != null)
                predicate = predicate.And(whereClause);

            return (result ?? entity).AsExpandable().Where(predicate);
        }

        private IQueryable<T> SecureRetrieve<T>(Expression<Func<T, bool>> whereClause, params string[] childrenToInclude)
            where T : class
        {
            var entity = db.Set<T>();
            IQueryable<T> result = null;
            foreach (var child in childrenToInclude)
                result = (result ?? entity).Include(child);

            var predicate = PredicateBuilder.New<T>(true);
            if (whereClause != null)
                predicate = predicate.And(whereClause);

            return (result ?? entity).AsExpandable().Where(predicate);
        }
        public T GetOne(Expression<Func<T, bool>> whereClause, bool safe = false, params string[] childrenToInclude)
        {
            db.Configuration.LazyLoadingEnabled = !safe;
            db.Configuration.ProxyCreationEnabled = !safe;

            return SecureRetrieve(whereClause, childrenToInclude).FirstOrDefault();
        }
        public T GetOne<T>(Expression<Func<T, bool>> whereClause, bool safe = false, params string[] childrenToInclude)
          where T : class
        {
            db.Configuration.LazyLoadingEnabled = !safe;
            db.Configuration.ProxyCreationEnabled = !safe;

            return SecureRetrieve(whereClause, childrenToInclude).FirstOrDefault();
        }
        public virtual int Delete(Expression<Func<T, bool>> predicate)
        {
            /*
            IQueryable<TEntity> item = db.Set<TEntity>().Where(predicate);
            db.Set<TEntity>().RemoveRange(item);
            */
            SecureRetrieve(predicate).DeleteFromQuery();

            return db.SaveChanges();
        }

        public virtual bool Update(T obj, params Expression<Func<T, object>>[] propertiesToUpdate)
        {
            db.Set<T>().Attach(obj);
            Array.ForEach(propertiesToUpdate, p => db.Entry(obj).Property(p).IsModified = true);
            return db.SaveChanges() > 0;
        }
        public virtual int Update(List<T> l, params Expression<Func<T, object>>[] propertiesToUpdate)
        {
            foreach (T t in l)
            {
                db.Set<T>().Attach(t);
                Array.ForEach(propertiesToUpdate, p => db.Entry(t).Property(p).IsModified = true);
            }
            return db.SaveChanges();
        }

        public virtual bool Add(T e, bool delaySave = false)
        {
            db.Set<T>().Add(e);
            if (!delaySave) return db.SaveChanges() > 0;
            return true;
        }
        public virtual void Add(IEnumerable<T> list, bool delaySave = false)
        {
            db.BulkInsert(list);
            //if (!delaySave) db.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing) if (db != null) db.Dispose();

                // new shared cleanup logic
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(db);
        }

    }
}
