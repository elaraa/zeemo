﻿using DB.ORM.DB;
using Services.Web.Business;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web.App.Admin.Controllers;

namespace Web.App.Admin
{
    public static class MenuExtension
    {
        //items should be represented as composite pattern

        private static MenuItem MenuLoadChilddren(SystemMenu menu, ref List<SystemMenu> menus)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var controller = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();
            var menuList = menu == null ? menus.Where(w => w.ParentMenuId == null) : menus.Where(w => w.ParentMenuId == menu.MenuId);
            bool hasChildren = menuList.Count() > 0;
            MenuItem menuItem;
            if (hasChildren)
            {
                menuItem = new MenuItemComposite();
                foreach (var item in menuList.OrderBy(o => o.SortOrder))
                    (menuItem as MenuItemComposite).Children.Add(MenuLoadChilddren(item, ref menus));

            }
            else menuItem = new MenuItemLeaf();

            if (menu != null)
            {
                menuItem.Label = menu.MenuName;
                menuItem.Link = string.IsNullOrEmpty(menu.Controller) ? "" : urlHelper.Action(menu.Action, menu.Controller);
                menuItem.Selected = menu.Controller == controller;
                menuItem.Icon.IconText = menu.MenuIcon;
            }
            return menuItem;
        }

        public static MvcHtmlString RenderMenu(this HtmlHelper helper)
        {
            /// get menu by user id
            var menuItems = new MenuAccessService((helper.ViewContext.Controller as BaseController).UserId).MyMenus;
            /// render the Menu
            var item = MenuLoadChilddren(null, ref menuItems);
            var sb = new StringBuilder();
            sb.Append("<ul class=\"page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu \" data-keep-expanded=\"false\" data-auto-scroll=\"true\" data-slide-speed=\"200\">");
            if ((item as MenuItemComposite) != null)
                foreach (var mi in (item as MenuItemComposite).Children)
                    sb.Append(mi.Draw());
            else
                sb.Append(item.Draw());
            sb.Append("</ul>");
            return new MvcHtmlString(sb.ToString());
            //return new MvcHtmlString("");
        }
    }
    public abstract class MenuItem
    {
        public string Link { get; set; }
        public Icon Icon { get; set; }
        public string Label { get; set; }
        public bool Selected { get; set; }
        public MenuItem()
        {
            Link = Label = string.Empty;
            Selected = false;
            Icon = new Icon { IconText = "" };
        }
        public abstract string Draw();
        public virtual bool IsSelected() { return Selected; }
        
    }
    public class MenuItemLeaf : MenuItem
    {
        public override string Draw()
        {
            if (string.IsNullOrEmpty(Link) && string.IsNullOrEmpty(Label))
                return "";
            return string.Format(
                    "<li class=\"nav-item {3}\">" +
                        "<a href = \"{0}\" class=\"nav-link \">" +
                        "   {2} " +
                         "<span class=\"title\">{1}</span>" +
                         "{4}"+
                        "</a>" +
                    "</li>", Link, Label, Icon, Selected? "start active open":"",Selected? "<span class=\"selected\"></span>":"");
        }
    }
    public class MenuItemComposite : MenuItem
    {
        public MenuItemComposite():base()
        {
            Children = new List<MenuItem>();
        }
        public List<MenuItem> Children { get; set; }
        public override bool IsSelected()
        {
            foreach (var item in Children)
                if (item.IsSelected()) return true;
            return false;
        }
        public override string Draw()
        {
            if (Children.Count < 1)
                return new MenuItemLeaf { Icon= Icon, Label = Label, Link = Link, Selected = Selected }.Draw();
            Selected = IsSelected();
            StringBuilder output = new StringBuilder();
            ///draw the parent
            output.AppendFormat("<li class=\"nav-item {0}\">", Selected ? "start active open" : "");
            output.AppendFormat("<a href=\"{0}\" class=\"nav-link nav-toggle\" >" +
                                    "{2} " +
                                   "<span class=\"title\">{1}</span>"+
                                    "{3}"+
                                "</a>", 
                                    Link == string.Empty? "javascript:;" : Link, Label, Icon, Selected? "<span class=\"selected\"></span><span class=\"arrow open\"></span>" : "<span class=\"arrow\"></span>");
            ///draw the children
            //if (Children != null && Children.Count > 0)
            output.Append(" <ul class=\"sub-menu\">");

            foreach (var item in Children)
                output.Append(item.Draw());

            //if (Children != null && Children.Count > 0)
            output.Append("</ul>");

            output.AppendFormat("</li>");
            return output.ToString();
        }
    }
}
