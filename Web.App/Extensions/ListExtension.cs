﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Web.App
{
    public struct ListButton
    {
        private const string _listButtonTemplate = "        <button type=\"button\" onclick=\"{1}\" class=\"btn {2} margin-bottom-20\" {4}><i class=\"{3}\"></i> {0}</button>";

        public string Label { get; set; }
        public string OnClickEvent { get; set; }
        public string Color { get; set; }
        public string Icon { get; set; }
        public string OtherProperties { get; set; }
        public static string UrlNavigateClickEvent(string url) { return string.IsNullOrEmpty(url) ? string.Empty : string.Format("location.href = '{0}';", url); }
        public static ListButton addButton(HtmlHelper helper, string createUrl = null) {

            if (string.IsNullOrEmpty(createUrl))
            {
                System.Web.Routing.RequestContext requestContext = HttpContext.Current.Request.RequestContext;
                string controller = helper.ViewContext.RouteData.Values["controller"].ToString();
                UrlHelper urlHelper = new UrlHelper(requestContext);
                createUrl = urlHelper.Action("Create", controller);
            }
            return new ListButton {
                Label = "Add",
                Icon = "fa fa-plus",
                OnClickEvent = string.Format("location.href = '{0}';",createUrl),
                Color = "btn-primary"
            };
        }
        public string GetButtonString() {
            return string.Format(_listButtonTemplate, Label, OnClickEvent, Color, Icon, OtherProperties);
        }
    }
    public static class ListExtension
    {
        private const string _listActionsContainer = "<div class=\"col-sm-11 col-xs-11 col-md-12 text-right\">" +
            "    <div class=\"btn-group btn-group-xs table-options\">" +
            "     {0} " +
            "    </div>" +
            "</div>";
       

        private static readonly string _childActions = string.Format("<div class=\"row\">{0}</div>", _listActionsContainer);

        public static MvcHtmlString DrawListActions<TModel>(this HtmlHelper<TModel> helper, string createUrl = null)
        {
            return DrawListActions<TModel>(helper, ListButton.addButton(helper, createUrl));
        }
        public static MvcHtmlString DrawChildListActions<TModel>(this HtmlHelper<TModel> helper, string createUrl = null)
        {
            return DrawChildListActions<TModel>(helper, ListButton.addButton(helper,createUrl));
        }
        public static MvcHtmlString DrawChildListActions<TModel>(this HtmlHelper<TModel> helper, params ListButton[] buttons)
        {
            return new MvcHtmlString(string.Format(_childActions, GetButtons(buttons)));
        }
        public static MvcHtmlString DrawListActions<TModel>(this HtmlHelper<TModel> helper, params ListButton[] buttons)
        {
            return new MvcHtmlString(string.Format(_listActionsContainer, GetButtons(buttons)));
        }
        private static string GetButtons(params ListButton[] buttons)
        {
            if (buttons == null || buttons.Length < 1) return "";
            var builder = new System.Text.StringBuilder();

            foreach (var item in buttons)
                builder.Append(item.GetButtonString());

            return builder.ToString();
        }
        
    }
}