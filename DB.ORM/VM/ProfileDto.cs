﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DB.ORM.DB
{
    public class Profile
    {
        public Profile()
        {
            Membership = "Golden";
        }
        public int Id { get; set; }
        public string Pic { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Address { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$", ErrorMessageResourceName = "WrongMobileFormat", ErrorMessageResourceType = typeof(ZM.Message))]
        public string Mobile { get; set; }
        [DataType(DataType.Url)]
        public string Website { get; set; }
        public string Membership { get; set; }
        public string UserName { get; set; }
    }
}
