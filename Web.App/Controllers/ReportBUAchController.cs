﻿using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class ReportBUAchController : BaseReportController
    {
        public override ActionResult Index()
        {
            //ViewBag.Time = LookupHelper.GetTimeDefinitions(this);
            //ViewBag.Teams = LookupHelper.GetMyTeams(this);
            ViewBag.ReportName = "Report";

            return base.Index();
        }
        public ReportBUAchController()
        {
            ViewNamePlural= ViewNameSingular = "Report";
            //ReportName = "BU+Achievement";

        }
    }
}