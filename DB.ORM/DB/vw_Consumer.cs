namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Consumer
    {
        [Key]
        [Column(Order = 0)]
        public int ConsumerId { get; set; }

        [StringLength(50)]
        public string ConsumerCode { get; set; }

        [Key]
        [Column(Order = 1)]
        public string ConsumerName { get; set; }

        [Key]
        [Column(Order = 2)]
        public string Phone { get; set; }

        [Key]
        [Column(Order = 3, TypeName = "date")]
        public DateTime BirthDate { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(6)]
        public string Gender { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(7)]
        public string Language { get; set; }

        public int? ReferralId { get; set; }

        public string Logo { get; set; }

        [Key]
        [Column(Order = 6)]
        public bool ShowNotifications { get; set; }

        [StringLength(50)]
        public string OTP { get; set; }

        [Key]
        [Column(Order = 7)]
        public bool PendingOTP { get; set; }

        [Key]
        [Column(Order = 8)]
        public bool IsActive { get; set; }
    }
}
