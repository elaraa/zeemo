namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vConsumerNotification")]
    public partial class vConsumerNotification
    {
        [Key]
        [Column(Order = 0)]
        public bool IsRead { get; set; }

        [Key]
        [Column(Order = 1)]
        public string NotificationMessage { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NotificationId { get; set; }

        [Key]
        [Column(Order = 3)]
        public string NotificationImage { get; set; }

        public string MerchantBrandName { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime CreationDate { get; set; }
    }
}
