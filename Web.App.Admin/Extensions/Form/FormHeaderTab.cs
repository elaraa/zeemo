﻿using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Web.App.Admin
{
    public class FormHeaderTab
    {
        private const string _tabTemplate = "  <li class=\"{0}\">" +
                                            "    <a href = \"#{1}\" data-toggle=\"tab\"> {2}</a>" +
                                            "</li>";

        public string Name { get; set; }
        public string Id { get; set; }
        public bool Active { get; set; }
        public string Icon { get; set; }
        public override string ToString()
        {
            string iconHtml = string.IsNullOrEmpty(Icon) ? "" : string.Format("<i class=\"{0}\"></i>", Icon);
            return string.Format(_tabTemplate, Active ? "active" : "", Id, iconHtml + Name);
        }
        public static FormHeaderTab DefaultTab(HtmlHelper helper, string tabName = "Details", string tabId = "tab_1")
        {
            return new FormHeaderTab
            {
                Name = tabName,
                Icon = null,
                Active = false
            };
        }
        public static string GetTabs(params FormHeaderTab[] tabs)
        {
            if (tabs == null || tabs.Length < 1) return "";
            var tabsHtml = new StringBuilder("<ul class=\"nav nav-tabs\">");
            var activeTab = tabs.FirstOrDefault(w => w.Active == true);
            activeTab = activeTab ?? tabs[0];
            activeTab.Active = true;

            foreach (var item in tabs)
                tabsHtml.Append(item.ToString());
            tabsHtml.Append("</ul>");
            return tabsHtml.ToString();
        }
    }
}