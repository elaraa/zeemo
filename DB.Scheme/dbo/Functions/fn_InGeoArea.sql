﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION  [dbo].[fn_InGeoArea]
(
	@Lat1 nvarchar(20),
    @Lng1 nvarchar(20),
    @Lat2 nvarchar(20),
    @Lng2 nvarchar(20)
)
RETURNS bit
AS
BEGIN
DECLARE @P1 geography, @P2 geography;
SET @P1 = geography::STGeomFromText('POINT(' + @Lat1 +' '+ @Lng1 + ')', 4326);
SET @P2 = geography::STGeomFromText('POINT(' + @Lat2 +' '+ @Lng2 + ')', 4326);

RETURN case when @P1.STDistance(@P2)/1000 
<= (select top 1 AppSettingValue from AppSettings where AppSettingCode = 'MaxGeoDistance') Then 1 else 0 end;

END