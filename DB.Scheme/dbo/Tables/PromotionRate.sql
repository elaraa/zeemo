﻿CREATE TABLE [dbo].[PromotionRate] (
    [PromotionRunRateId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [PromotionRunId]     INT            NOT NULL,
    [ConsumerId]         INT            NOT NULL,
    [Rate]               SMALLINT       NOT NULL,
    [RateTypeId]         INT            NULL,
    [RateComment]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK__OfferRat__804FE5BA5F237549] PRIMARY KEY CLUSTERED ([PromotionRunRateId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0 -> good , 1 -> bad', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PromotionRate', @level2type = N'COLUMN', @level2name = N'Rate';

