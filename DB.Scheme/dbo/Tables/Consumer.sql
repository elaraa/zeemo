﻿CREATE TABLE [dbo].[Consumer] (
    [ConsumerId]        INT            IDENTITY (1, 1) NOT NULL,
    [ConsumerCode]      NVARCHAR (50)  NULL,
    [ConsumerName]      NVARCHAR (MAX) NOT NULL,
    [Phone]             NVARCHAR (MAX) NOT NULL,
    [BirthDate]         DATE           NOT NULL,
    [Gender]            CHAR (1)       NOT NULL,
    [ReferralId]        INT            NULL,
    [Photo]             NVARCHAR (MAX) NULL,
    [Language]          NVARCHAR (5)   CONSTRAINT [DF_Consumer_Language] DEFAULT (N'en') NOT NULL,
    [ShowNotifications] BIT            NOT NULL,
    [OTP]               NVARCHAR (50)  NULL,
    [PendingOTP]        BIT            CONSTRAINT [DF__Consumer__Pendin__6383C8BA] DEFAULT ((1)) NOT NULL,
    [IsActive]          BIT            CONSTRAINT [DF__Consumer__Active__628FA481] DEFAULT ((0)) NOT NULL,
    [LoginDate]         DATETIME       NULL,
    [FCToken]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Consumer] PRIMARY KEY CLUSTERED ([ConsumerId] ASC)
);

