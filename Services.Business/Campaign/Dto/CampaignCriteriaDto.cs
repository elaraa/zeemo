﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Services.Web.Business.Campaign.Dto
{
    public class CampaignCriteriaDto
    {
        public int PromotionRunCriteriaId { get; set; }
        public int PromotionRunId { get; set; }
        public int? AgeFrom { get; set; }
        public int? AgeTo { get; set; }

        [StringLength(6)]
        public string Gender { get; set; } 

    }
}