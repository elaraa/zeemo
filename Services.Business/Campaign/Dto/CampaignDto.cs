﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Services.Web.Business.Campaign.Dto
{
    public class CampaignDto
    {
        public int PromotionRunId { get; set; }
        public int PromotionId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public int RedeemCountPerPerson { get; set; }
        public int NumberOfPoints { get; set; }
        public int NumberOfPieces { get; set; }
        public IList<CampaignCriteriaDto> CampaignCriteria { get; set; }
        public IList<CampaignBranchDto> Branches { get; set; }

        public CampaignDto() {
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
        }
    }
}