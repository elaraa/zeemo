﻿using Services.Base;
using System;
using System.Collections.Generic;
using DB.ORM.DB;
using Services.Mobile.Business.Dto;
using AutoMapper;
using System.Linq;

namespace Services.Mobile.Business
{
   public class LocationService : BaseService<Area>
    {
        public IQueryable<LocationDto> AllAreas(string userToken)
        { 
             
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var lang = getLanguageByConsumer(consumerId);
                    var areas =lang=="en" ? Get().Select(a=> new LocationDto {AreaId=a.AreaId,AreaName=a.AreaName,Lat=a.Lat,Lng=a.Lng })
                        : Get().Select(a => new LocationDto { AreaId = a.AreaId, AreaName = a.AreaNameAr, Lat = a.Lat, Lng = a.Lng }); 
                    return areas;
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }

        }

      
    }
}
