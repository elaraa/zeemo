﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(PromotionMetaDetaDto))]
    public partial class Promotion  
    {
        class PromotionMetaDetaDto
        {
            [Required(ErrorMessageResourceName = "RequiredPromotionName", ErrorMessageResourceType = typeof(ZM.Promotion))]
            public string PromotionName { get; set; }
        }
    }
}
