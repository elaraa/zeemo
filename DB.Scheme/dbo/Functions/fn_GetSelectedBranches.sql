﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetSelectedBranches]
(	@PromotionRunId int,
	@MerchantId int
)
RETURNS TABLE 
AS
RETURN 
(
select distinct mb.BranchId, mb.MerchantId, mb.Address, a.AreaName
, (CASE WHEN rb.PromotionRunId = @PromotionRunId THEN 1 ELSE 0 END) AS Selected
from [dbo].[MerchantBranch] mb INNER JOIN
 dbo.Area AS a ON a.AreaId = mb.AreaId left join

[dbo].[PromotionRunBranch] rb on rb.[BranchId] = mb.[BranchId] and rb.PromotionRunId = @PromotionRunId

where mb.MerchantId = @MerchantId
)