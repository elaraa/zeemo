﻿using DB.ORM.DB;
using Services.Web.Business.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.App.Shared
{
    public class SessionSetting
    {
        public static UnitOfWork PromotionalUnitOfWork
        {
            get { return HttpContext.Current.Session[ZM.SessionResource.PromotionalUnitOfWork] as UnitOfWork; }

            set { HttpContext.Current.Session[ZM.SessionResource.PromotionalUnitOfWork] = value; }
        }
        public static UnitOfWork BranchUnitOfWork
        {
            get { return HttpContext.Current.Session[ZM.SessionResource.BranchUnitOfWork] as UnitOfWork; }

            set { HttpContext.Current.Session[ZM.SessionResource.BranchUnitOfWork] = value; }
        }
        public static string Culture
        {
            get { return HttpContext.Current.Session[ZM.SessionResource.SessionCulture] as string; }
            set { HttpContext.Current.Session[ZM.SessionResource.SessionCulture] = value; }
        }
        public static bool? IsRightToLeft
        {
            get { return HttpContext.Current.Session[ZM.SessionResource.IsRightToLeft] as bool?; }
            set { HttpContext.Current.Session[ZM.SessionResource.IsRightToLeft] = value; }
        }
    }
}