﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.App.Admin.Models
{
    public class VMRole
    {
        public VMRole()
        {

        }
        public VMRole(SystemRole role)
        {
            if (role == null) return;

            RoleId = role.SystemRoleId;
            RoleName = role.RoleName;
            IsActive = role.Active;
            IsAdmin = role.IsAdmin;
            if(role.SystemRoleMenus != null)
            MenuItems = string.Join(",",role.SystemRoleMenus.Select(s => s.MenuId).ToList());
        }

        public int RoleId { get; set; }
        [Required(ErrorMessageResourceName = "RequiredRoleName", ErrorMessageResourceType = typeof(ZM.RoleAdmin))]
        public string RoleName { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }
        public int? CountryId { get; set; }
        public string MenuItems { get; set; }
        public SystemRole GetModel() {
            var menus = new List<SystemRoleMenu>();
            foreach (var item in MenuItems.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                menus.Add(new SystemRoleMenu { RoleId = RoleId, MenuId = Convert.ToInt32(item) });

            return new SystemRole {
                SystemRoleId = RoleId,
                RoleName = RoleName,
                IsAdmin = IsAdmin,
                Active = IsActive,
                SystemRoleMenus = menus
            };
        }
    }
}