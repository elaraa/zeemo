using AutoMapper;
using DB.ORM.DB;
using Services.Base;
using Services.Web.Business.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static DB.ORM.DB.Consumer;

namespace Web.App.Admin.Controllers
{
    public class ConsumerController : BaseControllerTemplateDto<vw_Consumer, Consumer, ConsumerDto, int>
    {
       public ConsumerController() : base()
       {
            ViewNameSingular = ZM.ViewPages.ConsumerViewNameSingular;
            ViewNamePlural = ZM.ViewPages.ConsumerViewNamePlural;
       }
       protected override Expression<Func<Consumer, bool>> GetOneExpression(int? id)
       {
           return id.HasValue ? (x => x.ConsumerId == id.Value) : (Expression<Func<Consumer, bool>>)(x => 1 == 0);
       }
        protected override void InitTemplateServices()
        {
            ListService = new BaseService<vw_Consumer>(UserId);
            ActionService = new ConsumerService();
        }
        protected override void FillViewBag(bool isCreate)
       {
           ViewBag.Genders = LookupHelper.GetGenders(this);
           ViewBag.Languages = LookupHelper.GetLanguages(this);
           base.FillViewBag(isCreate);
       }
    }
}
