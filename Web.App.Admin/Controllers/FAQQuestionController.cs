using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
namespace Web.App.Admin.Controllers
{
    public class FAQQuestionController : BaseControllerTemplate<vw_FAQ, FAQQuestion, int>
    {
       public FAQQuestionController() : base()
       {
            ViewNameSingular = "FAQ";
            ViewNamePlural = "FAQs";
            PropertiesToUpdate = new Expression<Func<FAQQuestion, object>>[]
           {
               x=>x.FaqQuestionId,
               x=>x.QuestionText,
               x=>x.FaqCategoryId,
               x=>x.AnswerText01,
               x=>x.AnswerImage,
               x=>x.AnswerText02

           };
       }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Categories = LookupHelper.GetFAQCategories(this);
            base.FillViewBag(isCreate);
        }
        protected override Expression<Func<FAQQuestion, bool>> GetOneExpression(int? id)
    {
        return id.HasValue ? (x => x.FaqQuestionId == id.Value) : (Expression<Func<FAQQuestion, bool>>)(x => 1 == 0);
    }
 }
}
