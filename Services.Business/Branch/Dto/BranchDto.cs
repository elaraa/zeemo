﻿using DB.ORM.DB;
using Services.Web.Business.Branch.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Services.Web.Business.Dto
{
    public class BranchDto
    {
        public int BranchId { get; set; }
        public int MerchantId { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public int AreaId { get; set; }
        [Required]
        public decimal Lat { get; set; }
        [Required]
        public decimal Lng { get; set; }

        List<BranchCashierDto> Cashiers { get; set; }
    }
   
}
