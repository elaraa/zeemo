namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerRedeemedPoint")]
    public partial class ConsumerRedeemedPoint
    {
        [Key]
        public int ConsumerPointRedeemId { get; set; }

        public int ConsumerId { get; set; }

        public int MerchantId { get; set; }

        public int Points { get; set; }

        public int PointId { get; set; }

        public virtual Consumer Consumer { get; set; }

        public virtual Merchant Merchant { get; set; }

        public virtual MerchantPoint MerchantPoint { get; set; }
    }
}
