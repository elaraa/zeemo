﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DB.ORM.DB
{
    [MetadataType(typeof(ConsumerMetaDataDto))]
    public partial class Consumer 
    {
        class ConsumerMetaDataDto
        {
            [StringLength(50)]
            public string ConsumerCode { get; set; }

            [Required(ErrorMessageResourceName = "RequiredConsumerName", ErrorMessageResourceType = typeof(ZM.ConsumerAdmin))]
            public string ConsumerName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredPhone", ErrorMessageResourceType = typeof(ZM.ConsumerAdmin))]
            [DataType(DataType.PhoneNumber)]
            public string Phone { get; set; }

            [Required(ErrorMessageResourceName = "RequiredGender", ErrorMessageResourceType = typeof(ZM.ConsumerAdmin))]
            [StringLength(1)]
            public string Gender { get; set; }

            [Required(ErrorMessageResourceName = "RequiredLanguage", ErrorMessageResourceType = typeof(ZM.ConsumerAdmin))]
            [StringLength(5)]
            public string Language { get; set; }
        }

        [NotMapped]
        public class ConsumerDto : Consumer {
            public int Id { get; set; }
            public string Logo { get; set; }
            public HttpPostedFileBase PictureFile { get; set; }
        }
    }
}
