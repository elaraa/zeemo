namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_PromotionToCheck
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string PromotionName { get; set; }

        public string PromotionNameAr { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionTypeId { get; set; }

        [Key]
        [Column(Order = 3)]
        public string PromotionTypeName { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MerchantId { get; set; }

        [Key]
        [Column(Order = 5)]
        public string MerchantName { get; set; }

        public DateTime? CreationDate { get; set; }
    }
}
