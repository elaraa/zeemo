﻿using DB.ORM.DB;
using Services.Base;
using System.Collections.Generic;
using System.Linq;

namespace Services.Web.Business
{
    public class MenuAccessService : BaseService<SystemMenu>
    {
        public MenuAccessService(int userId) : base(userId) { }
        public bool CanAccess(string controllerName)
        {
            if (controllerName.IsEmpty())
                return false;

            return Any<vw_Menus>(c=>c.Controller == controllerName && c.UserId == UserId);
        }
        public List<SystemMenu> MyMenus => db.fn_GetAccessMenu(UserId).ToList();
    }
}
