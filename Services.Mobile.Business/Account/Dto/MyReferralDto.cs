﻿

namespace Services.Mobile.Business.Dto
{
   public class ReferralDto
    {
        public string ReferralCode { get; set; }

        public int Friends { get; set; }

        public int Points { get; set; }

        public string ShareLink { get; set; }
    }
}
