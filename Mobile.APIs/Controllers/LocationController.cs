﻿using Services.Mobile.Business;
using System.Web.Http;
using static Services.Helper.EnumHelpers;

namespace Mobile.APIsConsumer
{
    public class LocationController : ApiController
    {
        LocationService LocationService = new LocationService();

        [HttpPost]
        public IHttpActionResult GetList()
        {
            //using (var db = new DBModel())
            //{
            //    return Json(new RequestResult
            //    {
            //        Failed= false,
            //        Data = db.Areas.ToList()
            //    });
            //}
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(new RequestResult { Failed = false, Data = LocationService.AllAreas(userToken) });
            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }
        ////For any request dataheader object will contain the following:
        /*
         * getApplicationName()
            getAvailableLocationProviders()
            getBuildId()
            getBatteryLevel()
            getBrand()
            getBuildNumber()
            getBundleId()
            getCarrier()
            getDeviceId()
            getDeviceType()
            getDeviceName()
            getFontScale()
            getFreeDiskStorage()
            getIpAddress()
            getMacAddress()
            getManufacturer()
            getModel()
            getPowerState()
            getReadableVersion()
            getSystemName()
            getSystemVersion()
            getTotalDiskCapacity()
            getTotalMemory()
            getUniqueId()
            getUsedMemory()
            getUserAgent()
            getVersion()
            hasNotch()
            isBatteryCharging()
            isLocationEnabled()
            isHeadphonesConnected()
            isTablet()
         */
    }
}
