﻿using Kendo.Mvc.UI;
using Services.Base;
using Services.Web.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.App.Admin.Controllers;

namespace Web.App.Admin
{
    public class LookupHelper
    {
        private int UserId { get; set; }
        public LookupHelper(BaseController controller)
        {
            UserId = controller.UserId;
        }
        public IQueryable<TModel> GetMyData<TModel>() where TModel : class, new()
        {
            return new BaseService<TModel>(UserId).Get(null, true);
        }
        public IEnumerable<SelectListItem> GetMyDropDownListData<TModel>(Func<TModel, SelectListItem> selector) where TModel : class, new()
        {
            return GetMyData<TModel>().ToDropDownList(selector);
        }
       
        

        public static IEnumerable<SelectListItem> GetMyRoles(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.SystemRole>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.RoleName, Value = s.SystemRoleId.ToString() });
        }

        internal static IEnumerable<SelectListItem> GetFAQCategories(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.FAQCategory>(controller.UserId).Get(null, true)
               .ToDropDownList(s => new SelectListItem { Text = s.CategoryName, Value = s.FaqCategoryId.ToString() });
        }

        internal static IEnumerable<SelectListItem> GetMyInterests(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.Interest>(controller.UserId).Get(null, true)
                .ToDropDownList(s => new SelectListItem { Text = s.InterestName, Value = s.InterestId.ToString() });
        }

        public static IEnumerable<TreeViewItemModel> GetAllMenus(BaseController controller, string[] selectedMenuItems = null) {
            var menus = new BaseService<DB.ORM.DB.SystemMenu>(controller.UserId).Get(null, true).ToList();
            if (menus == null) return null;
            var items = MenuLoadChilddren(null,ref menus, selectedMenuItems);
            return items.Items;
        }
        private static TreeViewItemModel MenuLoadChilddren(DB.ORM.DB.SystemMenu menu, ref List<DB.ORM.DB.SystemMenu> menus, string[] selectedMenuItems = null)
        {
            var menuList = menu == null ? menus.Where(w => w.ParentMenuId == null) : menus.Where(w => w.ParentMenuId == menu.MenuId);
            var menuItem = new TreeViewItemModel
            {
                HasChildren = menuList.Count() > 0,
                Text = menu==null?string.Empty: menu.MenuName,
                Id = menu == null ? string.Empty : menu.MenuId.ToString(),
                Expanded = false
            };
            
            menuItem.Checked = selectedMenuItems==null?false: selectedMenuItems.Contains(menuItem.Id);

            if (menuItem.HasChildren)
                foreach (var item in menuList.OrderBy(o => o.SortOrder))
                    menuItem.Items.Add(MenuLoadChilddren(item, ref menus, selectedMenuItems));

            return menuItem;
        }
        public static IEnumerable<SelectListItem> GetGenders(BaseController controller)
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "M", Text = "Male" },
                new SelectListItem { Value = "F", Text = "Female" }
            };
        }

        public static IEnumerable<SelectListItem> GetLanguages(BaseController controller)
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "en", Text = "English" },
                new SelectListItem { Value = "ar", Text = "Arabic" }
            };
        }

        public static IEnumerable<SelectListItem> GetMaritalStatuses(BaseController controller)
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "S", Text = "Single" },
                new SelectListItem { Value = "M", Text = "Married" }
            };
        }
    }
}