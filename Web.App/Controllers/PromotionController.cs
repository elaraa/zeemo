﻿
using AutoMapper;
using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Web.Business;
using Services.Web.Business.Base;
using Services.Web.Business.Dto;
using Services.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Shared;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using static Services.Helper.EnumHelpers;

namespace Web.App.Controllers
{
    public class PromotionController :  BaseControllerTemplate<vw_Promotion, Promotion, int>, IDisposable
    {
        PromotionService promotionService;
        
        public PromotionController()
        {
            promotionService = new PromotionService();
            
            ViewNamePlural = ZM.ViewPages.PromotionViewNamePlural;
            ViewNameSingular = ZM.ViewPages.PromotionViewNameSingular;

            if (SessionSetting.PromotionalUnitOfWork == null)
                SessionSetting.PromotionalUnitOfWork = promotionService.InitiateUnitOfWork();
           

            PropertiesToUpdate = new Expression<Func<Promotion, object>>[]
                {
                    x=>x.PromotionId,
                    x=>x.MerchantId,
                    x=>x.PromotionTypeId,
                    x=>x.PromotionName
                };
        }

        public override ActionResult Index()
        {
            ViewBag.Title = ViewNamePlural;
            
            reinitiateUnitOfWork();
            return View();
        }

        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.PromotionTypes = LookupHelper.GetPromotionTypes(this);
            ViewBag.BaseUrl = promotionService.GetBaseUrl;

            base.FillViewBag(isCreate);
        }
        protected override Expression<Func<Promotion, bool>> GetOneExpression(int? id) => id.HasValue ? 
                                      (x => x.PromotionId == id.Value) : (Expression<Func<Promotion, bool>>)(x => 1 == 0);

        private void reinitiateUnitOfWork() {
            if (SessionSetting.PromotionalUnitOfWork != null)
                SessionSetting.PromotionalUnitOfWork.Rollback();

            SessionSetting.PromotionalUnitOfWork = promotionService.InitiateUnitOfWork();
            SessionSetting.PromotionalUnitOfWork.Begin();
        }
        [HttpGet]
        public override ActionResult Create()
        {
            try
            {
                FillViewBag(true);
                reinitiateUnitOfWork();
                return View(new Promotion { MerchantId = UserId });
            }
            catch (Exception e) { Elmah.ErrorSignal.FromCurrentContext().Raise(e);  }
            return View(new Promotion { MerchantId = UserId });
        }

        public ActionResult Details(int? id)
        {
            FillViewBag(false);
            ViewBag.Title = ZM.Promotion.Details;
            reinitiateUnitOfWork();
            var promotion = SessionSetting.PromotionalUnitOfWork.GetEntity(GetOneExpression(id), false, "promotionImages");
            return View("Details", promotion);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveBasicModel(Promotion promotion) {
            promotion.IsDraft = promotion.PromotionId == 0 ? true : promotion.IsDraft;
            var _promotion = SessionSetting.PromotionalUnitOfWork.AddOrUpdateEntity<Promotion>(promotion);
            return Content(_promotion.PromotionId.ToString());
        }

        [HttpPost]
        public ActionResult SubmitModel(PromotionFiles[] files, int promotionId)
        {
            try
            {
                var uploadRes = true;
                if (files != null)
                {
                    var basePath = @"Uploads/" + ZM.Uploads.Promotion + "/" + promotionId + "/";
                    promotionService.SavePromotionImages(files.Select(c => c.fileName).ToList(), basePath, promotionId, SessionSetting.PromotionalUnitOfWork);
                    uploadRes = promotionService.uploadFiles(files, ZM.Uploads.Promotion, promotionId);
                }
                if (uploadRes)
                {
                    if (SessionSetting.PromotionalUnitOfWork.Complete())
                    {
                        ShowSuccess(ZM.Message.SuccessEditCode, true);
                        return Json(true, JsonRequestBehavior.AllowGet);
                    }
                }
                ShowError(ZM.Message.ErrorCode);
            }
            catch (Exception e) {}
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendPromotion(int? id)
        {
            return Json(promotionService.sendPromotion(id.Value), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteImage(int? id)
        {
            promotionService.deleteImage(id.Value, SessionSetting.PromotionalUnitOfWork);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeletePromotion(int? id) 
        {
            return Json(promotionService.deletePromotion(id.Value),JsonRequestBehavior.AllowGet);
        }

    }
}