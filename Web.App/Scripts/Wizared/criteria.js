﻿function OnSelectGridChanged(e) {
    var checkBoxes = $(".k-checkbox").map(function () {
        return this.checked;
    }).get();
    var branchSelection = document.getElementsByClassName("BranchSelection");
    for (var i = 0; i < branchSelection.length; i++) {
        branchSelection[i].value = checkBoxes[i + 1] ? "1" : "0";
    }
}
var currentTab = 1, maxTab = 3;
var submittedFormId = "";



function index(gridName, dataItem) {
    var data = $("#" + gridName).data("kendoGrid").dataSource.data();
    return data.indexOf(dataItem);
}

function Success(data) {
    $('#PromotionRunId').val(data);
}
function Failure(data) {
    currentTab = 1;
}
function onSuccess(e) {
    var listView = $("#attachmentListView").data("kendoListView");
    listView.dataSource.read();
}

$('#submitBtn').on("click", function (e) {
    $("#Main").submit();
});