﻿using Newtonsoft.Json;
using Services.Helper;
using Services.Mobile.Business;
using Services.Mobile.Business.Offer;
using Services.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using static Services.Helper.EnumHelpers;

namespace Mobile.APIsConsumer
{
    public class OfferController : ApiController
    {
        OfferService offerService = new OfferService();

        [HttpPost]
        public IHttpActionResult GetLastOfferToRate()
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if(ModelState.IsValid)
                return Json(offerService.getLastOfferToRate(userToken));
            return Json (new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr()});
        }

        [HttpPost]
        public IHttpActionResult GetSortBy()
        {
            return Json(new RequestResult { Failed = false, Data = offerService.getSortBy()});
        }

        [HttpPost]
        public IHttpActionResult GetList([FromBody] ListParams @params, int? interestId = null, string merchants = "", string offerTypes = "", int? sortBy = null, string LocationId="")
        {
            var userToken = Request.Headers.Authorization.Parameter;
            return Json(offerService.getList(@params, userToken,Request.Headers ,interestId, merchants, offerTypes, sortBy,LocationId));
        }
       
        [HttpPost]
        public IHttpActionResult GetOne([FromBody]ConsumerStructContract<int> id)
        {
            
            var userToken = Request.Headers.Authorization.Parameter;

            if (ModelState.IsValid)
                return Json(offerService.getOne(id.wcObject, userToken));
            return Json(offerService.getReportOfferReasonList(userToken));

        }

        [HttpPost]
        public IHttpActionResult GetQR([FromBody]ConsumerStructContract<int> id)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(offerService.getQR(id.wcObject, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr()});
        }
       
        [HttpPost]
        public IHttpActionResult RateOffer([FromBody]ConsumerRateOfferDto offer)
        {
            //check token, if valid => check offer is in his list/and is a valid offer => if valid => apply rating
            var userToken = Request.Headers.Authorization.Parameter;
            return Json(offerService.rateOffer(offer, userToken));
        }
        [HttpPost]
        public IHttpActionResult RateBadOffer([FromBody]ConsumerRateOfferDto offer)
        {
            //check token, if valid => check offer is in his list/and is a valid offer => if valid => apply rating
            var userToken = Request.Headers.Authorization.Parameter;
            return Json(offerService.rateOffer(offer, userToken));
        }

        [HttpPost]
        public IHttpActionResult GetOfferTypes()
        {
            //using (var db = new DBModel())
            //{
            //    return Json(db.OfferTypes.ToList());
            //}
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(new RequestResult { Failed = false, Data = offerService.getOfferTypes(userToken) });
           return Json(new RequestResult { Failed = true });
        }
         
        [HttpPost]
        public IHttpActionResult GetOfferRateTypes()
        {
            
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(new RequestResult { Failed=false,Data=offerService.getOfferRateTypes(userToken) });
            return Json(new RequestResult { Failed = true,ErrorCode=ReturnErrorCode.DBError.DescriptionAttr() });

        }

        [HttpPost]
        public IHttpActionResult AddToWishList([FromBody]ConsumerStructContract<int> id)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            try
            {
                
                offerService.addToWishList(id.wcObject, userToken);
                return GetOne(id);
            }
            catch (Exception ex)
            {
                return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DBError.DescriptionAttr(), Data = ex });
            }
        }
    
        [HttpPost]
        public IHttpActionResult ReportOffer([FromBody]VMReportOfferDto offer)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                 return Json(offerService.reportOffer(offer,userToken));
            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DBError.DescriptionAttr() });
        }

        [HttpPost]
        public IHttpActionResult GetReportOfferReasonList()
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if(ModelState.IsValid)
                return Json(offerService.getReportOfferReasonList(userToken));
            return Json(offerService.getReportOfferReasonList(userToken));

        }
    }
}
