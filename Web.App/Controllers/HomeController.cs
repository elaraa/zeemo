﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class HomeController : BaseController
    {
        public override ActionResult Index()
        {
            FillViewBag(true);
            ///https://dottutorials.net/dynamic-user-defined-dashboards-asp-net-core-tutorial/
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = ZM.Message.AboutMessage;

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = ZM.Message.ContactMessage;

            return View();
        }
    }
}