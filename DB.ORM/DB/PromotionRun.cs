namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PromotionRun")]
    public partial class PromotionRun
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PromotionRun()
        {
            ConsumerReportPromotionRuns = new HashSet<ConsumerReportPromotionRun>();
            PromotionRunBranches = new HashSet<PromotionRunBranch>();
            PromotionRunCriterias = new HashSet<PromotionRunCriteria>();
        }

        public int PromotionRunId { get; set; }

        public int PromotionId { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public int? RedeemCountPerPerson { get; set; }

        public int? NumberOfPoints { get; set; }

        public int? NumberOfPieces { get; set; }

        public decimal? Savings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerReportPromotionRun> ConsumerReportPromotionRuns { get; set; }

        public virtual Promotion Promotion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PromotionRunBranch> PromotionRunBranches { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PromotionRunCriteria> PromotionRunCriterias { get; set; }
    }
}
