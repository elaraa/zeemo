﻿using Services.Base;
using Services.Web.Business;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Helpers.File;

namespace Web.App.Admin.Controllers
{
    public class BaseControllerImportExport<DownloadModel> : BaseController
         where DownloadModel : class, new()
    {
        protected List<string> FileDownloadExcludedColumns { get; set; }
        protected string FileName { get; set; }
        protected string SheetName { get; set; }
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase UploadFiles, MasterDataType dataType, params string[] inputs)
        {
            ViewBag.Title = ViewNamePlural;
            if (UploadFiles == null || UploadFiles.ContentLength < 1)
            {
                ShowError("No file selected");
                return View("Index");
            }
            System.Data.DataSet data = FileHelper.GetDataSetFromExcel(
                FileHelper.UploadFile(UploadFiles)
                );
            if (data != null && data.Tables.Count > 0)
            {
                MasterDataUpload.UploadStatusResult result = new MasterDataUpload(UserId).UploadFile(
                    data.Tables[0]
                    , dataType
                    , inputs
                    );

                switch (result.Status)
                {
                    case MasterDataUpload.UploadStatus.Success:
                        ShowSuccess("Data imported successfully");
                        break;
                    case MasterDataUpload.UploadStatus.FileIsEmpty:
                        ShowError("File is empty");
                        break;
                    case MasterDataUpload.UploadStatus.InvalidFileHeader:
                        ShowError("File does not contain the correct structure, kindly download the template then reupload it");
                        break;
                    case MasterDataUpload.UploadStatus.dbError_FailedtoImportData:
                        ShowError("Failed to upload the data, kindly contact administrator to check it.");
                        break;
                    case MasterDataUpload.UploadStatus.DataContainsErrors:
                        ShowError("Cannot upload the data, check below errors");
                        break;
                    default:
                        break;
                }
                return View("Index", result.failedReasons);
            }
            else
            {
                ShowError("File does not contain the correct structure, kindly download the template then reupload it");
                return View("Index");
            }
        }
        public FileResult DownloadFile()
        {
            System.Data.DataTable data = ToDataTable
                (new BaseService<DownloadModel>(UserId).Get().ToList(),
                FileDownloadExcludedColumns);

            FileContentResult file = new FileContentResult(
                 FileHelper.DatatableToBytes(data, SheetName)
                , MimeMapping.GetMimeMapping(FileName))
            {
                FileDownloadName = FileName
            };
            return file;
        }
        private static System.Data.DataTable ToDataTable(List<DownloadModel> data, List<string> excludedColumns)
        {
            System.ComponentModel.PropertyDescriptorCollection props =
                System.ComponentModel.TypeDescriptor.GetProperties(typeof(DownloadModel));
            System.Data.DataTable table = new System.Data.DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                System.ComponentModel.PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name);
            }
            object[] values = new object[props.Count];
            if (data != null)
                foreach (DownloadModel item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item);
                    }
                    table.Rows.Add(values);
                }
            if (excludedColumns != null)
                foreach (string exclude in excludedColumns)
                {
                    if (table.Columns.Contains(exclude))
                        table.Columns.RemoveAt(table.Columns.IndexOf(exclude));
                }

            return table;
        }

        protected static string ConvertToDBbitString(bool variable)
        {
            return variable ? "1" : "0";
        }
    }
}