﻿

using EnumsNET;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Services.Helper
{
    public static class EnumHelpers
    {
        public static string DescriptionAttr<T>(this T source)
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }
        public enum CipherCode
        {
            [Description("UserTokenPassword")]
            UserTokenPassword = 0,

            [Description("MerchantTokenPassword")] 
            MerchantTokenPassword = 1,

            [Description("ExpirationMaxPeriod")]
            ExpirationMaxPeriod = 2,

            [Description("QRCodePassword")]
            QRCodePassword
        }

        public enum SettingApp   
        {
            [Description("BaseFileUrl")]
            BaseFileUrl,

            [Description("UploadRoute")] 
            UploadRoute,

            [Description("DeleteRoute")]
            DeleteRoute
        }

        public enum QRErrors
        {
            [Description("InvalidOffer")]
            InvalidOffer = -2,

            [Description("InvalidToken")]
             InvalidToken
        }


        public enum OfferSortCriteria
        {
            [Description("Ending Soon")]
            EndingSoon = 1,
            [Description("Lowest Price")]
            LowestPrice = 2,
            [Description("Lowest Pieces")]
            LowestPieces = 3
        }

        public enum PromotionStatus
        {
            [Description("Draft")]
            Draft = 0,

            [Description("Pending")]
            Pending,

            [Description("Approved")]
            Approved,

            [Description("Rejected")]
            Rejected
        }
        public enum QRReturnType
        {
            [Description("CUST")]
            Customer = 0,

            [Description("POINT")]
            Offer
        }

        public enum Constants
        {
            [Description("Menus")]
            Menu = 0
        }

        public enum ReturnErrorCode {
            [Description("InvalidRequest")]
            InvalidRequest = 0,

            [Description("DBError")]
             DBError,

            [Description("DataInComplete")]
            DataInComplete,

            [Description("ConsumerNotFound")]
            ConsumerNotFound, 

            [Description("OfferNotFound")]
            OfferNotFound,

            [Description("NoPoints")]
            NoPoints,

            [Description("CipherError")]
            CipherError,
            
            [Description("InvalidAmount")]
            InvalidAmount,

            [Description("InvoiceNotFound")]
            InvoiceNotFound,

            [Description("InvalidType")]
            InvalidType
        }
    }
}
