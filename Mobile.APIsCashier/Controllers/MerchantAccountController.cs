﻿using Services.Mobile.Business;
using Services.Mobile.Business.Dto;
using Services.Mobile.Business.Merchant_Account.Dto;
using System.Linq;
using System.Web.Http;

namespace Mobile.APIsCashier
{
    public class MerchantAccountController : ApiController
    {
        MerchantAccountService Service = new MerchantAccountService();

        [HttpPost]
        public IHttpActionResult Login([FromBody] LoginCredentialsDto login)
        {
            if (ModelState.IsValid)
                return Json(Service.login(login));
            return Json(new RequestResult { Failed = true, ErrorCode = "NotSecure" });
        }

        

        [HttpPost]
        public IHttpActionResult Logout()
        {
            var userToken = Request.Headers.Authorization.Parameter;
            return Json( Service.logout(userToken)); 
        }
        [HttpPost]
        public IHttpActionResult ChangeLanguage([FromBody]ConsumerContract<string> lang)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(Service.updateLanguage(lang,userToken));
            return Json(new RequestResult { Failed = true, ErrorCode = "NotSupported" });

            //if (lang.value.Contains("en") || lang.value.Contains("ar"))
            //    return Json(new RequestResult { Failed = false });
            //return Json(new RequestResult { Failed = true, ErrorCode = "NotSupported" });
        }

    }

  
}
