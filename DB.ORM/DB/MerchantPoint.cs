namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MerchantPoint")]
    public partial class MerchantPoint
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MerchantPoint()
        {
            ConsumerRedeemedPoints = new HashSet<ConsumerRedeemedPoint>();
        }

        [Key]
        public int PointId { get; set; }

        public int MerchantId { get; set; }

        public int PointValue { get; set; }

        public string PointDescription { get; set; }

        public string PointDescriptionAr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerRedeemedPoint> ConsumerRedeemedPoints { get; set; }

        public virtual Merchant Merchant { get; set; }
    }
}
