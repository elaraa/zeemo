﻿
function RecordAction(gridId, recordId, Url, swal, SucessMessageHeader, SucessMessage, FailMessageHeader,
    FailMessage, Title, Text, Type, ConfirmButtonText, CancelButtonText,safe) {

    if (swal != 'False') {
        ConfirmationSWAL(gridId, recordId, Url, SucessMessageHeader, SucessMessage, FailMessageHeader,
            FailMessage, Title, Text, Type, ConfirmButtonText, CancelButtonText, safe);
    }

    return false;
}


ConfirmationSWAL = (gridId, recordId, Url, SucessMessageHeader, SucessMessage, FailMessageHeader,
    FailMessage, Title, Text, Type, ConfirmButtonText, CancelButtonText, safe) => {
    swal({
        title: Title,
        text: Text,
        type: Type,
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: ConfirmButtonText,
        cancelButtonText: CancelButtonText,
        closeOnConfirm: false,
        closeOnCancel: false
    },

        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: Url,
                    data: { id: parseInt(recordId) },
                    dataType: "json",
                    success: function (data) {
                        if (data == true) {
                            swal(SucessMessageHeader, SucessMessage, "success");
                            reloadControl("#" + gridId, "kendoGrid");
                        } else {
                            swal(FailMessageHeader, FailMessage, "error");
                        }
                    },
                    error: function (httpRequest, textStatus, errorThrown) { }
                });

            } else {
                swal(FailMessageHeader, safe, "error");
            }
        });
}