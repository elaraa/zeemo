namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Campaign
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionRunId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string PromotionName { get; set; }

        public string PromotionNameAr { get; set; }

        [Key]
        [Column(Order = 3)]
        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public int? RedeemCountPerPerson { get; set; }

        public int? NumberOfPoints { get; set; }

        public int? NumberOfPieces { get; set; }

        public int? NumberOfBranches { get; set; }

        public int? NumberOfCriteria { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(11)]
        public string Status { get; set; }
    }
}
