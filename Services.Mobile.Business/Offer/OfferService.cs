﻿using Services.Base;
using DB.ORM.DB;
using Services.Mobile.Business.Dto;
using System;
using Newtonsoft.Json;
using Services.Security;
using static Services.Helper.EnumHelpers;
using System.Linq;
using System.Collections.Generic;
using Services.Mobile.Business.Offer;
using System.Data.Entity.SqlServer;
using DB.ORM.DBsp_fn.Dto;

namespace Services.Mobile.Business
{
    public class OfferService : BaseService<PromotionRun>
    {
        
        private bool validateOffer(int offerId)
        {
            try
            {
                var offer = GetOne(c => c.PromotionRunId == offerId);
                if (offer.CloseDate == null && offer.StartDate <= DateTime.Now && offer.EndDate > DateTime.Now)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            };
        }
        public RequestResult getQR(int offerId, string userToken)
        {
            try
            {
                if (!validateOffer(offerId))
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest", Data = QRErrors.InvalidOffer };

                int consumerId = getTokenId(userToken);
                if (consumerId != -1)
                {
                    var data = JsonConvert.SerializeObject(new QRDto
                    {
                       OfferCode = offerId.ToString(),
                       ConsumerCode = consumerId.ToString(),
                       CurrentDate = DateTime.Now
                    });
                    return new RequestResult { Failed = false, Data = StringCipher.Encrypt(data, CipherCode.QRCodePassword)};
                }
                else
                    return new RequestResult
                             { Failed = true, ErrorCode = "InvalidRequest", Data = QRErrors.InvalidToken };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = "", ErrorCode = "DBError" };
            };
        }

        public ListResult<viewOfferDto> getList(ListParams @params, string userToken, object Header,int? interestId = null, string merchants = "", string offerTypes = "", int? sortBy = null, string locationId = "")
        {
            int consumerId = -1;
            try   
            {
                consumerId = getTokenId(userToken);
                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var lang = getLanguageByConsumer(consumerId);
                    var limit = @params != null ? @params.@params.limit : 100;
                    var start = (@params != null ? @params.@params.page - 1 : 0) * limit;
                    var lat = !string.IsNullOrWhiteSpace(locationId) && !locationId.Contains("undefined") ? locationId.Split(',')[0] : null;
                    var lng = !string.IsNullOrWhiteSpace(locationId) && !locationId.Contains("undefined") ? locationId.Split(',')[1] : null;
                    var merchantsIds = !string.IsNullOrWhiteSpace(merchants) ? merchants.Split(',').Select(int.Parse).ToArray() : null;
                    var offerTypesIds = !string.IsNullOrWhiteSpace(offerTypes) ? offerTypes.Split(',').Select(int.Parse).ToArray() : null;
                    
                    var offers = db.fn_GetOffers(consumerId, lat, lng).Where(a => (merchantsIds != null? merchantsIds.Contains( a.MerchantId) : true)
                               && (offerTypesIds != null ? offerTypesIds.Contains(a.PromotionTypeId) : true) && (interestId.HasValue ? a.InterestId == interestId : true))
                               .OrderBy(c=>c.OfferId).Skip(start).Take(limit).ToList();
                    var totalCount = offers.Count();
                    offers = sortBy == (int)OfferSortCriteria.EndingSoon ? offers.OrderBy(c => c.EndDate).ToList() :
                        (sortBy == (int)OfferSortCriteria.LowestPieces ? offers.OrderBy(c => c.NoOfPieces).ToList() : (offers.OrderByDescending(c => c.Savings).ToList()));
                    return new ListResult<viewOfferDto> 
                    {
                        dataHeader =Header,
                        page = @params.@params.page,
                        pageCount = totalCount / @params.@params.limit,
                        totalCount = totalCount,
                        data = offers.AsQueryable().Select(c=>new viewOfferDto {
                            BrandName =c.BrandName,
                            Duration=c.Duration,
                            Image=c.Image,
                            NoOfPieces =c.NoOfPieces,
                            OfferId=c.OfferId,
                            OfferName=lang=="en" ? c.OfferName : c.OfferNameAr,
                            OfferTypeName =c.OfferTypeName
                        }),
                        limit = @params.@params.limit
                    };
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }

        }

        public List<Tuple<int, string>> getSortBy()
        {
            var list = new List<Tuple<int, string>>
            {
                new Tuple<int, string>((int)OfferSortCriteria.EndingSoon, OfferSortCriteria.EndingSoon.DescriptionAttr()),
                new Tuple<int, string>((int)OfferSortCriteria.LowestPieces ,OfferSortCriteria.LowestPieces.DescriptionAttr()),
                new Tuple<int, string>((int)OfferSortCriteria.LowestPrice, OfferSortCriteria.LowestPrice.DescriptionAttr())
            };
            return list;
        }

        public RequestResult getLastOfferToRate(string userToken)
        {
            int consumerid = -1;
            try
            {
                consumerid = getTokenId(userToken);
                if (Any<Consumer>(w=>w.ConsumerId == consumerid ))
                {
                    var lang = getLanguageByConsumer(consumerid);
                    var lastOffer = db.fn_GetLastOfferToRate(consumerid);
                    if (lastOffer != null && lastOffer.Count() > 0)
                    {
                        var entity = lastOffer.FirstOrDefault();
                        return new RequestResult
                        {
                            Failed = false,
                            Data = new
                            {
                                id = entity.PromotionRunId,
                                name = (lang == "en" ? entity.PromotionName : entity.PromotionNameAr),
                                date = entity.CreationDate,
                                merchant = entity.MerchantName
                            }
                        };
                    }
                    else
                        return new RequestResult 
                        { Failed = false, Data = "", ErrorCode = ReturnErrorCode.OfferNotFound.DescriptionAttr() };

                }
                else
                    return new RequestResult { Failed = false, Data = "" ,ErrorCode=ReturnErrorCode.InvalidRequest.DescriptionAttr()};
            }
            catch (Exception e)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true,ErrorCode= ReturnErrorCode.DataInComplete.DescriptionAttr() };
            }
        }

        public RequestResult getOne(int id, string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var offer = GetOne<vw_PromotionOffers>(w => w.PromotionRunId == id);
                    var lang = getLanguageByConsumer(consumerId);
                    var imgs = Get<PromotionImage>(i => i.PromotionId == offer.PromotionId);
                    var images = imgs != null ? imgs.Select(s => GetBaseUrl + s.ImageUrl) : null;
                    var branches = Get<PromotionRunBranch>(i => i.PromotionRunId == offer.PromotionRunId).ToList();
                    var branchesIds = branches != null ? branches.Select(c => c.BranchId).ToList() : null;
                    var vwBranches = branchesIds != null ? Get<vw_Branch>(c => branchesIds.Contains(c.BranchId)).ToList() : null;
                    var isFav = Any<ConsumerWishList>(w => w.PromotionId == id && w.ConsumerId == consumerId);
                    return new RequestResult
                    {
                        Failed = false,
                        Data = new OfferDetailsDto
                                   {
                                       OfferId = offer.PromotionRunId,
                                       NoOfPieces = offer.NumberOfPieces,
                                       Description = lang=="en"? offer.Description : offer.DescriptionAr,
                                       Duration = offer.Duration ?? 0,
                                       OfferName = lang == "en" ? offer.PromotionName : offer.PromotionNameAr,
                                       OfferType = offer.PromotionTypeName,
                                       BrandName = offer.MerchantBrandName,
                                       Images = images,
                                       IsFav = isFav,
                                       ShareMessage = "Zeemo is active and Online on <a href=\"http://zeemo.mazanocorp.com/ABS123=\"></a>",
                                       Branches = vwBranches != null ? vwBranches.Select(c=> new BranchesDto { Address = c.Address, Lat = c.Lat.ToString(), Lng = c.Lng.ToString()}).ToList() : null
                        } 
                    };
                }
                else
                    return new RequestResult { Failed = true };
            }  
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true };
            }

       
            
        }
        
        public IQueryable<OfferTypeDto> getOfferTypes(string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var res = Get<PromotionType>(null, true);
                    if(res != null)
                     return res.Select(c=> new OfferTypeDto {
                        OfferTypeId = c.PromotionTypeId,
                        OfferTypeName = c.PromotionTypeName
                    });
                }
                return null;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
        }

        public IQueryable<OfferRateTypeDto> getOfferRateTypes(string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var lang = getLanguageByConsumer(consumerId);
                    var res =lang=="en" ? Get<PromotionRateType>().Select(a=>new OfferRateTypeDto {OfferRateTypeId= a.PromotionRateTypeId,OfferRateTypeName=a.PromotionRateType1 }) 
                        : Get<PromotionRateType>().Select(a => new OfferRateTypeDto {OfferRateTypeId = a.PromotionRateTypeId,OfferRateTypeName = a.PromotionRateTypeAr });

                    return res;
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
        }

        public void addToWishList(int id, string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                   
                    var existsOfferInWishList = GetOne<ConsumerWishList>(w => w.PromotionId == id && w.ConsumerId == consumerId);
                    if (existsOfferInWishList != null)
                        Delete<ConsumerWishList>(w => w.PromotionId == id && w.ConsumerId == consumerId);
                    else
                         Add(new ConsumerWishList { PromotionId = id, ConsumerId = consumerId });
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
        }

        public RequestResult getReportOfferReasonList(string userToken)
        {
            int consumerId = -1; 
            try
            {
                consumerId = getTokenId(userToken);
            
                if (consumerId !=-1)
                {
                    var lang = getLanguageByConsumer(consumerId);
                    return new RequestResult
                    {
                        Failed = false,
                        Data = lang=="en" ? Get<PromotionRateType>().Select(s => new VMReportOfferReasonDto { ReportOfferReasonId = s.PromotionRateTypeId, ReportOfferReasonText = s.PromotionRateType1 })
                                          : Get<PromotionRateType>().Select(s => new VMReportOfferReasonDto { ReportOfferReasonId = s.PromotionRateTypeId, ReportOfferReasonText = s.PromotionRateTypeAr })
                    };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true };
            }
        }

        public RequestResult rateOffer(ConsumerRateOfferDto Offer, string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);
                var consumer = GetOne<Consumer>(w => w.ConsumerId == consumerId);
                if (consumer != null)
                {
                    var offerEntity = GetOne<vw_Campaign>(a => a.PromotionRunId == Offer.Id);
                    if (offerEntity != null)
                    {
                        var valid = validateOffer(offerEntity.PromotionRunId);
                        if (valid)
                        {
                            Add(new PromotionRate
                            {
                                PromotionRunId=Offer.Id,
                                Rate= Convert.ToInt16(Offer.Rate),
                                RateTypeId=Offer.RateTypeId,
                                RateComment=Offer.RateComment,
                                ConsumerId=consumerId
                            });
                        }

                    }
                    return new RequestResult {Failed=true };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true };
            }
        }

        public RequestResult reportOffer(VMReportOfferDto Offer, string userToken)
        {
            int consumerId = -1; 
            try
            {
                consumerId = getTokenId(userToken);
                var consumer = GetOne<Consumer>(w => w.ConsumerId == consumerId);
                if (consumer != null)
                {
                            Add(new ConsumerReportPromotionRun
                            {
                                PromotionRunId = Offer.OfferId,
                                ReasonTypeId = Offer.ReportReasonId,
                                ReasonText = Offer.ReasonText,
                                ConsumerId = consumerId
                            });
                    return new RequestResult { Failed = false };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true };
            }
        }

    }


}