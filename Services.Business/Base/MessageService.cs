﻿using Services.Base.Base;

namespace Services.Web.Business
{
    public static class MessageService
    {
        public static RunResult ShowSuccessMessage(bool isCreate = false)
        {
            if (isCreate)
                return new RunResult { Succeeded = true, SuccessDesc = ZM.Message.SuccessCreateCode };
            else
                return new RunResult { Succeeded = true, SuccessDesc = ZM.Message.SuccessEditCode };
        }
    }
}
