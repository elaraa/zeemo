﻿using DB.ORM.DB;
using Services.Base;
using Services.Mobile.Business;
using Services.Mobile.Business.CashierMerchantOffer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static Services.Helper.EnumHelpers;

namespace Mobile.APIsCashier
{
    public class MerchantOfferController : ApiController
    {
        MerchantOfferService service = new MerchantOfferService();
        [HttpPost]
        public IHttpActionResult GetList([FromBody] ListParams @params)
        {

            var userToken = Request.Headers.Authorization.Parameter;
            var Header = Request.Headers;
            if (ModelState.IsValid)
                return Json(service.getList(@params, userToken, Header));
            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });

            //    using (var db = new DBModel())
            //    {
            //        var totalCount = db.vOffers.Count();
             
            //        return Json(new ListResult<vOffer>
            //        {
            //            dataHeader = Request.Headers,
            //            page = @params.@params.page,
            //            pageCount = totalCount / @params.@params.limit,
            //            totalCount = totalCount,
            //            data = db.vOffers.Where(w => w.BrandName == "Pharetra Company").OrderBy(o => o.OfferId).Skip((@params.@params.page - 1) * @params.@params.limit).Take(@params.@params.limit).ToList(),
            //            limit = @params.@params.limit
            //        });
            //    }
        }
    }
}
