﻿using DB.ORM.DB;
using Services.Base;
using Services.Web.Business;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using Web.App.Admin.Models;
using Web.App.Models;

namespace Web.App.Admin.Controllers
{
    public class RoleController : BaseControllerList<vw_SystemRole, SystemRole, int>
    {
        public RoleController()
        {
            ViewNamePlural = "Roles";
            ViewNameSingular = "Role";
        }
        protected override Expression<Func<SystemRole, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.SystemRoleId == id.Value) : (Expression<Func<SystemRole, bool>>)(x => 1 == 0);
        }
        protected override void InitTemplateServices()
        {
            ListService = new BaseService<vw_SystemRole>(UserId);
            ActionService = new RoleService(UserId);
        }
        public override ActionResult Create() {
            FillViewBag(true);
            return View(new VMRole { IsActive= true, IsAdmin = false });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VMRole model) {
            var err = "";
            if (ModelState.IsValid)
            {
                if (ActionService.Add(model.GetModel()))
                {
                    ShowSuccess(ZM.Message.SuccessCreateCode, true);
                    return RedirectToAction("Index");
                }
                else err = "Failed to save role. Try again later!";
            }
            err += GetModelStateErrors(ModelState);
            ShowError(err);
            FillViewBag(true);
            return View(model);
        }
        protected void FillViewBag(bool isCreate, string[] selectedMenuItems = null)
        {
            base.FillViewBag(isCreate);
            ViewBag.Menus = LookupHelper.GetAllMenus(this, selectedMenuItems);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var role = ActionService.GetOne(w => w.SystemRoleId == id,true,"SystemRoleMenus");
            if (role == null)
                return HttpNotFound();

            var model = new VMRole(role);
            FillViewBag(false, model.MenuItems.Split(new []{ ","},StringSplitOptions.RemoveEmptyEntries));
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VMRole model)
        {
            var err = "";
            if (ModelState.IsValid)
            {
                if (ActionService.Update(model.GetModel(), new Expression<Func<SystemRole, object>>[]{
                    x=>x.RoleName,
                    x => x.IsAdmin,
                    x => x.Active
                }))
                {
                    ShowSuccess(ZM.Message.SuccessEditCode, true);
                    return RedirectToAction("Index");
                }
                    
                else err = "Failed to save role. Try again later!";
            }
            err += GetModelStateErrors(ModelState);
            ShowError(err);
            FillViewBag(false, model.MenuItems.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries));
            return View(model);
        }
    }
}