﻿using AutoMapper;
using DB.ORM.DB;
using Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using static DB.ORM.DB.Consumer;
using static DB.ORM.DB.Interest;
using static DB.ORM.DB.Merchant;
using static DB.ORM.DB.SystemUser;
using Profile = DB.ORM.DB.Profile;

namespace Web.App.Admin
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitMapper();
        }

        private void InitMapper()
        {
            var baseUrl = new BaseService<SystemUser>().GetBaseUrl;

            Mapper.Initialize(cfg => {
                cfg.CreateMap<UserDto, SystemUser>()
                         .ForMember(d => d.UserId, s => s.MapFrom(src => src.Id));
                cfg.CreateMap<SystemUser, UserDto>()
                         .ForMember(d => d.Id, s => s.MapFrom(src => src.UserId))
                         .ForMember(d=>d.ConfirmPassword, s=>s.MapFrom(src=>src.Password))
                         .ForMember(d => d.Logo, s => s.MapFrom(src => baseUrl + src.UserLogo));

                cfg.CreateMap<MerchantDto, Merchant>().ForMember(d => d.MerchantId, s => s.MapFrom(src => src.Id));
                cfg.CreateMap<Merchant, MerchantDto>()
                         .ForMember(d => d.Id, s => s.MapFrom(src => src.MerchantId))
                         .ForMember(d => d.ConfirmPassword, s => s.MapFrom(src => src.Password))
                         .ForMember(d => d.Logo, s => s.MapFrom(src => baseUrl + src.MerchantLogo));

                cfg.CreateMap<ConsumerDto, Consumer>().ForMember(d => d.ConsumerId, s => s.MapFrom(src => src.Id));
                cfg.CreateMap<Consumer, ConsumerDto>()
                         .ForMember(d => d.Id, s => s.MapFrom(src => src.ConsumerId))
                         .ForMember(d => d.Logo, s => s.MapFrom(src => baseUrl + src.Photo));

                cfg.CreateMap<InterestDto, Interest>().ForMember(d => d.InterestId, s => s.MapFrom(src => src.Id));
                cfg.CreateMap<Interest, InterestDto>()
                         .ForMember(d => d.Id, s => s.MapFrom(src => src.InterestId))
                         .ForMember(d => d.Logo, s => s.MapFrom(src => baseUrl + src.Icon));

                cfg.CreateMap<Profile, SystemUser>().ForMember(d => d.UserId, s => s.MapFrom(src => src.Id));
                cfg.CreateMap<SystemUser, Profile>()
                     .ForMember(d => d.Id, s => s.MapFrom(src => src.UserId))
                     .ForMember(d => d.Pic, s => s.MapFrom(src => baseUrl + src.UserLogo))
                     .ForMember(d => d.FullName, s => s.MapFrom(src =>src.UserName));


            });
        }
    }
}
