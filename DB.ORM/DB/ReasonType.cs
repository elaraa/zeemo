namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReasonType")]
    public partial class ReasonType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ReasonType()
        {
            ConsumerReportPromotionRuns = new HashSet<ConsumerReportPromotionRun>();
        }

        public int ReasonTypeId { get; set; }

        public string ReasonTypeText { get; set; }

        public string ReasonTypeTextAr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerReportPromotionRun> ConsumerReportPromotionRuns { get; set; }
    }
}
