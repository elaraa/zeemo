﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.Dto
{ 
   public class LastOfferToRateDto
    {
       
        public int PromotionRunId { get; set; }
        public string PromotionName { get; set; }
        public string PromotionNameAr { get; set; }

        public string MerchantName { get; set; }
        public DateTime? CreationDate { get; set; }
  
    }
}
