﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Offer
{
    public class OfferDetailsDto
    {
        public int OfferId { get; set; }
        public string OfferName { get; set; }
        public string OfferType { get; set; }
        public int? NoOfPieces { get; set; }
        public int Duration { get; set; }
        public string Description { get; set; }
        public string BrandName { get; set; }
        public IQueryable<string> Images { get; set; }
        public bool IsFav { get; set; }
        public string ShareMessage { get; set; }
        public List<BranchesDto> Branches { get; set; }
    }
    public class BranchesDto
    {
        public string Address { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }

    }
}
