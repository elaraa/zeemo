namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PromotionRate")]
    public partial class PromotionRate
    {
        [Key]
        public long PromotionRunRateId { get; set; }

        public int PromotionRunId { get; set; }

        public int ConsumerId { get; set; }

        public short Rate { get; set; }

        public int? RateTypeId { get; set; }

        public string RateComment { get; set; }
    }
}
