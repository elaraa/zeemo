﻿
using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using Services.Repository;
using Services.Security;
using static Services.Helper.EnumHelpers;
using Services.Base.Base;

namespace Services.Base
{
    public class BaseService<TEntity> : GenericRepository<TEntity>
      where TEntity : class
    {
        private bool disposed = false;// to detect redundant calls
        private SecurityContext<TEntity> securityContext;
        protected DBModel _db;
        public int UserId
        { get => db.UserId; set => db.UserId = value; }

        public BaseService(int userId = int.MinValue) : base()
        {
            db = new DBModel
            {
                UserId = userId
            };
            db.Database.CommandTimeout = 0;
            securityContext = new SecurityContext<TEntity>(userId, ref db);
        }

        public int getTokenId(string userToken)
        {
            try
            {
                return Convert.ToInt32(StringCipher.Decrypt(userToken, CipherCode.UserTokenPassword));
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return -1;
            }
        }

        public string getLanguageByConsumer(int id)
        {
            try
            {
                return GetOne<Consumer>(a => a.ConsumerId == id).Language;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return "error";
            }
        }

        public string getCashierLanguage(int id) 
        {
            try
            {
                return GetOne<MerchantBranchCashier>(a => a.MerchantBranchCashierId == id).Language;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return "error";
            }
        }
        public string getMerchantCashierTokenId(string userToken) 
        {
            try 
            {
                var data = StringCipher.Decrypt(userToken, CipherCode.MerchantTokenPassword);
                
                return data;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return "";
            }
        }

     
        public string GetBaseUrl
        {
            get
            {
                try
                {
                    return Get<AppSetting>().AsEnumerable().FirstOrDefault(c => c.AppSettingCode == SettingApp.BaseFileUrl.DescriptionAttr()).AppSettingValue;
                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    return "";
                }
            }
        }

        public string GetUploadUrl
        {
            get
            {
                try
                {
                    return Get<AppSetting>().AsEnumerable().FirstOrDefault(c => c.AppSettingCode == SettingApp.UploadRoute.DescriptionAttr()).AppSettingValue;
                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    return "";
                }
            }
        }

        public string GetDeleteUrl
        {
            get
            {
                try
                {
                    return Get<AppSetting>().AsEnumerable().FirstOrDefault(c => c.AppSettingCode == SettingApp.DeleteRoute.DescriptionAttr()).AppSettingValue;
                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    return "";
                }
            }
        }

    }

    public class SecurityContext<TEntity>
        where TEntity : class
    {
        public int UserId { get; set; }
        private int? CountryId { get; set; }
        private DBModel DB { get; set; }
        public SecurityContext(int userId,ref DBModel dB )
        {
            UserId = userId;
            ///Get CountryId of Employee
            //CountryId = dB.Users.FirstOrDefault(w=>w.ID == userId).CountryId;
            //get countryId of user
            DB = dB;

        }
        public LambdaExpression GetContraints()
        {
            ///TODO: Add the extra conditions
           
            return null;
        }
       
    }
}
