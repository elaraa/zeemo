﻿

CREATE view [dbo].[vw_Branch]
as
select b.BranchId, b.AreaId, a.AreaName, a.AreaNameAr, b.Address, b.AddressAr, b.Lat, b.Lng, COUNT(c.MerchantBranchCashierId) AS Cashiers
from MerchantBranch b LEFT JOIN
dbo.MerchantBranchCashier c ON b.BranchId =c.BranchId JOIN
 Area a on b.AreaId = a.AreaId
 GROUP BY b.BranchId, b.AreaId, a.AreaName, a.AreaNameAr, b.Address, b.AddressAr, b.Lat, b.Lng