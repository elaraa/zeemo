using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Admin.Controllers
{
    public class FAQCategoryController : BaseControllerTemplate<FAQCategory, FAQCategory, int>
    {
       public FAQCategoryController() : base()
       {
            ViewNameSingular = "FAQCategory";
            ViewNamePlural = "FAQCategorys";
            PropertiesToUpdate = new Expression<Func<FAQCategory, object>>[]
           {
               x=>x.FaqCategoryId,
               x=>x.CategoryName

           };
       }
    protected override Expression<Func<FAQCategory, bool>> GetOneExpression(int? id)
    {
        return id.HasValue ? (x => x.FaqCategoryId == id.Value) : (Expression<Func<FAQCategory, bool>>)(x => 1 == 0);
    }
 }
}
