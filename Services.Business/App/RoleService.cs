﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services.Base
{
    public class RoleService: BaseService<SystemRole>
    {
        public RoleService(int userId):base(userId)
        {
        }

        public override bool Update(SystemRole obj, params Expression<Func<SystemRole, object>>[] propertiesToUpdate)
        {
            db.SystemRoleMenus.Where(w => w.RoleId == obj.SystemRoleId).DeleteFromQuery();
            db.SystemRoleMenus.AddRange(obj.SystemRoleMenus);
            obj.SystemRoleMenus = null;
            return base.Update(obj, propertiesToUpdate);
        }
        public override int Delete(Expression<Func<SystemRole, bool>> predicate)
        {
            var ms = db.SystemRoles.Where(predicate);
            (from mst in db.SystemRoleMenus
             join m in ms on mst.RoleId equals m.SystemRoleId
             select mst).DeleteFromQuery();
            ms.DeleteFromQuery();
            return db.SaveChanges();
        }
    }
}
