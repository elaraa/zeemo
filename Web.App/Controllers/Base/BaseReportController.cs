﻿using Microsoft.Reporting.WebForms;
using Services.Web.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Web.App.Controllers
{
    public class BaseReportController : BaseController
    {
        //    [HttpPost]
        //    [ValidateAntiForgeryToken]
        //    public ActionResult Index(VMReport model)
        //    {
        //        Response.Redirect(CompileUrl(model),true);
        //        return null;
        //        //return Redirect(CompileUrl(model));
        //        //return Json(CompileUrl(model));
        //    }
        //    protected string ReportName { get; set; }
        //    private string CompileUrl(VMReport model){

        //        model.createdBy = Session["UserFullName"].ToString();
        //        ReportSettings rs = new ReportSettings(UserId);
        //        model.imagesource = rs.ReportLogoUrl;
        //        //string.Format("{0}/Pages/ReportViewer.aspx?{1}{2}",reportserver,reportfolder,parameters);
        //        //foreach property in model, generate "&propertyname="propertyvalue
        //        string properties = "";
        //        foreach (System.ComponentModel.PropertyDescriptor prop in System.ComponentModel.TypeDescriptor.GetProperties(typeof(VMReport)))
        //            //table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        //            if(prop.GetValue(model) != null)
        //            properties += string.Format("&{0}={1}", prop.Name, prop.GetValue(model));

        //        return string.Format("{0}/Pages/ReportViewer.aspx?%2f{1}/{2}{3}", rs.ReportServer,rs.ReportFolder, ReportName, properties);
        //    }
        public BaseReportController():base()
        {
            ApplySecurity = false;
        }
        public ActionResult ReportContent(string reportName, Dictionary<string, string> pparameters)
        {
            ReportViewer reportViewer = new ReportViewer();
            ReportSettings rs = new ReportSettings();

            reportViewer.ProcessingMode = ProcessingMode.Remote;
            reportViewer.ServerReport.ReportPath = string.Format("/{0}/{1}", rs.ReportFolder, reportName);
            reportViewer.ServerReport.ReportServerUrl = new Uri(rs.ReportServer);
            reportViewer.Height = new Unit(400, UnitType.Pixel);
            reportViewer.Width = new Unit(100, UnitType.Percentage);

            pparameters.Add("imagesource", rs.ReportLogoUrl);
            pparameters.Add("createdBy", Session["UserFullName"].ToString());

            IEnumerable<ReportParameter> rps = new List<ReportParameter>();
            if (pparameters != null)
            {
                pparameters.Remove("action");
                pparameters.Remove("controller");
                rps = rps.Concat(pparameters.Where(v => !string.IsNullOrWhiteSpace(v.Value)).Select(v => new ReportParameter(v.Key, v.Value.Split(new char[] { ',' }))));
            }
            
            if (rps.Count() > 0)
                reportViewer.ServerReport.SetParameters(rps);

            return PartialView(reportViewer);
        }

    }
}