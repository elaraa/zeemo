﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Offer
{
  public class VMReportOfferReasonDto
    {
        public int ReportOfferReasonId { get; set; }
        public string ReportOfferReasonText { get; set; }
    }
}
