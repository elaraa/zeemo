﻿namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
     
    [MetadataType(typeof(MerchantBranchMetadataDto))]
    public partial class MerchantBranch
    {
        class MerchantBranchMetadataDto
        {
            [Key]
            public int BranchId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredAddress", ErrorMessageResourceType = typeof(ZM.Branch))]
            public string Address { get; set; }
        }
    }
}
