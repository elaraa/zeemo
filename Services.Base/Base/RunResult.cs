﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Base.Base
{
    public class RunResult
    {
        public bool Succeeded { get; set; } = false;
        public string ErrorDesc { get; set; }
        public string SuccessDesc { get; set; }
    }
}
