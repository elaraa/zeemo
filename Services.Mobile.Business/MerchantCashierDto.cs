﻿using System;

namespace Services.Mobile.Business.Dto
{
   public class MerchantCashierDto
    {
        public string MerchantID { get; set; }
        public string CashierID { get; set; }
    }
}
