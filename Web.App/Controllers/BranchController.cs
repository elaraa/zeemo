﻿using AutoMapper;
using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Web.Business;
using Services.Web.Business.Base;
using Services.Web.Business.Branch.Dto;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Web.Mvc;
using Web.App.Shared;
using System.Net;
using Services.Base;

namespace Web.App.Controllers
{
    public class BranchController : BaseControllerTemplate<vw_Branch, MerchantBranch, int>
    {
        UnitOfWork unitOfWork;
        BranchService branchService = new BranchService();
        public BranchController()
        {
            ViewNamePlural =ZM.Branch.ViewNamePlural ;
            ViewNameSingular = ZM.Branch.ViewNameSingular;

            if (SessionSetting.BranchUnitOfWork == null)
                SessionSetting.BranchUnitOfWork = branchService.InitiateUnitOfWork();
            else
                unitOfWork = SessionSetting.BranchUnitOfWork;

            PropertiesToUpdate = new Expression<Func<MerchantBranch, object>>[]
                {
                    x=>x.AreaId,
                    x=>x.Address,
                    x=>x.Lat,
                    x=>x.Lng
                };
        }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Areas = LookupHelper.GetAreas(this);
            base.FillViewBag(isCreate);
        }
        protected override Expression<Func<MerchantBranch, bool>> GetOneExpression(int? id) => id.HasValue ? (x => x.BranchId == id.Value) : (Expression<Func<MerchantBranch, bool>>)(x => 1 == 0);

        private void reinitiateUnitOfWork()
        {
            if(unitOfWork != null)
                unitOfWork.Rollback();

            unitOfWork = branchService.InitiateUnitOfWork();
            unitOfWork.Begin();
            SessionSetting.BranchUnitOfWork = unitOfWork;
        }

        public override ActionResult Index()
        {
            ViewBag.Title = ViewNamePlural;
            reinitiateUnitOfWork();
            return View();
        }

        public override ActionResult Create()
        {
            FillViewBag(true);

            decimal lat = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["DefaultLat"]);
            decimal lng = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["DefaultLng"]);

            reinitiateUnitOfWork();
            return View(new MerchantBranch { MerchantId = UserId, Lat = lat, Lng = lng });
        }
        public override ActionResult Edit(int? id)
        {
            FillViewBag(true);
            reinitiateUnitOfWork();

            var branch = unitOfWork.GetEntity(GetOneExpression(id), false, "MerchantBranchCashiers");
            return View(branch);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveBasicModel(MerchantBranch branch)
        {
            var _branch = unitOfWork.AddOrUpdateEntity<MerchantBranch>(branch);
            return Content(_branch.BranchId.ToString());
        }

        [HttpGet]
        public ActionResult SubmitModel()
        {
            var result = unitOfWork.Complete();
            if (result)
                ShowSuccess(ZM.Message.SuccessEditCode, true);
            else
                ShowError(ZM.Message.ErrorCode, true);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReadBranchCashiersList([DataSourceRequest] DataSourceRequest request)
        {
            var models = unitOfWork.GetEntities<MerchantBranchCashier>().ToList();
            models.ForEach(x => x.Password = LoginService.Decrypt(x.Password));

            return Json(Mapper.Map<List<MerchantBranchCashier>, List<BranchCashierDto>>(models).ToDataSourceResult(request));
        }

        [HttpPost]
        public void SaveCashier(MerchantBranchCashier model, string branchId)
        {
            FillViewBag(false);

            model.BranchId = int.Parse(branchId);
            model.Password = LoginService.Encrypt(model.Password);

            if (unitOfWork.GetEntity<MerchantBranchCashier>(x => x.LoginId == model.LoginId && x.Password == model.Password, true) == null ||
                             unitOfWork.GetEntity<MerchantBranchCashier>(x => x.LoginId == model.LoginId && x.Password == model.Password, false) == null)
            {
                model.Language = "en";
                unitOfWork.AddOrUpdateEntity(model);
            }
        }

        [HttpPost]
        public JsonResult DeleteCashier(MerchantBranchCashier model)
        {
            FillViewBag(false);
            if (model == null)
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));

            return Json(unitOfWork.RemoveOneEntity<MerchantBranchCashier>(x=>x.MerchantBranchCashierId == model.MerchantBranchCashierId), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteBranch(int? id) 
        {
            return Json(branchService.deleteBranch(id.Value), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult InArea(int areaId,decimal lat,decimal lng)
        {
            var res = branchService.inArea(areaId,lat,lng);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }
}