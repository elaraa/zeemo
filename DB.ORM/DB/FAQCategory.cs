namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FAQCategory")]
    public partial class FAQCategory
    {
        public int FaqCategoryId { get; set; }

        [Required]
        public string CategoryName { get; set; }
    }
}
