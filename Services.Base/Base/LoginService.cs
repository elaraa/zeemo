﻿using DB.ORM.DB;
using Services.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Services.Base
{
   
    public class LoginService
    {
        const string password = "Mazano@123";
        public enum AuthenticationResult
        {
            UserNameOrPasswordCannotBeEmpty = -1,
            UserDoesNotExist = -2,
            UserFound = 0,
            Error = -3
        }
        public class AuthResult
        {
            public AuthenticationResult Result { get; set; }
            public int UserId { get; set; }
            public string FullName { get; set; }
            public string FirstName
            {
                get
                {
                    if (!string.IsNullOrEmpty(FullName))
                    {
                        var s = FullName.Split(' ');
                        if (s.Length > 0)
                            return s[0];
                        return FullName;
                    }
                    return FullName;
                }
            }
            public string LastName
            {
                get
                {
                    if (!string.IsNullOrEmpty(FullName))
                    {
                        var s = FullName.Split(' ');
                        if (s.Length > 1)
                            return s[s.Length - 1];
                        return string.Empty;
                    }
                    return string.Empty;
                }
            }
            public string Picture { get; set; }
            public AuthResult()
            {
                Result = AuthenticationResult.Error;
                UserId = int.MinValue;
                FullName = string.Empty;
                Picture = string.Empty;
            }
        }

        public static string HashString(string str) {
            string hashedString = string.Empty;

            using (var sha512 = System.Security.Cryptography.SHA512.Create())
            {
                // Send a sample text to hash.  
                var hashedBytes = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(str));
                // Get the hashed string.  
                hashedString = BitConverter.ToString(hashedBytes).Replace("-", "");
            }
            return hashedString;
        }


        public static string Encrypt(string TextToEncrypt)
        {
            byte[] MyEncryptedArray = UTF8Encoding.UTF8
               .GetBytes(TextToEncrypt);

            MD5CryptoServiceProvider MyMD5CryptoService = new
               MD5CryptoServiceProvider();

            byte[] MysecurityKeyArray = MyMD5CryptoService.ComputeHash
               (UTF8Encoding.UTF8.GetBytes(password));

            MyMD5CryptoService.Clear();

            var MyTripleDESCryptoService = new
               TripleDESCryptoServiceProvider();

            MyTripleDESCryptoService.Key = MysecurityKeyArray;

            MyTripleDESCryptoService.Mode = CipherMode.ECB;

            MyTripleDESCryptoService.Padding = PaddingMode.PKCS7;

            var MyCrytpoTransform = MyTripleDESCryptoService
               .CreateEncryptor();

            byte[] MyresultArray = MyCrytpoTransform
               .TransformFinalBlock(MyEncryptedArray, 0,
               MyEncryptedArray.Length);

            MyTripleDESCryptoService.Clear();

            return Convert.ToBase64String(MyresultArray, 0,
               MyresultArray.Length);
        }



        public static string Decrypt(string TextToDecrypt)
        {
            try
            {
                byte[] MyDecryptArray = Convert.FromBase64String
                   (TextToDecrypt);

                MD5CryptoServiceProvider MyMD5CryptoService = new
                   MD5CryptoServiceProvider();

                byte[] MysecurityKeyArray = MyMD5CryptoService.ComputeHash
                   (UTF8Encoding.UTF8.GetBytes(password));

                MyMD5CryptoService.Clear();
                var MyTripleDESCryptoService = new TripleDESCryptoServiceProvider();
                MyTripleDESCryptoService.Key = MysecurityKeyArray;
                MyTripleDESCryptoService.Mode = CipherMode.ECB;
                MyTripleDESCryptoService.Padding = PaddingMode.PKCS7;
                var MyCrytpoTransform = MyTripleDESCryptoService.CreateDecryptor();

                byte[] MyresultArray = MyCrytpoTransform
                   .TransformFinalBlock(MyDecryptArray, 0,
                   MyDecryptArray.Length);

                MyTripleDESCryptoService.Clear();

                return UTF8Encoding.UTF8.GetString(MyresultArray);
            }
            catch (Exception e) {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return TextToDecrypt;
            }
        }
    

//public static string Encrypt(string message, string password = password)
//        {
//            // Encode message and password
//            byte[] messageBytes = ASCIIEncoding.ASCII.GetBytes(message);
//            byte[] passwordBytes = ASCIIEncoding.ASCII.GetBytes(password);

//            // Set encryption settings -- Use password for both key and init. vector
//            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
//            ICryptoTransform transform = provider.CreateEncryptor(passwordBytes, passwordBytes);
//            CryptoStreamMode mode = CryptoStreamMode.Write;

//            // Set up streams and encrypt
//            MemoryStream memStream = new MemoryStream();
//            CryptoStream cryptoStream = new CryptoStream(memStream, transform, mode);
//            cryptoStream.Write(messageBytes, 0, messageBytes.Length);
//            cryptoStream.FlushFinalBlock();

//            // Read the encrypted message from the memory stream
//            byte[] encryptedMessageBytes = new byte[memStream.Length];
//            memStream.Position = 0;
//            memStream.Read(encryptedMessageBytes, 0, encryptedMessageBytes.Length);

//            // Encode the encrypted message as base64 string
//            string encryptedMessage = Convert.ToBase64String(encryptedMessageBytes);

//            return encryptedMessage;
//        }

//        public static string Decrypt(string encryptedMessage, string password = password)
//        {
//            // Convert encrypted message and password to bytes
//            byte[] encryptedMessageBytes = Convert.FromBase64String(encryptedMessage);
//            byte[] passwordBytes = ASCIIEncoding.ASCII.GetBytes(password);

//            // Set encryption settings -- Use password for both key and init. vector
//            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
//            ICryptoTransform transform = provider.CreateDecryptor(passwordBytes, passwordBytes);
//            CryptoStreamMode mode = CryptoStreamMode.Write;

//            // Set up streams and decrypt
//            MemoryStream memStream = new MemoryStream();
//            CryptoStream cryptoStream = new CryptoStream(memStream, transform, mode);
//            cryptoStream.Write(encryptedMessageBytes, 0, encryptedMessageBytes.Length);
//            cryptoStream.FlushFinalBlock();

//            // Read decrypted message from memory stream
//            byte[] decryptedMessageBytes = new byte[memStream.Length];
//            memStream.Position = 0;
//            memStream.Read(decryptedMessageBytes, 0, decryptedMessageBytes.Length);

//            // Encode deencrypted binary data to base64 string
//            string message = ASCIIEncoding.ASCII.GetString(decryptedMessageBytes);

//            return message;
//        }
        //private LogFiles log = new LogFiles();
        public static AuthResult Authenticate(string userName, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(userName) || string.IsNullOrWhiteSpace(userName)
                    ||
                    string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password)
                    )
                    return new AuthResult { Result = AuthenticationResult.UserNameOrPasswordCannotBeEmpty };

                string hashedPassword = HashString(password);
                    var dbUser = new BaseService<Merchant>().GetOne(c => c.LoginId.ToLower().Trim() == userName.ToLower().Trim() && c.Password == hashedPassword);
                    if (dbUser != null)
                        return new AuthResult { Result = AuthenticationResult.UserFound, UserId = dbUser.MerchantId, FullName = string.Format("{0} {1}",dbUser.MerchantName, dbUser.MerchantBrandName), Picture = new BaseService<SystemUser>().GetBaseUrl + dbUser.MerchantLogo };
                

                return new AuthResult { Result = AuthenticationResult.UserDoesNotExist };
            }
            catch (Exception e)
            {
                //log.WriteToFile(log.logError, "getSystemUser", "LoginService", e.ToString());
                return new AuthResult();
            }
        }
        public static AuthResult AuthenticateAdmin(string userName, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(userName) || string.IsNullOrWhiteSpace(userName)
                    ||
                    string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password)
                    )
                    return new AuthResult { Result = AuthenticationResult.UserNameOrPasswordCannotBeEmpty };

                string hashedPassword = HashString(password);
               
                    var dbUser = new BaseService<SystemUser>().GetOne(c => c.LoginId.ToLower().Trim() == userName.ToLower().Trim() && c.Password == hashedPassword);
                    if (dbUser != null)
                        return new AuthResult { Result = AuthenticationResult.UserFound, UserId = dbUser.UserId, FullName = dbUser.UserName , Picture = new BaseService<SystemUser>().GetBaseUrl + dbUser.UserLogo};
                

                return new AuthResult { Result = AuthenticationResult.UserDoesNotExist };
            }
            catch (Exception e)
            {
                //log.WriteToFile(log.logError, "getSystemUser", "LoginService", e.ToString());
                return new AuthResult();
            }
        }
    }
}
