namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MerchantBranch")]
    public partial class MerchantBranch
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MerchantBranch()
        {
            MerchantBranchCashiers = new HashSet<MerchantBranchCashier>();
            PromotionRunBranches = new HashSet<PromotionRunBranch>();
        }

        [Key]
        public int BranchId { get; set; }

        public int MerchantId { get; set; }

        [Required]
        public string Address { get; set; }

        public string AddressAr { get; set; }

        public int AreaId { get; set; }

        public decimal Lat { get; set; }

        public decimal Lng { get; set; }

        public virtual Area Area { get; set; }

        public virtual Merchant Merchant { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantBranchCashier> MerchantBranchCashiers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PromotionRunBranch> PromotionRunBranches { get; set; }
    }
}
