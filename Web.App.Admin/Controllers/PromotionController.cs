﻿using DB.ORM.DB;
using Services.Base;
using Services.Web.Business.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Admin.Models;

namespace Web.App.Admin.Controllers
{
    public class PromotionController : BaseControllerList<vw_PromotionToCheck, Promotion, int>
    {
        PromotionService promotionService = new PromotionService();
        public PromotionController()
        {
            ViewNamePlural = "Promotions";
            ViewNameSingular = "Promotion";

            PropertiesToUpdate = new Expression<Func<Promotion, object>>[]
                {
                    x=>x.PromotionName,
                    x=>x.PromotionNameAr,
                    x=>x.PromotionTypeId,
                    x=>x.Description,
                    x=>x.DescriptionAr,
                    x=>x.IsDraft,
                    x=>x.ApprovedDate,
                    x=>x.RejectedDate,
                   
                };
        }
        protected override Expression<Func<Promotion, bool>> GetOneExpression(int? id) => id.HasValue ? (x => x.PromotionId == id.Value) : (Expression<Func<Promotion, bool>>)(x => 1 == 0);


        //move logic to service
        public ActionResult Edit(int id) { 
            FillViewBag(false);
            
            var dmodel = ListService.GetOne(w=>w.PromotionId == id);
            var baseURl = promotionService.GetBaseUrl;
            //var baseURL = ListService.Get<AppSetting>().AsQueryable().FirstOrDefault(a => a.AppSettingCode == SettingApp.BaseFileUrl.DescriptionAttr()).AppSettingValue;
            if (dmodel == null)
                return HttpNotFound();
            var dimodel = new BaseService<PromotionImage>(UserId).Get(w=>w.PromotionId == id,true).ToList();
            foreach (var item in dimodel)
            {
                item.ImageUrl = baseURl + item.ImageUrl;
            }
            return View(new VMPromotion{
                PromotionId = dmodel.PromotionId,
                PromotionName = dmodel.PromotionName,
                MerchantName = dmodel.MerchantName,
                OfferTypeName = dmodel.PromotionTypeName,
                Images = dimodel
            });
        }

        [HttpPost]
        public ActionResult Approve(int? id)
        {
            if (!id.HasValue)
                return View(false);

            return Json(promotionService.approve(id.Value),JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Reject(int? id)
        {
            if (!id.HasValue)
                return View(false);

            return Json(promotionService.reject(id.Value),JsonRequestBehavior.AllowGet);
        }
    }
}