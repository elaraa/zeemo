namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerReportPromotionRun")]
    public partial class ConsumerReportPromotionRun
    {
        [Key]
        public int ReportPromotionRunId { get; set; }

        public int PromotionRunId { get; set; }

        public int ConsumerId { get; set; }

        public int ReasonTypeId { get; set; }

        public string ReasonText { get; set; }

        public virtual Consumer Consumer { get; set; }

        public virtual PromotionRun PromotionRun { get; set; }

        public virtual ReasonType ReasonType { get; set; }
    }
}
