﻿using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Dto
{
    public class MerchantPoint
    {
        public int PointOrder { get; set; }
        public int PointValue { get; set; }
        public string ImageUrl { get; set; }
        public string PointDescription { get; set; }
        public bool IsCheched { get; set; }
    }
}
