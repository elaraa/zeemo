﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Interests
{
    public class InterestDto
    {
        public int InterestId { get; set; }
        public string InterestName { get; set; }
        public string Icon { get; set; } 
    }
}
