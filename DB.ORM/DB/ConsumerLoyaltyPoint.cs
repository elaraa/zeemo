namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerLoyaltyPoint")]
    public partial class ConsumerLoyaltyPoint
    {
        [Key]
        public int ConsumerPointId { get; set; }

        public int ConsumerId { get; set; }

        public int MerchantId { get; set; }

        public int Points { get; set; }

        public int? PromotionRunId { get; set; }

        public string InvoiceNumber { get; set; }

        public decimal? InvoiceTotalAmount { get; set; }

        public DateTime CreationDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime ActivationDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime ExpiryDate { get; set; }

        public int Remain { get; set; }
    }
}
