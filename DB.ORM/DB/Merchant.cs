namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Merchant")]
    public partial class Merchant
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Merchant()
        {
            ConsumerRedeemedPoints = new HashSet<ConsumerRedeemedPoint>();
            MerchantBranches = new HashSet<MerchantBranch>();
            MerchantCategories = new HashSet<MerchantCategory>();
            MerchantPoints = new HashSet<MerchantPoint>();
            Promotions = new HashSet<Promotion>();
        }

        public int MerchantId { get; set; }

        [Required]
        public string MerchantName { get; set; }

        [Required]
        public string MerchantBrandName { get; set; }

        public string MerchantLogo { get; set; }

        public int InterestId { get; set; }

        public string LoginId { get; set; }

        public string Password { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        [StringLength(20)]
        public string Mobile { get; set; }

        public string Website { get; set; }

        public string Facebook { get; set; }

        public bool HasLoyalty { get; set; }

        public decimal? PointsAmount { get; set; }

        public decimal? Pounds { get; set; }

        public int? PointsActivationDuration { get; set; }

        public int? PointsExpirationDuration { get; set; }

        [StringLength(1)]
        public string Membership { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerRedeemedPoint> ConsumerRedeemedPoints { get; set; }

        public virtual Interest Interest { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantBranch> MerchantBranches { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantCategory> MerchantCategories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantPoint> MerchantPoints { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Promotion> Promotions { get; set; }
    }
}
