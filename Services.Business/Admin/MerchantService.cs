﻿using DB.ORM.DB; 
using System;
using Services.Base;
using System.Web;
using System.Linq;
using static Services.Helper.EnumHelpers;
using Web.Helpers.File;
using static DB.ORM.DB.SystemUser;
using Services.Base.Base;
using AutoMapper;
using static DB.ORM.DB.Merchant;

namespace Services.Web.Business.Admin
{
    public class MerchantService : BaseService<MerchantDto>
    {
        public MerchantService(){
            
        }

        public override RunResult AddOrUpdate(MerchantDto merchant, bool isCreate)
        {   
            try
            {
                if (Any<Merchant>(c => c.MerchantId != merchant.Id && c.LoginId == merchant.LoginId))
                    return new RunResult { ErrorDesc = ZM.Message.DuplicatedLoginName };

                if (!string.IsNullOrEmpty(merchant.Password) && merchant.Id == 0)
                    merchant.Password = LoginService.HashString(merchant.Password);

                var systemMerchant = AddOrUpdate(Mapper.Map<Merchant>(merchant));
                if (systemMerchant != null)
                {
                    var baseUploadUrl = "uploads/" + ZM.Uploads.Merchant + '/' + systemMerchant.MerchantId.ToString() + '/';
                    if (merchant.PictureFile != null && merchant.PictureFile.ContentLength > 0)
                    {
                        var isUploaded = FileHelper.UploadFiles(ZM.Uploads.Merchant, systemMerchant.MerchantId.ToString(), GetBaseUrl, GetUploadUrl, merchant.PictureFile);
                        if (isUploaded)
                        {
                            FileHelper.DeleteFiles(systemMerchant.MerchantLogo, GetBaseUrl, GetDeleteUrl);
                            systemMerchant.MerchantLogo = baseUploadUrl + merchant.PictureFile.FileName;
                            AddOrUpdate(systemMerchant);
                        }
                    }
                    return MessageService.ShowSuccessMessage(isCreate);
                }

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            return new RunResult { ErrorDesc = ZM.Message.ErrorCode };
        }
    }
}
