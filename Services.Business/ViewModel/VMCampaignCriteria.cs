﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.App.Models
{
    [AutoMapTo(typeof(DB.ORM.DB.PromotionRunCriteria))]
    public class VMCampaignCriteria
    {
        public int PromotionRunCriteriaId { get; set; }
        public int PromotionRunId { get; set; }
        public int? AgeFrom { get; set; }
        public int? AgeTo { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

    }
}