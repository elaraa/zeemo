﻿using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Dto
{
    public class ConsumerRegistration
    {
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public DateTime Birtdate { get; set; }
        public string ReferralCode { get; set; }
        public string Language { get; set; }
    }
}
