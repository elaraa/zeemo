﻿using DB.ORM.DB;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Services.Web.Business;
using Services.Web.Business.Base;
using Services.Web.Business.Campaign.Dto;
using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using Web.App.Shared;

namespace Web.App.Controllers
{
    public class CampaignController : BaseControllerList<vw_Campaign, PromotionRun, int>
    {
        CampaignService campaignService;
        public CampaignController()
        {
            ViewNamePlural =ZM.Campaign.ViewNamePlural ;
            ViewNameSingular = ZM.Campaign.ViewNameSingular;

            campaignService = new CampaignService();
            PropertiesToUpdate = new Expression<Func<PromotionRun, object>>[]
                {
                    x=>x.PromotionId,
                    x=>x.StartDate,
                    x=>x.EndDate,
                    x=>x.CloseDate,
                    x=>x.RedeemCountPerPerson,
                    x=>x.NumberOfPieces,
                    x=>x.NumberOfPoints
                };
        }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Promotions = LookupHelper.GetPromotions(this);
            ViewBag.Genders = LookupHelper.GetGenders(this);
            base.FillViewBag(isCreate);
        }
        protected override Expression<Func<PromotionRun, bool>> GetOneExpression(int? id)
                       => id.HasValue ? (x => x.PromotionRunId == id.Value) : 
                                    (Expression<Func<PromotionRun, bool>>)(x => 1 == 0);

        //public ActionResult ReadCriteriaList([DataSourceRequest] DataSourceRequest request)
        //{
        //    return Json(campaignService.ReadCriteriaData(unitOfWork).ToDataSourceResult(request));
        //}
        public ActionResult ReadBranchesList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(campaignService.ReadBranches(UserId).ToDataSourceResult(request));
        }

        public override ActionResult Create()
        {
            FillViewBag(true);
            return View(new CampaignDto() { });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CampaignDto model)
        {
            FillViewBag(false);
            var result = campaignService.SaveModel(model);
            if (result)
            {
                ShowSuccess(ZM.Message.SuccessCreateCode,true);
                return RedirectToAction("Index");
            }
            ShowError(ZM.Message.ErrorCode);
            return View(model);
        }

        public ActionResult CopyCampaign(int? id)
        {
            FillViewBag(true);
            var model = campaignService.copyCampaign(id.Value, UserId);
            return View("Create", model);
        }

        public ActionResult ViewCampaign(int? id)
        {
            FillViewBag(true);
            var model = campaignService.viewCampaign(id.Value, UserId);

            return View("Details", model);
        }

        [HttpPost]
        public ActionResult CloseCampaign(int? id)
        { 
            FillViewBag(false);
            return Json(campaignService.closeCampaign(id.Value), JsonRequestBehavior.AllowGet);
        }

        [HttpPost] 
        public JsonResult DeleteCampiagn(int? id)
        {

            return Json(campaignService.deleteCampiagn(id.Value), JsonRequestBehavior.AllowGet);

        }

        // will be fixed 
        [HttpPost]
        public JsonResult NoAction(dynamic x)
        { return Json(true, JsonRequestBehavior.AllowGet); }
    }
}