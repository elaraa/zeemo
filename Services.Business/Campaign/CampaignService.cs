﻿using DB.ORM.DB;
using DB.ORM.Dto;
using Microsoft.AspNet.Identity;
using Services.Web.Business.Base;
using Services.Web.Business.Dto;
using Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Base;
using Services.Web.Business.Campaign.Dto;
using AutoMapper;

namespace Services.Web.Business
{
    public class CampaignService : BaseService<PromotionRun>
    {
        public CampaignService()
        {
        }

        public UnitOfWork InitiateUnitOfWork()
        {
            DBModel db = new DBModel();
            var unitOfWork = new UnitOfWork(db, new GenericRepository<PromotionRun>(db), new GenericRepository<PromotionRunBranch>(db),
                                                      new GenericRepository<PromotionRunCriteria>(db));
            return unitOfWork;
        }

        //public IQueryable<PromotionRunCriteria> ReadCriteriaData(UnitOfWork unitOfWork)
        //{
        //    return unitOfWork.GetEntities<PromotionRunCriteria>();
        //}

        public IQueryable<vw_CampaignBranches> ReadBranches(int merchantId)
        {
            var result = db.vw_CampaignBranches.Where(c => c.MerchantId == merchantId);
            return result;
        }

        public CampaignDto copyCampaign(int id, int merchantId)
        {
            try
            {
                var model =  getCampaign(id, merchantId);
                var days = (model.EndDate.Value - model.StartDate).TotalDays;
                model.StartDate = DateTime.Now;
                model.EndDate = model.StartDate.AddDays(days);
               
                return model;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new CampaignDto();
            }
        }

        public CampaignDto viewCampaign(int id, int merchantId)
        {
            return getCampaign(id,  merchantId);
        }

        public bool closeCampaign(int id) {
            try
            {
                var promotionRun = GetOne(x => x.PromotionRunId == id);
                promotionRun.CloseDate = DateTime.Now;
                AddOrUpdate(promotionRun);
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        private CampaignDto getCampaign(int id, int merchantId)
        {
            try
            {
                var promotionRun = GetOne(x => x.PromotionRunId == id);
                var model = Mapper.Map<CampaignDto>(promotionRun);
                var branches = db.fn_GetSelectedBranches(id, merchantId).ToList();
                model.Branches = Mapper.Map<List<DB.ORM.Dto.CampaignBranchDto>, List<Campaign.Dto.CampaignBranchDto>>(branches);
                model.CampaignCriteria = Mapper.Map<List<PromotionRunCriteria>,
                                  List<CampaignCriteriaDto>>(promotionRun.PromotionRunCriterias.ToList());
                foreach (var item in model.CampaignCriteria)
                    item.Gender = item.Gender.Contains("M") ? ZM.Campaign.Male : ZM.Campaign.Female;

                return model;
            }
            catch (Exception e) {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new CampaignDto();
            }
        }

        public bool SaveModel(CampaignDto model)
        {
            var unitOfWork = InitiateUnitOfWork();
            try
            {
                var campaign = unitOfWork.AddEntity(Mapper.Map<PromotionRun>(model));
                foreach (var branch in model.Branches)
                    if (branch.Selected == 1)
                        unitOfWork.AddEntity(new PromotionRunBranch
                        { BranchId = branch.BranchId, PromotionRunId = campaign.PromotionRunId });
                if (model.CampaignCriteria != null)
                {
                    foreach (var criteria in model.CampaignCriteria)
                    {
                        if (criteria.AgeTo == null || criteria.AgeFrom == null || criteria.Gender == "")
                            continue;
                        
                        criteria.PromotionRunId = campaign.PromotionRunId;
                        criteria.Gender = criteria.Gender != null ? criteria.Gender.Substring(0, 1) : null;
                        unitOfWork.AddEntity(Mapper.Map<PromotionRunCriteria>(criteria));
                    }
                }
               

                unitOfWork.Complete();
                
            }
            catch (Exception e) { unitOfWork.Rollback(); return false; }
            return true;
        }

        public bool deleteCampiagn(int id)
        {
            try 
            {
                var entity = GetOne(x => x.PromotionRunId == id);
                if (entity != null)
                {
                    if (!Any<ConsumerRedeemedPromotionRun>(a=>a.PromotionRunId==entity.PromotionRunId))
                    {
                        Delete(a => a.PromotionRunId == entity.PromotionRunId);
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception)
            {

                return false;
            }
        }

    }
}