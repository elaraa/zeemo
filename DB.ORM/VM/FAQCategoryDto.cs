﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(FAQCategoryMetaDataDto))]
    public partial class FAQCategory 
    {
        class FAQCategoryMetaDataDto
        {
            [Required(ErrorMessageResourceName = "RequiredCategoryName", ErrorMessageResourceType = typeof(ZM.FAQAdmin))]
            public string CategoryName { get; set; }
        }
    }
}
