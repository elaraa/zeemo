﻿using AutoMapper;
using DB.ORM.DB;
using Services.Mobile.Business.Dto;
using System.Web.Http;

namespace Mobile.APIs
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            Mapper.Initialize(cfg => {
                cfg.CreateMap<MerchantAccountDto, Merchant>();
                cfg.CreateMap<Merchant, MerchantAccountDto>();
            });
        }
    }
}
