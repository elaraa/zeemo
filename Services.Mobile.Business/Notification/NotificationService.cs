﻿using DB.ORM.DB;
using Services.Base;
using System;
using System.Linq;


namespace Services.Mobile.Business.Notification
{
    public class NotificationService:BaseService<vConsumerNotification>
    {
        public ListResult<vConsumerNotification> GetConsumerNotifications(ListParams @params ,object header,string userToken)
        {
            int consumerId = -1;
            try
            {

                consumerId = getTokenId(userToken);
                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var totalCount = db.vConsumerNotifications.Count();

                    return new ListResult<vConsumerNotification>
                    {
                        dataHeader = header,
                        page = @params.@params.page,
                        pageCount = totalCount / @params.@params.limit,
                        totalCount = totalCount,
                        data = db.vConsumerNotifications.OrderBy(o => o.CreationDate).Skip((@params.@params.page - 1) * @params.@params.limit).Take(@params.@params.limit),
                        limit = @params.@params.limit
                    };
                }
                else
                    return null;
            
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
        }

        public int? GetCount(string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);
                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var count= Get(c => c.IsRead == false).Count();
                    return count;
                }
                else
                    return null;
            }
            catch (Exception e)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
             
        }

    }
}
