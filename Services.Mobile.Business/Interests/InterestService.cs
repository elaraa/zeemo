﻿using Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB.ORM.DB;
using static Services.Helper.EnumHelpers;

namespace Services.Mobile.Business.Interests
{
   public class InterestService : BaseService<ConsumerInterest>
    {
        public IQueryable<InterestDto> allInterst(string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);
                
                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var lang = getLanguageByConsumer(consumerId);
                    //var intersts =lang=="en"? db.Interests.Select(a => new InterestDto{InterestId= a.InterestId,InterestName= a.InterestName,Icon= a.Icon }).ToList()
                    //    : db.Interests.Select(a => new InterestDto { InterestId = a.InterestId, InterestName = a.InterestNameAr, Icon = a.Icon }).ToList();
                    var intersts = lang == "en" ? Get<Interest>().Select(a=>new InterestDto{ InterestId=a.InterestId,InterestName=a.InterestName,Icon= GetBaseUrl + a.Icon})
                        : Get<Interest>().Select(a => new InterestDto { InterestId = a.InterestId, InterestName = a.InterestNameAr, Icon = a.Icon });
                    return intersts;
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }

        }

        public IEnumerable<int> allInterestIds(string userToken)
        {
            //return db.ConsumerInterests.Select(s => s.InterestId).Distinct();
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var Interests = db.ConsumerInterests;
                    if (Interests.Count() > 0)
                        return Interests.Select(s => s.InterestId).Distinct();
                    else
                        return null;
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
        }

        public IQueryable<InterestDto> getInterstBasedOnConsumerInterst(string userToken)  
        { 
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var lang = getLanguageByConsumer(consumerId);
                    var CIList = Get().Select(c => c.InterestId).Distinct();
                    
                    var intersts = lang=="en" ? Get<Interest>(i => CIList.Contains(i.InterestId))
                        .Select(a => new InterestDto { InterestId = a.InterestId, InterestName = a.InterestName, Icon = a.Icon })
                        : Get<Interest>(i => CIList.Contains(i.InterestId)).Select(a => new InterestDto { InterestId = a.InterestId, InterestName = a.InterestNameAr, Icon = GetBaseUrl + a.Icon });
                    return intersts;
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }

        }

        public RequestResult saveInterest(int[] interests, string userToken)
        { 
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);
                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    Delete(w => w.ConsumerId == consumerId);
                  
                    foreach (var item in interests)
                    {
                        Add<ConsumerInterest>(new ConsumerInterest { InterestId = item, ConsumerId = consumerId });
                    }
                    return new RequestResult { Failed = false };
                }
                else
                 return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DBError.DescriptionAttr() };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.CipherError.DescriptionAttr() };
            }
        }
    }
}
