﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Base
{
    public class ListResult<T>
           where T : class
    {
        public int page { get; set; }
        public int pageCount { get; set; }
        public int totalCount { get; set; }
        public IQueryable<T> data { get; set; }
        public int limit { get; set; }
        public object dataHeader { get; set; }

    }
    
}
