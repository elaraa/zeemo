﻿using AutoMapper;
using DB.ORM.DB;
using DB.ORM.Dto;
using Services.Base;
using Services.Mobile.Business.Dto;
using System;
using System.Data.Entity;
using System.Linq;
using static Services.Helper.EnumHelpers;

namespace Services.Mobile.Business
{
    public class MerchantService : BaseService<Merchant>
    {
        public ListResult<MerchantDto> getMechants(ListParams @params, object headers, string userToken)
        {
            try
            {
                int consumerId = getTokenId(userToken);
                if (consumerId != -1)
                {
                    var totalCount = db.Merchants.Count();
                    return new ListResult<MerchantDto>
                    {
                        dataHeader = headers,
                        page = @params.@params.page,
                        pageCount = totalCount / @params.@params.limit,
                        totalCount = totalCount,
                        data = db.fn_GetMerchants(consumerId).OrderBy(o => o.MerchantId)
                                           .Skip((@params.@params.page - 1) * @params.@params.limit).Take(@params.@params.limit),
                        limit = @params.@params.limit
                    };
                }
             }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            return null;
        }
        public RequestResult getMechant(int merchantId, string userToken)
        {
            try
            {
                int consumerId = getTokenId(userToken);
                if (consumerId != -1) { 
                    return new RequestResult
                    {
                        Failed = false,
                        Data = db.fn_GetMerchants(consumerId).FirstOrDefault(c => c.MerchantId == merchantId)
                    };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.InvalidRequest.DescriptionAttr() };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() };
            };
        }

        public RequestResult followMerchant(int merchantId, string userToken)
        {
            try
            {
                int consumerId = getTokenId(userToken);
                if (consumerId != -1)
                {
                    var stillFollowing = Any<MerchantFollower>(w => w.MerchantId == merchantId && w.ConsumerId == consumerId);
                    if (stillFollowing)
                        DeleteOne<MerchantFollower>(w => w.MerchantId == merchantId && w.ConsumerId == consumerId);
                    else
                        Add(new MerchantFollower { ConsumerId = consumerId, MerchantId = merchantId });

                    return new RequestResult { Failed = false };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.InvalidRequest.DescriptionAttr() };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() };
            };
        }
        public RequestResult getMerchantBranches(int merchantId, string userToken)
        {
            try
            {
                int consumerId = getTokenId(userToken);
                if (consumerId != -1)
                {
                  return new RequestResult
                    {
                        Failed = false,
                        Data = Get<MerchantBranch>(c=>c.MerchantId == merchantId).Select(c=> new MerchantBranchesDto { 
                             Label = c.Address,
                             Lat = (double) c.Lat,
                             Lng = (double) c.Lng
                        })
                    };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.InvalidRequest.DescriptionAttr() };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() };
            }

        }

        public RequestResult getMerchantsByInterest(int interestId, string userToken)
        {
            try 
            {
                int consumerId = getTokenId(userToken);
                if (consumerId != -1)
                {
                    return new RequestResult
                    {
                        Failed = false,
                        Data = Get(w => w.InterestId == interestId).Select(s => new InterestMerchantsDto
                        { MerchantId = s.MerchantId, MerchantName = s.MerchantName, MerchantLogo = GetBaseUrl + s.MerchantLogo })
                    };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.InvalidRequest.DescriptionAttr() };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() };
            }
        }
        
        public RequestResult getMerchantPointsMap(int id , string userToken)
        {
            try 
            {
                int consumerId = getTokenId(userToken);
                if (consumerId != -1)
                {
                    var lang = getLanguageByConsumer(consumerId);
                    var pointMapRemain = Get<vw_MerchantPointMap>(a => a.MerchantId == id && a.ConsumerId == consumerId);
                    var pointMapSavin = Get<vw_MerchantPointMap>(a => a.MerchantId == id && a.ConsumerId == consumerId);
                    // check null before and aggregation or select functions
                    var remain = pointMapRemain != null ? pointMapRemain.Select(a => a.Remain).Sum() : 0;
                    var saving= pointMapSavin!=null ? pointMapSavin.Select(a => a.Remain).Sum() : 0;

                    var expiredDuration = Get<AppSetting>().AsEnumerable().FirstOrDefault(c => c.AppSettingCode == CipherCode.ExpirationMaxPeriod.DescriptionAttr()).AppSettingValue;
                    int maxExpiredDuration = int.Parse(expiredDuration);  //check null or not
                    return new RequestResult
                    {
                        Failed = false,
                        Data = new
                        {
                            EarnedPoints = remain,
                            SavedValue = saving,
                            data = db.fn_GetMerchantPoints(id, remain).Select(a=>new {a.PointOrder,a.PointValue,a.IsChecked, Description=(lang == "en") ? a.PointDescription:a.PointDescriptionAr }).ToList(),
                            ExpiredPointDuration = Get<vw_MerchantPointMap>(a => a.MerchantId == id && a.ConsumerId == consumerId && DbFunctions.DiffDays(DateTime.Now,a.ExpiryDate) <= maxExpiredDuration)
                                   .FirstOrDefault()
                        }
                    };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.InvalidRequest.DescriptionAttr() };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() };
            }
        }

       
    }
}
