﻿using DB.ORM.DB;
using Microsoft.AspNet.Identity;
using Services.Base;
using Services.Repository;
using Services.Web.Business.Base;
using Services.Web.Business.Branch.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Web.Business
{
    public class BranchService : BaseService<MerchantBranch>
    {
        public BranchService()
        {
        }

        public UnitOfWork InitiateUnitOfWork()
        {
            DBModel db = new DBModel();
            var unitOfWork = new UnitOfWork(db, new GenericRepository<MerchantBranch>(db),
                                                      new GenericRepository<MerchantBranchCashier>(db));
            return unitOfWork;
        }

        public bool deleteBranch(int id)
        {
            try
            {
                var entity = GetOne(x => x.BranchId == id);
                if (entity != null)
                {
                    if (!Any<MerchantBranchCashier>(a => a.BranchId == entity.BranchId))
                    {
                        Delete(a => a.BranchId == entity.BranchId);
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool inArea(int areaId, decimal lat, decimal lng)
        {
            var area = GetOne<Area>(a => a.AreaId == areaId);
            var res = db.fn_InGeoArea(area.Lat.ToString(),area.Lng.ToString(),lat.ToString(),lng.ToString());
            return res;
        }

    }
}
