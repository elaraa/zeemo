namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerRedeemedPromotionRun")]
    public partial class ConsumerRedeemedPromotionRun
    {
        [Key]
        public int ConsumerRedeemedPromotionId { get; set; }

        public int ConsumerId { get; set; }

        public int PromotionRunId { get; set; }

        public DateTime? CreationDate { get; set; }
    }
}
