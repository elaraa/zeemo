﻿using DB.ORM.DB;
using Services.Base;
using Services.Mobile.Business.Dto;
using System;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using Services.Security;
using static Services.Helper.EnumHelpers;
using Newtonsoft.Json;
using System.Net.Http;
using Services.Web.Business.Base;

namespace Services.Mobile.Business.FileManagment
{
   public class FileManagmentService : BaseService<PromotionImage>
   {

        public bool uploadFile(MultipartFormDataStreamProvider provider,string root)
        {
            try 
            {
                foreach (var file in provider.FileData)
                {
                    var name = file.Headers.ContentDisposition.FileName;

                    var module = file.Headers.ContentDisposition.Name;
                    // remove double quotes from string.
                    module = module.Trim('"');
                    name = name.Trim('"');

                    var localFileName = file.LocalFileName;
                    root = root + "\\" + module;

                    if (!Directory.Exists(root))
                        Directory.CreateDirectory(root);

                    var filePath = Path.Combine(root, name);

                    File.Move(localFileName, filePath);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
            return false;
        }



        public bool deleteFile(string url)
        {
            try
            {
               // int id = int.Parse(Id);
               // var e = Get().AsQueryable().FirstOrDefault(a => a.PromotionImageId == id);
               // var entit = GetOne(a => a.PromotionImageId == id);  //  ==> tack long time to retreive data from DB
                if (url != "")
                {
                   // var path = entit.ImageUrl;
                    var oldPath = HttpContext.Current.Server.MapPath("~/" + url);
                    File.Delete(oldPath);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

    }
}
