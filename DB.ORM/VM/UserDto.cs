﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DB.ORM.DB
{
    [MetadataType(typeof(UserMetaDataDto))]
    public partial class SystemUser
    {
        class UserMetaDataDto
        {
            [Required(ErrorMessageResourceName = "RequiredUserName", ErrorMessageResourceType = typeof(ZM.UserAdmin))]
            public string UserName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredLoginName", ErrorMessageResourceType = typeof(ZM.UserAdmin))]
            public string LoginId { get; set; }

            [Required(ErrorMessageResourceName = "RequiredPassword", ErrorMessageResourceType = typeof(ZM.UserAdmin))]
            [DataType(DataType.Password)]
            public string Password { get; set; }
        }
        [NotMapped]
        public class UserDto : SystemUser
        {
            public int Id { get; set; }

            [DataType(DataType.Password)]
            [Compare("Password", ErrorMessageResourceName = "MissMatching", ErrorMessageResourceType = typeof(ZM.UI))]
            public string ConfirmPassword { get; set; }

            public string Logo { get; set; }

            public HttpPostedFileBase PictureFile { get; set; }
        }

    }
}
