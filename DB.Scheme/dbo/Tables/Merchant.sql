﻿CREATE TABLE [dbo].[Merchant] (
    [MerchantId]               INT              IDENTITY (1, 1) NOT NULL,
    [MerchantName]             NVARCHAR (MAX)   NOT NULL,
    [MerchantBrandName]        NVARCHAR (MAX)   NOT NULL,
    [MerchantLogo]             NVARCHAR (MAX)   NULL,
    [InterestId]               INT              NOT NULL,
    [LoginId]                  NVARCHAR (MAX)   NULL,
    [Password]                 NVARCHAR (MAX)   NULL,
    [Address]                  NVARCHAR (MAX)   NULL,
    [Email]                    NVARCHAR (MAX)   NULL,
    [Mobile]                   NVARCHAR (20)    NULL,
    [Website]                  NVARCHAR (MAX)   NULL,
    [Facebook]                 NVARCHAR (MAX)   NULL,
    [HasLoyalty]               BIT              CONSTRAINT [DF_Merchant_HasLoyalty] DEFAULT ((0)) NOT NULL,
    [PointsAmount]             DECIMAL (18, 10) NULL,
    [Pounds]                   DECIMAL (18, 10) NULL,
    [PointsActivationDuration] INT              NULL,
    [PointsExpirationDuration] INT              NULL,
    [Membership]               CHAR (1)         NULL,
    CONSTRAINT [PK_Merchant] PRIMARY KEY CLUSTERED ([MerchantId] ASC),
    CONSTRAINT [FK_Merchant_Interest] FOREIGN KEY ([InterestId]) REFERENCES [dbo].[Interest] ([InterestId])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'P: Platinum,G: Golden, S: Silver', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Merchant', @level2type = N'COLUMN', @level2name = N'Membership';

