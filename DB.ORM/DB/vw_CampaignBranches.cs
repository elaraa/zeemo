namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_CampaignBranches
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BranchId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MerchantId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string Address { get; set; }

        public string AddressAr { get; set; }

        [Key]
        [Column(Order = 3)]
        public string AreaName { get; set; }

        public string AreaNameAr { get; set; }
    }
}
