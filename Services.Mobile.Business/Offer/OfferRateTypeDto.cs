﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Offer
{
    public class OfferRateTypeDto
    {
        public int OfferRateTypeId { get; set; }

        
        public string OfferRateTypeName { get; set; } 
    }
}
