﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;


namespace Web.Helpers.File
{
    public class FileHelper
    {
        private static string _excelIDColumn = "AutoID";
        private static DataColumn _excelAutoIDColumn { get {
                var idColumn = new DataColumn(_excelIDColumn, typeof(int));
                idColumn.AutoIncrement = true;
                idColumn.AutoIncrementSeed = 1;
                idColumn.AutoIncrementStep = 1;

                return idColumn;
            } }
        private static List<string> _ExcelFileExtensions => new List<string> { ".xls", ".xlsx" };
        public static DataSet GetDataSetFromExcel(string filePath, bool hasHeaders = false)
        {
            DataSet dsFile = new DataSet();
            string HDR = hasHeaders ? "Yes" : "No";
            string[] strConn = new string[] {
                string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR={1};IMEX=0\"", filePath, HDR)
                ,string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR={1};IMEX=0\"",filePath, HDR) };

            string fileExt = filePath.Substring(filePath.LastIndexOf('.')).ToLower();

            if (!_ExcelFileExtensions.Contains(fileExt))
            {
                return null;
                //throw new Exception(string.Format("Cannot import {0}, Excel files are only supported.", fileExt));
            }


            OleDbConnection conn = null;

            bool connectionOpened = true;
            try
            {
                conn = new OleDbConnection(strConn[0]);
                conn.Open();
            }
            catch
            {
                connectionOpened = false;
            }
            if (!connectionOpened)
            {
                try
                {
                    conn = new OleDbConnection(strConn[1]);
                    conn.Open();
                }
                catch { return GetDateSetFromExcel(filePath,1); }
            }
            DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

            foreach (DataRow schemaRow in schemaTable.Rows)
            {
                string sheet = schemaRow["TABLE_NAME"].ToString();
                DataTable dtSheet = null;
                if (!sheet.EndsWith("_"))
                {
                    dtSheet = new DataTable();
                    string query = "SELECT  * FROM [" + sheet + "]";
                    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    //dtSheet.Columns.Add(_excelIDColumn, typeof(int));
                    //dtSheet.Columns[0].AutoIncrement = true;
                    //dtSheet.Columns[0].AutoIncrementSeed = 1;
                    //dtSheet.Columns[0].AutoIncrementStep = 1;
                    //dtexcel.Locale = CultureInfo.CurrentCulture;
                    dtSheet.Columns.Add(_excelAutoIDColumn);
                    daexcel.Fill(dtSheet);
                    dtSheet.TableName = sheet;
                    dsFile.Tables.Add(dtSheet);
                }
            }

            conn.Close();
            return dsFile;
        }
        public static List<string> UploadFiles(HttpPostedFileBase[] files, string directory = @"~/TempFiles/")
        {
            string relativePath = string.Empty;
            List<string> filePaths = new List<string>();

            foreach (HttpPostedFileBase file in files)
            {
               filePaths.Add(UploadFile(file, directory));
            }
            return filePaths;
        }

        public static bool UploadFiles(string section, string fileId, string baseUrl, string uploadRoute, params HttpPostedFileBase[] files)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    foreach (var file in files)
                    {
                        using (var content = new MultipartFormDataContent())
                        {
                            byte[] Bytes = new byte[file.InputStream.Length + 1];
                            file.InputStream.Read(Bytes, 0, Bytes.Length);
                            var fileContent = new ByteArrayContent(Bytes);
                            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") 
                                                             { FileName = file.FileName, Name = section + "\\" + fileId };
                            content.Add(fileContent);
                            var requestUri = baseUrl + uploadRoute;
                            var result = client.PostAsync(requestUri, content).Result;

                            if (result.IsSuccessStatusCode)
                                return true;
                            else
                                return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return false;
        }

        public static bool DeleteFiles(string pathUrl, string baseUrl, string DeleteRoute)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var Data = new StringContent(pathUrl, Encoding.UTF8);
                    var requestUri = baseUrl + DeleteRoute;
                    var result = client.PostAsync(requestUri, Data).Result;
                    if (result.IsSuccessStatusCode)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public static string UploadFile(HttpPostedFileBase file, string directory = @"~/TempFiles/")
        {
            string relativePath = string.Empty;

            relativePath = Path.Combine(directory, file.FileName)
                                    .Replace(file.FileName,
                                        string.Format("{1}{0}", file.FileName,
                                            Guid.NewGuid()
                                                .ToString()
                                                .Replace("-", "")));
            if (file != null && file.ContentLength > 0)
            {
                relativePath = HttpContext.Current.Server.MapPath(relativePath);
                file.SaveAs(relativePath);
            }
            return relativePath;
        }

        public static DataSet GetDateSetFromExcel(string filePath, int headerRow = 1)
        {
            var fi = new FileInfo(filePath);
            return GetDateSetFromExcel(fi.OpenRead(), headerRow);
        }

        public static DataSet GetDateSetFromExcel(Stream fileStream, int headerRow = 1)
        {
            var dsResult = new DataSet();


            using (ExcelPackage package = new ExcelPackage(fileStream))
            {
                foreach (var sheet in package.Workbook.Worksheets)
                {

                    try
                    {
                        var table = new DataTable { TableName = sheet.Name };

                        /*int sheetStartRow = 1;
                        if (headerRow > 0)
                        {
                            sheetStartRow = headerRow;
                        }*/
                        var columns = from firstRowCell in sheet.Cells[sheet.Dimension.Start.Row, sheet.Dimension.Start.Column, sheet.Dimension.Start.Row, sheet.Dimension.End.Column]
                                      select new DataColumn(headerRow > 0 ? firstRowCell.Value.ToString() : $"Column {firstRowCell.Start.Column}");

                        table.Columns.Add(_excelAutoIDColumn);
                        table.Columns.AddRange(columns.ToArray());

                        //var startRow = headerRow > 0 ? sheetStartRow + 1 : sheetStartRow;

                        for (var rowIndex = 1; rowIndex <= sheet.Dimension.End.Row; rowIndex++)
                        {
                            var inputRow = sheet.Cells[sheet.Dimension.Start.Row + rowIndex, sheet.Dimension.Start.Column, sheet.Dimension.Start.Row + rowIndex, sheet.Dimension.End.Column];
                            //if input row is empty
                            if(IsEmpty(inputRow))
                                continue;
                            var row = table.Rows.Add();
                            foreach (var cell in inputRow)
                            {
                                if(cell.Start.Column - sheet.Dimension.Start.Column + 1 < table.Columns.Count)
                                    row[cell.Start.Column- sheet.Dimension.Start.Column+1] = cell.Value;
                            }
                        }

                        dsResult.Tables.Add(table);
                    }
                    catch(Exception e)
                    {
                    }
                }

            }
            return dsResult;
        }

        private static bool IsEmpty(ExcelRange inputRow)
        {
            foreach (var cell in inputRow)
                if (cell!=null && cell.Value  != null && !string.IsNullOrEmpty(cell.Value.ToString()))
                    return false;
            return true;
        }

        public static byte[] DatatableToBytes(DataTable dataTable, string sheetName = "sheet1")
        {
            MemoryStream memStream = new MemoryStream();

            try
            {
                using (ExcelPackage pck = new ExcelPackage(memStream))
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);
                    ws.Cells["A1"].LoadFromDataTable(dataTable, true);

                    var range = ws.Cells[1, 1, dataTable.Rows.Count + 1, dataTable.Columns.Count];
                    var dcd = ws.ConditionalFormatting.AddContainsText(range).Style.Border.Top.Color;

                    // add the excel table entity
                    var table = ws.Tables.Add(range, sheetName);
                    table.ShowHeader = true;
                    table.ShowFilter = true;
                    table.ShowRowStripes = true;
                    ws.Cells.AutoFitColumns();

                    pck.Save();
                }
            }
            catch
            {

            }
            return memStream.ToArray();
        }
    }
}
