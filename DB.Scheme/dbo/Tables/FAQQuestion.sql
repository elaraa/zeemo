﻿CREATE TABLE [dbo].[FAQQuestion] (
    [FaqQuestionId] INT            IDENTITY (1, 1) NOT NULL,
    [QuestionText]  NVARCHAR (MAX) NOT NULL,
    [FaqCategoryId] INT            NOT NULL,
    [AnswerText01]  NVARCHAR (MAX) NULL,
    [AnswerImage]   NVARCHAR (MAX) NULL,
    [AnswerText02]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_FAQQuestion] PRIMARY KEY CLUSTERED ([FaqQuestionId] ASC)
);

