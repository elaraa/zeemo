﻿using DB.ORM.DB; 
using System;
using Services.Base;
using System.Web;
using System.Linq;
using static Services.Helper.EnumHelpers;
using Web.Helpers.File;
using static DB.ORM.DB.SystemUser;
using Services.Base.Base;
using AutoMapper;

namespace Services.Web.Business.Admin
{
    public class UserService : BaseService<UserDto>
    {
        public UserService(){}

        public override RunResult AddOrUpdate(UserDto user, bool isCreate)
        {
            try
            {
                if (Any<SystemUser>(c => c.UserId != user.Id && c.LoginId == user.LoginId))
                    return new RunResult { ErrorDesc= ZM.Message.DuplicatedLoginName };

                if (!string.IsNullOrEmpty(user.Password) && user.Id == 0)
                    user.Password = LoginService.HashString(user.Password);

                var systemUser = AddOrUpdate(Mapper.Map<SystemUser>(user));
                if (systemUser != null)
                {
                    var baseUploadUrl = "uploads/" + ZM.Uploads.User + '/' + systemUser.UserId.ToString() + '/';
                    if (user.PictureFile != null && user.PictureFile.ContentLength > 0)
                    {
                        var isUploaded = FileHelper.UploadFiles(ZM.Uploads.User, systemUser.UserId.ToString(), GetBaseUrl, GetUploadUrl, user.PictureFile);
                        if (isUploaded)
                        {
                            FileHelper.DeleteFiles(systemUser.UserLogo, GetBaseUrl, GetDeleteUrl);
                            systemUser.UserLogo = baseUploadUrl + user.PictureFile.FileName;
                            AddOrUpdate(systemUser);
                        }
                    }
                    return MessageService.ShowSuccessMessage(isCreate);
                }

            }
            catch (Exception e) {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            return new RunResult { ErrorDesc = ZM.Message.ErrorCode };
        }
    }
}
