﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetOffers]
(	
    @ConsumerId int,
	@Lat nvarchar(20) = null,
	@Lng nvarchar(20) = null
)
RETURNS TABLE 
AS
RETURN 
(
SELECT        T.PromotionRunId as OfferId, T.PromotionName as OfferName,T.PromotionNameAr as OfferNameAr,T.Duration,
T.PromotionTypeName as OfferTypeName, T.ImageUrl as [Image], T.MerchantBrandName AS BrandName, 
T.MerchantId, T.InterestId, T.PromotionTypeId, T.NumberOfPieces as NoOfPieces, T.EndDate, T.Savings
from(
select r.PromotionRunId,r.PromotionId ,r.EndDate,r.NumberOfPoints,r.NumberOfPieces, r.Savings,  DATEDIFF(second, r.StartDate, r.EndDate ) as Duration
,p.PromotionName,p.PromotionNameAr, t.PromotionTypeId, t.PromotionTypeName, (select top 1 AppSettingValue from AppSettings where AppSettingCode = 'BaseFileUrl') + i.ImageUrl as ImageUrl, m.MerchantId, m.MerchantBrandName, m.InterestId,
case when (DATEDIFF(year, c.BirthDate,  getdate () ) >= pc.AgeFrom and 
DATEDIFF(year, c.BirthDate,  getdate () ) <= pc.AgeTo and pc.Gender = c.Gender) then 1 else 0 end as ApplicableConsumer

FROM                    dbo.Promotion AS p INNER JOIN
                        dbo.PromotionRun AS r on r.PromotionId = p.PromotionId inner join 
						dbo.PromotionRunCriteria as pc on r.PromotionRunId=pc.PromotionRunId join
                        dbo.PromotionType AS t ON p.PromotionTypeId = t.PromotionTypeId LEFT JOIN
                        dbo.PromotionImage AS i ON p.PromotionId = i.PromotionId AND i.IsLogo = 1 INNER JOIN
                        dbo.Merchant AS m ON p.MerchantId = m.MerchantId join
						Consumer c on c.ConsumerId = @ConsumerId
						
WHERE        (r.StartDate <= GETDATE()) AND (r.EndDate >= GETDATE()) AND (r.CloseDate IS NULL)
           and ((@Lat is not null and @Lng is not null and (select top 1 * from dbo.fn_IsOfferInArea(r.PromotionRunId, @Lat, @Lng)) = 1) or
		        (@Lat is null or @Lng is null))
group by  r.PromotionRunId,r.EndDate,r.NumberOfPoints,r.NumberOfPieces,r.CloseDate,r.StartDate,r.PromotionId,
						r.Savings, p.PromotionName,p.PromotionNameAr,t.PromotionTypeId, t.PromotionTypeName,
						i.ImageUrl,pc.AgeFrom,pc.AgeTo,pc.Gender,m.MerchantId,m.InterestId , m.MerchantBrandName, c.BirthDate, c.Gender)T
group by  T.PromotionRunId, T.PromotionName,T.PromotionNameAr,T.Duration, T.PromotionTypeName, T.ImageUrl,T.MerchantBrandName,
T.MerchantId, T.InterestId, T.PromotionTypeId, T.NumberOfPieces, T.EndDate, T.Savings

having max(T.ApplicableConsumer) > 0
)