﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Offer
{
   public class viewOfferDto
    {
       
        public int OfferId { get; set; }

        public string OfferName { get; set; }

        public int? NoOfPieces { get; set; }

        public int? Duration { get; set; }

      
        public string OfferTypeName { get; set; }

        public string Image { get; set; }

       
        public string BrandName { get; set; }
    }
}
