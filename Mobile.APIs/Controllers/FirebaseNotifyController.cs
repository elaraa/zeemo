﻿using FireSharp;
using FireSharp.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace WebApplication2.Controllers
{
    public class Message
    {
        public string[] registration_ids { get; set; }
        public Notification notification { get; set; }
        public object data { get; set; }
    }
    public class Notification
    {
        public string title { get; set; }
        public string text { get; set; }
    }
    public class FirebaseNotifyController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Send()
        {

            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            //serverKey - Key from Firebase cloud messaging server  
            tRequest.Headers.Add(string.Format("Authorization: key={0}", "AAAAi7A8FMo:APA91bHW1SVIZlgzFsLTmn-XDiuq5k7fXZwrC8uWaCKLaPB9Lk1R5FU6W4VIygWXVtR6N1m4ZptbFs4TPPh6wLHhlzkLL9bhVn7koLCMS1-TOrw5l9eR0VUXK9bkXpkANEBz2aS4pa68"));
            //Sender Id - From firebase project setting  
            tRequest.Headers.Add(string.Format("Sender: id={0}", "599957181642"));
            tRequest.ContentType = "application/json";
            var payload = new
            {
                to = "eGjmBcjjFv4:APA91bHEjv6XqX9VAKtOYuJOX27QzidXn0aBPl_xQ18VP4_zzMHZg7C1u035p-8lTipa1BJCaPoSb9n2-sUqwRKd_ktefuR1mQ8c38zTCqXfOuGW67485Ox1sgahiFWlVeYo_7Qea-ck",
         
                notification = new
                {
                    body = "Hi from my pc",
                    title = "H for notify"
                }
            };

            string postbody = JsonConvert.SerializeObject(payload).ToString();
            Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
            tRequest.ContentLength = byteArray.Length;
            using (Stream dataStream = tRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
                using (WebResponse tResponse = tRequest.GetResponse())
                {
                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                    {
                        if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                            }
                    }
                }
            }
            //var deviceTokens = new List<string>();
            //deviceTokens.Add("eGjmBcjjFv4:APA91bHEjv6XqX9VAKtOYuJOX27QzidXn0aBPl_xQ18VP4_zzMHZg7C1u035p-8lTipa1BJCaPoSb9n2-sUqwRKd_ktefuR1mQ8c38zTCqXfOuGW67485Ox1sgahiFWlVeYo_7Qea-ck");
            //var messageInformation = new Message()
            //{
            //    notification = new Notification()
            //    {
            //        title = "hi",
            //        text = "bye"
            //    },
            //    data = null,
            //    registration_ids = deviceTokens.ToArray()
            //};
            //string jsonMessage = JsonConvert.SerializeObject(messageInformation);



            return Ok();
        }
    
  }
}
