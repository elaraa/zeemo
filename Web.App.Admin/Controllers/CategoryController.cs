using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
namespace Web.App.Admin.Controllers
{
    public class CategoryController : BaseControllerTemplate<Category, Category, int>
    {
       public CategoryController() : base()
       {
            ViewNameSingular = "Category";
            ViewNamePlural = "Categorys";
            PropertiesToUpdate = new Expression<Func<Category, object>>[]
           {
               x=>x.CategoryId,
               x=>x.Quota,
               x=>x.CategoryName

           };
       }
    protected override Expression<Func<Category, bool>> GetOneExpression(int? id)
    {
        return id.HasValue ? (x => x.CategoryId == id.Value) : (Expression<Func<Category, bool>>)(x => 1 == 0);
    }
 }
}
