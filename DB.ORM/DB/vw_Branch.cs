namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Branch
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int BranchId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AreaId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string AreaName { get; set; }

        public string AreaNameAr { get; set; }

        [Key]
        [Column(Order = 3)]
        public string Address { get; set; }

        public string AddressAr { get; set; }

        [Key]
        [Column(Order = 4)]
        public decimal Lat { get; set; }

        [Key]
        [Column(Order = 5)]
        public decimal Lng { get; set; }

        public int? Cashiers { get; set; }
    }
}
