﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetMerchants]
(	
  @ConsumerId int
)
RETURNS TABLE 
AS
RETURN 
(

select T.MerchantId, T.MerchantName, T.ImageUrl, T.IsFollowed, T.Interest, 
T.Savings + (SUM(T.Points) * (T.PointsAmount/ T.Pounds)) as Savings
, SUM(T.Points) as Points, min(T.ExpiryDate) as FirstPointsToExpire
from 
(select m.MerchantId as MerchantId, m.MerchantName as MerchantName, m.MerchantLogo as ImageUrl, 
(case when f.MerchantFollowerId is not null then  CAST(1 AS BIT) else  CAST(0 AS BIT) end) as IsFollowed
, i.InterestName as Interest, isnull(lp.Remain, 0) as Points, lp.ExpiryDate
, sum(r.Savings) as Savings 
, m.PointsAmount, m.Pounds
from Merchant m join
Interest i on m.InterestId = i.InterestId left join 
ConsumerRedeemedPromotionRun rp on rp.ConsumerId = @ConsumerId left join
PromotionRun r on r.PromotionRunId = rp.PromotionRunId left join
Promotion p on p.PromotionId = r.PromotionId and p.MerchantId = m.MerchantId left join
ConsumerLoyaltyPoint lp on lp.MerchantId = m.MerchantId	and lp.ConsumerId = @ConsumerId left join
MerchantFollower f on f.MerchantId = m.MerchantId and f.ConsumerId = @ConsumerId

--where lp.ExpiryDate >= convert(date, GETDATE()) 
group by m.MerchantId, m.MerchantName, m.MerchantLogo, f.MerchantFollowerId, i.InterestName
, m.PointsAmount, m.Pounds, lp.ConsumerPointId, lp.Remain, lp.ExpiryDate )T

group by T.MerchantId, T.MerchantName, T.ImageUrl, T.IsFollowed, T.Interest, T.Savings, T.PointsAmount, T.Pounds
)