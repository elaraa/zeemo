﻿using Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB.ORM.DB;
namespace Services.Mobile.Business.System
{
   public class SystemService:BaseService<FAQCategory>
   {
        public RequestResult contactUs(ContactUsData data,string userToken)
        {
             int consumerId = -1;
                try
                {
                    consumerId = getTokenId(userToken);
                    var consumer = GetOne<Consumer>(w => w.ConsumerId == consumerId);

                    if (consumer != null)
                    {
                        return new RequestResult
                        {
                            Failed = false,
                            Data = data
                        };
                    }
                    else
                        return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };

                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                    return new RequestResult { Failed = true };
                }
            
        }

        public RequestResult getTermsAndConditions()
        {
            try
            {
                    return new RequestResult
                    {
                        Failed = false,
                        Data = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lectus arcu bibendum at varius vel pharetra vel turpis nunc. In est ante in nibh mauris cursus mattis molestie a. Quis imperdiet massa tincidunt nunc pulvinar sapien et ligula ullamcorper. Pellentesque eu tincidunt tortor aliquam nulla facilisi. Eu turpis egestas pretium aenean pharetra. Sit amet consectetur adipiscing elit duis tristique sollicitudin nibh. Morbi tristique senectus et netus. Egestas sed sed risus pretium quam vulputate dignissim. Purus faucibus ornare suspendisse sed nisi lacus sed. Sed euismod nisi porta lorem mollis aliquam ut porttitor. Viverra mauris in aliquam sem fringilla ut morbi tincidunt augue. Posuere ac ut consequat semper viverra nam. Ornare quam viverra orci sagittis. Sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque eu. Vitae et leo duis ut diam quam.
                                Tortor aliquam nulla facilisi cras fermentum.Rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque.Quam vulputate dignissim suspendisse in. Ut aliquam purus sit amet luctus venenatis lectus.Mattis ullamcorper velit sed ullamcorper.Risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est.Amet commodo nulla facilisi nullam vehicula ipsum a.Volutpat commodo sed egestas egestas fringilla phasellus faucibus scelerisque.Id aliquet lectus proin nibh nisl condimentum.Cras pulvinar mattis nunc sed blandit libero volutpat sed cras.
                                Convallis posuere morbi leo urna molestie at.Sit amet facilisis magna etiam tempor orci eu lobortis.Quis vel eros donec ac odio tempor orci dapibus ultrices.Aliquam vestibulum morbi blandit cursus risus.Congue mauris rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar.Cras fermentum odio eu feugiat.Vulputate ut pharetra sit amet aliquam id diam.Tristique risus nec feugiat in fermentum posuere.Risus pretium quam vulputate dignissim suspendisse in. Fermentum posuere urna nec tincidunt praesent semper.Lectus urna duis convallis convallis tellus id.Duis at consectetur lorem donec massa sapien faucibus et.Tempus egestas sed sed risus pretium quam vulputate dignissim suspendisse.Scelerisque mauris pellentesque pulvinar pellentesque.Viverra mauris in aliquam sem fringilla ut morbi tincidunt augue.Rhoncus urna neque viverra justo nec.
                                Tortor vitae purus faucibus ornare suspendisse.Sed cras ornare arcu dui vivamus arcu felis bibendum.Massa vitae tortor condimentum lacinia quis vel eros.Egestas purus viverra accumsan in nisl nisi.Leo urna molestie at elementum.Urna id volutpat lacus laoreet non curabitur.Condimentum lacinia quis vel eros donec ac odio tempor orci.Massa tincidunt dui ut ornare lectus sit amet est.Nisl condimentum id venenatis a.Nisi lacus sed viverra tellus in hac habitasse.Suspendisse potenti nullam ac tortor vitae purus faucibus.Morbi tempus iaculis urna id.Eu feugiat pretium nibh ipsum consequat.
                                Ut morbi tincidunt augue interdum velit euismod in pellentesque.Lorem mollis aliquam ut porttitor leo a.Sit amet tellus cras adipiscing.Aliquet risus feugiat in ante metus dictum at tempor commodo.Mi quis hendrerit dolor magna eget.Ac turpis egestas maecenas pharetra convallis posuere morbi leo.Suscipit tellus mauris a diam.Enim nunc faucibus a pellentesque sit amet porttitor.Sed ullamcorper morbi tincidunt ornare massa eget egestas.Odio eu feugiat pretium nibh ipsum consequat nisl."
                    };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true };
            }

        }

        public IQueryable<FAQCategory> fAQCategories(string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);
                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                { 
                    var res = Get();
                    return res;
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
        }

        public ListResult<FAQQuestion> fAQQuestions(int cateogryId,ListParams @params, string userToken,object Header)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var totalCount = db.vOffers.Count();
                    return new ListResult<FAQQuestion>
                    {
                        dataHeader = Header,
                        page = @params.@params.page,
                        pageCount = totalCount / @params.@params.limit,
                        totalCount = totalCount,
                        data = db.FAQQuestions.Where(w => w.FaqCategoryId == cateogryId).OrderBy(o => o.QuestionText).Skip((@params.@params.page - 1) * @params.@params.limit).Take(@params.@params.limit),
                        limit = @params.@params.limit
                    };
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }

        }
    }
}
