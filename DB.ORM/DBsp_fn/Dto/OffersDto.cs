﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DB.ORM.DBsp_fn.Dto
{
   public class OffersDto
    { 
        public int OfferId { get; set; } 
        public string OfferName { get; set; }
        public string OfferNameAr { get; set; } 
        public int? Duration { get; set; }
        public string OfferTypeName { get; set; }
        public string Image { get; set; }
        public string BrandName { get; set; }
        public int MerchantId { get; set; }
        public int InterestId { get; set; }
        public int PromotionTypeId { get; set; }
        public int? NoOfPieces { get; set; } 
        public decimal? Savings { get; set; }
        public DateTime? EndDate { get; set; }

    }
}
