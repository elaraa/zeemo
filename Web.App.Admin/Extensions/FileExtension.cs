﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Web.App.Admin
{
    public static class FileExtension
    {
        public static bool SaveSalesImport(this HttpPostedFileBase[] files, int fileId)
        {
            foreach (var item in files)
              saveFile(item, @"uploads\sales\imports", fileId);
            return true;
        }

        public static bool SaveFiles(this HttpPostedFileBase[] files, string section, int fileId)
        {
            foreach (var item in files)
                saveFile(item, @"uploads\" + section, fileId);
            return true;
        }
        public static bool deleteFiles(this string[] fileNames, string section, string fileId)
        {
            foreach (var file in fileNames)
                deleteFile(file, @"Uploads\" + section, fileId);
            return true;
        }
        public static bool SaveMarketShareFile(this HttpPostedFileBase file, int fileId) {
            return saveFile(file, @"uploads\MarketShare", fileId);
        }
        public static bool SaveTargetProductFile(this HttpPostedFileBase file, int year)
        {
            return saveFile(file, @"uploads\Target\Product", year);
        }
        public static bool SaveTargetProfileFile(this HttpPostedFileBase file, int timedefId, int userId)
        {
            return saveFile(file, string.Format(@"uploads\Target\ProfileTerritoryProduct\{0}", timedefId), userId);
        }
        public static bool SaveEmployeePic(this HttpPostedFileBase file, int empId) {
            return saveFile(file, @"uploads\Employee\", empId);
        }

        private static bool saveFile(this HttpPostedFileBase file, string folder, int fileId) {
            string rootFolder = string.Format(@"{0}\{1}", HttpContext.Current.Request.PhysicalApplicationPath, folder);
            string _template = rootFolder + @"\{0}\{1}";
            try
            {
                if (!System.IO.Directory.Exists(rootFolder))
                    System.IO.Directory.CreateDirectory(rootFolder);

                if (!System.IO.Directory.Exists(rootFolder + "\\" + fileId))
                    System.IO.Directory.CreateDirectory(rootFolder + "\\" + fileId);

                file.SaveAs(string.Format(_template, fileId, file.FileName));
                return true;
            }
            catch (Exception e) { Elmah.ErrorSignal.FromCurrentContext().Raise(e); return false; }
        }
        private static bool deleteFile(this string fileName, string folder, string fileId)
        {
            string rootFolder = string.Format(@"{0}\{1}", HttpContext.Current.Request.PhysicalApplicationPath, folder);
            string _template = rootFolder + @"\{0}\{1}";
            try
            {
                var physicalPath = Path.Combine(string.Format(_template, fileId, fileName));
                // TODO: Verify user permissions

                if (System.IO.File.Exists(physicalPath))
                {
                    System.IO.File.Delete(physicalPath);
                }
                return true;
            }
            catch (Exception e) { Elmah.ErrorSignal.FromCurrentContext().Raise(e); return false; }
        }
        #region PluginInput Helper
        //public static Sales.Core.ImportFile.PluginInput GetInput(this HttpPostedFileBase file)
        //{
        //    return new Sales.Core.ImportFile.PluginInput
        //    {
        //        FileStream = file.InputStream,
        //        FileName = file.FileName,
        //        FileContentType = file.ContentType
        //    };
        //}
        //public static Sales.Core.ImportFile.PluginInput[] GetInput(this HttpPostedFileBase[] files)
        //{
        //    var inputs = new List<Sales.Core.ImportFile.PluginInput>();
        //    foreach (var item in files)
        //        inputs.Add(item.GetInput());
        //    return inputs.ToArray();

        //}
        #endregion
    }
}