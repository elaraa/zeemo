namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SystemMenu")]
    public partial class SystemMenu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SystemMenu()
        {
            SystemMenu1 = new HashSet<SystemMenu>();
            SystemRoleMenus = new HashSet<SystemRoleMenu>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MenuId { get; set; }

        [Required]
        public string MenuName { get; set; }

        public string MenuIcon { get; set; }

        public int? ParentMenuId { get; set; }

        public string Color { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public int SortOrder { get; set; }

        [Required]
        [StringLength(50)]
        public string Module { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public int? CreatedById { get; set; }

        public int? LastModifiedById { get; set; }

        public virtual SystemUser SystemUser { get; set; }

        public virtual SystemUser SystemUser1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SystemMenu> SystemMenu1 { get; set; }

        public virtual SystemMenu SystemMenu2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SystemRoleMenu> SystemRoleMenus { get; set; }
    }
}
