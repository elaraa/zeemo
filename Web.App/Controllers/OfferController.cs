﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class OfferController : BaseControllerTemplate<vw_Promotion, Promotion, int>
    {
        public OfferController()
        {
            ViewNamePlural = "Offers";
            ViewNameSingular = "Offer";

        }
    }
}