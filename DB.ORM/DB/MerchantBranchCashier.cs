namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MerchantBranchCashier")]
    public partial class MerchantBranchCashier
    {
        public int MerchantBranchCashierId { get; set; }

        public int BranchId { get; set; }

        [Required]
        public string CashierName { get; set; }

        [Required]
        public string LoginId { get; set; }

        [Required]
        public string Password { get; set; }

        [StringLength(5)]
        public string Language { get; set; }

        public virtual MerchantBranch MerchantBranch { get; set; }
    }
}
