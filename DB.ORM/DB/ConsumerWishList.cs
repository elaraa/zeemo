namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerWishList")]
    public partial class ConsumerWishList
    {
        [Key]
        public int ConsumerWishListOfferId { get; set; }

        public int ConsumerId { get; set; }

        public int PromotionId { get; set; }
    }
}
