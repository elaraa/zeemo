﻿using Abp.Authorization.Users;
using DB.ORM.DB;
using System.ComponentModel.DataAnnotations;

namespace Services.Web.Business.Dto
{
    public class MerchantPointDto
    {
        public int PointId { get; set; }

        public int MerchantId { get; set; }

        public int PointValue { get; set; }

        public string PointDescription { get; set; }

        public string PointDescriptionAr { get; set; }
    }
   
}
