using AutoMapper;
using DB.ORM.DB;
using Services.Base;
using Services.Web.Business.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static DB.ORM.DB.Merchant;

namespace Web.App.Admin.Controllers
{
    public class MerchantController : BaseControllerTemplateDto<vw_Merchant, Merchant, MerchantDto, int>
    {
       public MerchantController() : base()
       {
            ViewNameSingular = ZM.ViewPages.MerchantViewNameSingular;
            ViewNamePlural = ZM.ViewPages.MerchantViewNamePlural;
            PropertiesToUpdate = new Expression<Func<Merchant, object>>[]
            {
               x=>x.MerchantName,
               x=>x.MerchantBrandName,
               x=>x.InterestId,
               x=>x.LoginId,
               x=>x.Website,
               x=>x.Facebook,
               x=>x.MerchantLogo
            };
       }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Interests = LookupHelper.GetMyInterests(this);
            base.FillViewBag(isCreate);
        }
        protected override void InitTemplateServices()
        {
            ListService = new BaseService<vw_Merchant>(UserId);
            ActionService = new MerchantService();
        }
        protected override Expression<Func<Merchant, bool>> GetOneExpression(int? id)
        {
            return id.HasValue ? (x => x.MerchantId == id.Value) : (Expression<Func<Merchant, bool>>)(x => 1 == 0);
        }

    }
}
