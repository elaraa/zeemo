﻿using AutoMapper;
using DB.ORM.DB;
using Services.Mobile.Business.Dto;
using System.Web.Http;
using System.Web.Routing;

namespace Mobile.APIs
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RouteTable.Routes.Ignore("{resource}.axd/{*pathInfo}");
            GlobalConfiguration.Configure(WebApiConfig.Register);

            Mapper.Initialize(cfg => {
                cfg.CreateMap< LocationDto, Area>();
                cfg.CreateMap<Area, LocationDto>();
            });
        }
    }
}
