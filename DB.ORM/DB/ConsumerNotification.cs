namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerNotification")]
    public partial class ConsumerNotification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int NotificationId { get; set; }

        public int ConsumerId { get; set; }

        public bool IsRead { get; set; }

        [Required]
        public string NotificationMessage { get; set; }

        public DateTime CreationDate { get; set; }

        public int? MerchantId { get; set; }
    }
}
