﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.Mobile.Business
{
    public class RequestResult
    {
        public bool Failed { get; set; }
        public string ErrorCode { get; set; }
        public object Data { get; set; }
    }
    public class ConsumerContract<T> where T: class
    {
        public T wcObject { get; set; }
    }
    public class ConsumerStructContract<T> where T: struct
    {
        public T wcObject { get; set; }
    }
    public class ListPaging
    {
        public int page { get; set; }
        public int limit { get; set; }

    }
    public class ListParams
    {

        public ListPaging @params { get; set; }
    }
    public class ContactUsData
    {
        public string title { get; set; }
        public string body { get; set; }
    }
}