namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_PromotionOffers
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionRunId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionId { get; set; }

        public int? NumberOfPieces { get; set; }

        public string Description { get; set; }

        public string DescriptionAr { get; set; }

        [Key]
        [Column(Order = 2)]
        public string PromotionName { get; set; }

        public string PromotionNameAr { get; set; }

        [Key]
        [Column(Order = 3)]
        public string MerchantBrandName { get; set; }

        [Key]
        [Column(Order = 4)]
        public string PromotionTypeName { get; set; }

        public int? Duration { get; set; }
    }
}
