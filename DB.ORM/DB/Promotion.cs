namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Promotion")]
    public partial class Promotion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Promotion()
        {
            PromotionImages = new HashSet<PromotionImage>();
            PromotionRuns = new HashSet<PromotionRun>();
        }

        public int PromotionId { get; set; }

        [Required]
        public string PromotionName { get; set; }

        public string PromotionNameAr { get; set; }

        public int PromotionTypeId { get; set; }

        public string Description { get; set; }

        public string DescriptionAr { get; set; }

        public int MerchantId { get; set; }

        public DateTime? CreationDate { get; set; }

        public bool IsDraft { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public DateTime? RejectedDate { get; set; }

        public virtual Merchant Merchant { get; set; }

        public virtual PromotionType PromotionType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PromotionImage> PromotionImages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PromotionRun> PromotionRuns { get; set; }
    }
}
