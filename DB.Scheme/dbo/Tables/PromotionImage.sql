﻿CREATE TABLE [dbo].[PromotionImage] (
    [PromotionImageId] INT            IDENTITY (1, 1) NOT NULL,
    [PromotionId]      INT            NOT NULL,
    [ImageUrl]         NVARCHAR (500) NOT NULL,
    [IsLogo]           BIT            NOT NULL,
    [SortOrder]        INT            NOT NULL,
    CONSTRAINT [PK_PromotionImage] PRIMARY KEY CLUSTERED ([PromotionImageId] ASC),
    CONSTRAINT [FK_PromotionImage_Promotion] FOREIGN KEY ([PromotionId]) REFERENCES [dbo].[Promotion] ([PromotionId])
);

