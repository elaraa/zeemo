namespace DB.ORM.DB
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBModel : DbContext
    {
        public DBModel()
            : base("name=DBModel")
        {
        }

        public virtual DbSet<AppSetting> AppSettings { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Consumer> Consumers { get; set; }
        public virtual DbSet<ConsumerInterest> ConsumerInterests { get; set; }
        public virtual DbSet<ConsumerLoyaltyPoint> ConsumerLoyaltyPoints { get; set; }
        public virtual DbSet<ConsumerNotification> ConsumerNotifications { get; set; }
        public virtual DbSet<ConsumerRedeemedPoint> ConsumerRedeemedPoints { get; set; }
        public virtual DbSet<ConsumerRedeemedPromotionRun> ConsumerRedeemedPromotionRuns { get; set; }
        public virtual DbSet<ConsumerReportPromotionRun> ConsumerReportPromotionRuns { get; set; }
        public virtual DbSet<ConsumerWishList> ConsumerWishLists { get; set; }
        public virtual DbSet<FAQCategory> FAQCategories { get; set; }
        public virtual DbSet<FAQQuestion> FAQQuestions { get; set; }
        public virtual DbSet<Interest> Interests { get; set; }
        public virtual DbSet<Merchant> Merchants { get; set; }
        public virtual DbSet<MerchantBranch> MerchantBranches { get; set; }
        public virtual DbSet<MerchantBranchCashier> MerchantBranchCashiers { get; set; }
        public virtual DbSet<MerchantCategory> MerchantCategories { get; set; }
        public virtual DbSet<MerchantFollower> MerchantFollowers { get; set; }
        public virtual DbSet<MerchantPoint> MerchantPoints { get; set; }
        public virtual DbSet<Promotion> Promotions { get; set; }
        public virtual DbSet<PromotionImage> PromotionImages { get; set; }
        public virtual DbSet<PromotionRate> PromotionRates { get; set; }
        public virtual DbSet<PromotionRateType> PromotionRateTypes { get; set; }
        public virtual DbSet<PromotionRun> PromotionRuns { get; set; }
        public virtual DbSet<PromotionRunBranch> PromotionRunBranches { get; set; }
        public virtual DbSet<PromotionRunCriteria> PromotionRunCriterias { get; set; }
        public virtual DbSet<PromotionType> PromotionTypes { get; set; }
        public virtual DbSet<ReasonType> ReasonTypes { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<SystemMenu> SystemMenus { get; set; }
        public virtual DbSet<SystemRole> SystemRoles { get; set; }
        public virtual DbSet<SystemRoleMenu> SystemRoleMenus { get; set; }
        public virtual DbSet<SystemUser> SystemUsers { get; set; }
        public virtual DbSet<vConsumerNotification> vConsumerNotifications { get; set; }
        public virtual DbSet<vOffer> vOffers { get; set; }
        public virtual DbSet<vw_Branch> vw_Branch { get; set; }
        public virtual DbSet<vw_Campaign> vw_Campaign { get; set; }
        public virtual DbSet<vw_CampaignBranches> vw_CampaignBranches { get; set; }
        public virtual DbSet<vw_Consumer> vw_Consumer { get; set; }
        public virtual DbSet<vw_FAQ> vw_FAQ { get; set; }
        public virtual DbSet<vw_Interests> vw_Interests { get; set; }
        public virtual DbSet<vw_Menus> vw_Menus { get; set; }
        public virtual DbSet<vw_Merchant> vw_Merchant { get; set; }
        public virtual DbSet<vw_MerchantPoint> vw_MerchantPoint { get; set; }
        public virtual DbSet<vw_MerchantPointMap> vw_MerchantPointMap { get; set; }
        public virtual DbSet<vw_MerchantPointSetting> vw_MerchantPointSetting { get; set; }
        public virtual DbSet<vw_Promotion> vw_Promotion { get; set; }
        public virtual DbSet<vw_PromotionOffers> vw_PromotionOffers { get; set; }
        public virtual DbSet<vw_PromotionToCheck> vw_PromotionToCheck { get; set; }
        public virtual DbSet<vw_SystemRole> vw_SystemRole { get; set; }
        public virtual DbSet<vw_User> vw_User { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Area>()
                .Property(e => e.Lat)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Area>()
                .Property(e => e.Lng)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Area>()
                .HasMany(e => e.MerchantBranches)
                .WithRequired(e => e.Area)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Category>()
                .Property(e => e.CategoryName)
                .IsUnicode(false);

            modelBuilder.Entity<Category>()
                .HasMany(e => e.MerchantCategories)
                .WithRequired(e => e.Category)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Consumer>()
                .Property(e => e.Gender)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Consumer>()
                .HasMany(e => e.ConsumerRedeemedPoints)
                .WithRequired(e => e.Consumer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Consumer>()
                .HasMany(e => e.ConsumerReportPromotionRuns)
                .WithRequired(e => e.Consumer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConsumerLoyaltyPoint>()
                .Property(e => e.InvoiceTotalAmount)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Interest>()
                .HasMany(e => e.Merchants)
                .WithRequired(e => e.Interest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Merchant>()
                .Property(e => e.PointsAmount)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Merchant>()
                .Property(e => e.Pounds)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Merchant>()
                .Property(e => e.Membership)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Merchant>()
                .HasMany(e => e.ConsumerRedeemedPoints)
                .WithRequired(e => e.Merchant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Merchant>()
                .HasMany(e => e.MerchantBranches)
                .WithRequired(e => e.Merchant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Merchant>()
                .HasMany(e => e.MerchantCategories)
                .WithRequired(e => e.Merchant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Merchant>()
                .HasMany(e => e.MerchantPoints)
                .WithRequired(e => e.Merchant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Merchant>()
                .HasMany(e => e.Promotions)
                .WithRequired(e => e.Merchant)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MerchantBranch>()
                .Property(e => e.Lat)
                .HasPrecision(18, 10);

            modelBuilder.Entity<MerchantBranch>()
                .Property(e => e.Lng)
                .HasPrecision(18, 10);

            modelBuilder.Entity<MerchantBranch>()
                .HasMany(e => e.MerchantBranchCashiers)
                .WithRequired(e => e.MerchantBranch)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MerchantBranch>()
                .HasMany(e => e.PromotionRunBranches)
                .WithRequired(e => e.MerchantBranch)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MerchantPoint>()
                .HasMany(e => e.ConsumerRedeemedPoints)
                .WithRequired(e => e.MerchantPoint)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Promotion>()
                .HasMany(e => e.PromotionImages)
                .WithRequired(e => e.Promotion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Promotion>()
                .HasMany(e => e.PromotionRuns)
                .WithRequired(e => e.Promotion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PromotionRun>()
                .Property(e => e.Savings)
                .HasPrecision(18, 10);

            modelBuilder.Entity<PromotionRun>()
                .HasMany(e => e.ConsumerReportPromotionRuns)
                .WithRequired(e => e.PromotionRun)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PromotionRun>()
                .HasMany(e => e.PromotionRunBranches)
                .WithRequired(e => e.PromotionRun)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PromotionRun>()
                .HasMany(e => e.PromotionRunCriterias)
                .WithRequired(e => e.PromotionRun)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PromotionRunCriteria>()
                .Property(e => e.Gender)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<PromotionType>()
                .HasMany(e => e.Promotions)
                .WithRequired(e => e.PromotionType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ReasonType>()
                .HasMany(e => e.ConsumerReportPromotionRuns)
                .WithRequired(e => e.ReasonType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SystemMenu>()
                .HasMany(e => e.SystemMenu1)
                .WithOptional(e => e.SystemMenu2)
                .HasForeignKey(e => e.ParentMenuId);

            modelBuilder.Entity<SystemMenu>()
                .HasMany(e => e.SystemRoleMenus)
                .WithRequired(e => e.SystemMenu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SystemRole>()
                .HasMany(e => e.SystemRoleMenus)
                .WithRequired(e => e.SystemRole)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SystemRole>()
                .HasMany(e => e.SystemUsers)
                .WithRequired(e => e.SystemRole)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SystemUser>()
                .HasMany(e => e.SystemMenus)
                .WithOptional(e => e.SystemUser)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<SystemUser>()
                .HasMany(e => e.SystemMenus1)
                .WithOptional(e => e.SystemUser1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<SystemUser>()
                .HasMany(e => e.SystemRoles)
                .WithOptional(e => e.SystemUser)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<SystemUser>()
                .HasMany(e => e.SystemRoles1)
                .WithOptional(e => e.SystemUser1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<SystemUser>()
                .HasMany(e => e.SystemRoleMenus)
                .WithOptional(e => e.SystemUser)
                .HasForeignKey(e => e.CreatedById);

            modelBuilder.Entity<SystemUser>()
                .HasMany(e => e.SystemRoleMenus1)
                .WithOptional(e => e.SystemUser1)
                .HasForeignKey(e => e.LastModifiedById);

            modelBuilder.Entity<vOffer>()
                .Property(e => e.Savings)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_Branch>()
                .Property(e => e.Lat)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_Branch>()
                .Property(e => e.Lng)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_Campaign>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Consumer>()
                .Property(e => e.Gender)
                .IsUnicode(false);

            modelBuilder.Entity<vw_Consumer>()
                .Property(e => e.Language)
                .IsUnicode(false);

            modelBuilder.Entity<vw_MerchantPointMap>()
                .Property(e => e.Savings)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_MerchantPointSetting>()
                .Property(e => e.PointsAmount)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_MerchantPointSetting>()
                .Property(e => e.Pounds)
                .HasPrecision(18, 10);

            modelBuilder.Entity<vw_Promotion>()
                .Property(e => e.Status)
                .IsUnicode(false);
        }
    }
}
