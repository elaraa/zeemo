﻿function DeleteGridRecordSwal(gridId, recordId, Url, swal, SucessMessageHeader, SucessMessage, FailMessageHeader,
    FailMessage, Title, Text, Type, ConfirmButtonText, CancelButtonText) {

    ConfirmationSWAL(gridId, recordId, Url, SucessMessageHeader, SucessMessage, FailMessageHeader,
        FailMessage, Title, Text, Type, ConfirmButtonText, CancelButtonText);

    return false;
}


ConfirmationSWAL = (gridId, recordId, Url, SucessMessageHeader, SucessMessage, FailMessageHeader,
    FailMessage, Title, Text, Type, ConfirmButtonText, CancelButtonText) => {
    swal({
        title: Title,
        text: Text,
        type: Type,
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: ConfirmButtonText,
        cancelButtonText: CancelButtonText,
        closeOnConfirm: false,
        closeOnCancel: false
    },

        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: Url,
                    data: { id: parseInt(recordId) },
                    dataType: "json",
                    success: function (data) {
                        if (data == true) {
                            swal(SucessMessageHeader, SucessMessage, "success");
                            reloadControl("#" + gridId, "kendoGrid");
                        }
                        else {
                            swal("ada", "sdasd", "error");
                        }
                    },
                    error: function (httpRequest, textStatus, errorThrown) { }
                });

            }
            else
            {
                swal(FailMessageHeader, FailMessage, "error");
            }
        });
}