namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MerchantFollower")]
    public partial class MerchantFollower
    {
        public int MerchantFollowerId { get; set; }

        public int MerchantId { get; set; }

        public int ConsumerId { get; set; }
    }
}
