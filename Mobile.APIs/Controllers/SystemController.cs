﻿using Newtonsoft.Json;
using Services.Helper;
using Services.Mobile.Business;
using Services.Mobile.Business.Offer;
using Services.Mobile.Business.System;
using Services.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebApplication2.Controllers
{
    public class SystemController : ApiController
    {
        SystemService systemService = new SystemService();
        [HttpPost]
        public RequestResult ContactUs([FromBody]ContactUsData data)
        {
            ///validate token
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return systemService.contactUs(data, userToken);
            return new RequestResult { Failed = false };
        }

        [HttpPost]
        public RequestResult GetTermsAndConditions()
        {
            ///validate token
            // var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return systemService.getTermsAndConditions(); 
            return new RequestResult { Failed = false };
        }
        [HttpPost]
        public IHttpActionResult FAQCategories()
        {
            //using (var db = new DBModel())
            //{
            //    return Json(db.FAQCategories.ToList());
            //}
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(systemService.fAQCategories(userToken));
            return Json(new RequestResult { Failed = false });
        }

        [HttpPost]
        public IHttpActionResult FAQQuestions(int cateogryId, [FromBody] ListParams @params)
        {
            //using (var db = new DBModel())
            //{
            //    var totalCount = db.FAQQuestions.Where(w => w.FaqCategoryId == cateogryId).Count();

            //    return Json(new ListResult<FAQQuestion>
            //    {
            //        dataHeader = Request.Headers,
            //        page = @params.@params.page,
            //        pageCount = totalCount / @params.@params.limit,
            //        totalCount = totalCount,
            //        data = db.FAQQuestions.Where(w => w.FaqCategoryId == cateogryId).OrderBy(o=>o.QuestionText).Skip( (@params.@params.page -1) * @params.@params.limit).Take(@params.@params.limit).ToList(),
            //        limit = @params.@params.limit
            //    });
            //}
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(systemService.fAQQuestions(cateogryId, @params, userToken, Request.Headers));
            return Json(new RequestResult { Failed = false });

        }
        [HttpPost]
        public IHttpActionResult CanSkipRating() {
            return Json(new RequestResult {
                Failed = false,
                Data = true
            });
        }

    }
}
