﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Web.App
{
    // public class FormActionButton
    // {
    //     private const string _buttonFormat = "    <button type = \"{2}\" class=\"btn {1}\" {3}>{0}</button>";
    //     public string Label { get; set; }
    //     public string CssClass { get; set; }
    //     public string Type { get; set; }
    //     public string OtherProperties { get; set; }
    //     public static FormActionButton SubmitButton()
    //     {
    //         return new FormActionButton
    //         {
    //             Type = "submit",
    //             CssClass = "btn blue",
    //             Label = "Submit"
    //         };
    //     }
    //     public static FormActionButton ResetButton()
    //     {
    //         return new FormActionButton
    //         {
    //             Type = "reset",
    //             CssClass = "btn-default",
    //             Label = "Reset"
    //         };
    //     }
    //     public virtual string GetButtonString(object helper)
    //     {
    //         return string.Format(_buttonFormat, Label, CssClass, Type, OtherProperties);
    //     }
    // }
    // public class FormActionLink : FormActionButton
    // {
    //     public string ViewName { get; set; }
    //     public object RouteValues { get; set; }
    //     public override string GetButtonString(object helper)
    //     {
    //         HtmlHelper h = helper as HtmlHelper;
    //         return h.ActionLink(Label, ViewName, RouteValues, new { @class = CssClass }).ToHtmlString();
    //     }
    //     public static FormActionLink BackButton(string returnView = "Index", object returnViewRouteValue = null) {
    //         return new FormActionLink { Label = " Back", CssClass = "btn fa fa-arrow-left", ViewName = returnView, RouteValues = returnViewRouteValue };
    //     }
    // }

    // public struct ListAction
    // {
    //     private const string _listButtonTemplate = "        <a onclick=\"{1}\" class=\"btn btn-circle btn-icon-only {2} \" {4}><i class=\"{3}\"></i> {0}</button>";

    //     public string Label { get; set; }
    //     public string OnClickEvent { get; set; }
    //     public string Color { get; set; }
    //     public string Icon { get; set; }
    //     public string OtherProperties { get; set; }
    //     public static string UrlNavigateClickEvent(string url) { return string.IsNullOrEmpty(url) ? string.Empty : string.Format("location.href = '{0}';", url); }
    //     public static ListAction addButton(HtmlHelper helper, string createUrl = null)
    //     {

    //         if (string.IsNullOrEmpty(createUrl))
    //         {
    //             System.Web.Routing.RequestContext requestContext = HttpContext.Current.Request.RequestContext;
    //             string controller = helper.ViewContext.RouteData.Values["controller"].ToString();
    //             UrlHelper urlHelper = new UrlHelper(requestContext);
    //             createUrl = urlHelper.Action("Create", controller);
    //         }
    //         return new ListAction
    //         {
    //             Label = "",
    //             Icon = "fa fa-plus",
    //             OnClickEvent = string.Format("location.href = '{0}';", createUrl),
    //             Color = "btn-primary"
    //         };
    //     }
    //     public string GetButtonString()
    //     {
    //         return string.Format(_listButtonTemplate, Label, OnClickEvent, Color, Icon, OtherProperties);
    //     }
    // }

    // public static class FormExtension
    // {
    //     private const string RowFormat = " <div class=\"form-group form-md-line-input form-md-floating-label\">"+

    //             "       {1}"+
    //             "        {0}"+
    //             "        {2}"+
    //             "</div>";

    //     private const string ActionRowFormat =  "<div class=\"form-actions noborder\">"+
    //         "    <button type=\"button\" class=\"btn blue\">Submit</button>"+
    //         "    <button type=\"button\" class=\"btn default\">Cancel</button>"+
    //         "{0}"+
    //         "</div>";
    //     private const string _actionRowFormat = "<div class=\"form-actions noborder\">" +
    //             "    {0}" +
    //         "</div>";


    //     private const string BooleanSwitch = "<div class=\"onoffswitch greensea\">" +
    //             "<input type = \"checkbox\" name=\"{0}\" class=\"onoffswitch-checkbox\" id=\"{0}\" {1}>" +
    //             "<label class=\"onoffswitch-label\" for=\"myonoffswitch14\">" +
    //             "    <div class=\"onoffswitch-inner\"></div>" +
    //             "    <div class=\"onoffswitch-switch\"></div>" +
    //             "</label>" +
    //         "</div>";

    //     private const string BooleanSwitchKendo = "<script>" +
    //         "$(function() {" +
    //             "$(\"#{0}\").kendoMobileSwitch();" +
    //             "});" +
    //     "</script>";

    //     private const string _formHeader = " <div class=\"portlet-title\">"+
    // "    <div class=\"caption {1}\">"+
    // "        <span class=\"caption-subject bold uppercase\">{0}</span>"+
    // "    </div>"+
    // "    <div class=\"actions\">"+
    // "       {2} "+ 
    // "        <a class=\"btn btn-circle btn-icon-only btn-default fullscreen\" href=\"javascript:;\" data-original-title=\"\" title=\"\"> </a>"+
    // "    </div>"+
    // "</div>";

    //     private const string _formHeaderTabbed = " <div class=\"portlet-title tabbable-line\">" +
    //"    <div class=\"caption {1}\">" +
    //"        <span class=\"caption-subject bold uppercase\">{0}</span>" +
    //"    </div>" +
    //" <ul class=\"nav nav-tabs\">{3}</ul>"+
    //"    <div class=\"actions\">" +
    //"       {2} " +
    //"        <a class=\"btn btn-circle btn-icon-only btn-default fullscreen\" href=\"javascript:;\" data-original-title=\"\" title=\"\"> </a>" +
    //"    </div>" +
    //"</div>";
    //     public enum InputType { Default, Password, TextArea, DropDownList }


    //     public static MvcHtmlString FormHeader(this HtmlHelper helper,string headerText, string fontColor = "font-green", params ListAction[] actions) {

    //         var actionStr = new System.Text.StringBuilder();
    //         foreach (var item in actions)
    //             actionStr.Append(item.GetButtonString());

    //         return new MvcHtmlString(string.Format(_formHeader, headerText, fontColor, actionStr.ToString()));
    //     }
    //     public static MvcHtmlString FormHeaderTabbed(this HtmlHelper helper, string headerText, string fontColor = "font-green", ListActions[] actions = null, FormTab[] tabs = null) {


    //     }
    //     public static MvcHtmlString FormEditorRow<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
    //     {
    //         return FormEditorRow(helper, expression, string.Empty);
    //     }
    //     public static MvcHtmlString FormEditorRow<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string label, IHtmlString control)
    //     {
    //         return new MvcHtmlString(FormEditorRowTemplate(helper, expression, label, control));
    //     }
    //     private static string FormEditorRowTemplate<TModel, TValue>(HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object label, IHtmlString control)
    //     {
    //         return string.Format(RowFormat,
    //             GetLabel(helper, expression, (label == null ? string.Empty : label.ToString())),
    //             control.ToHtmlString(),
    //             helper.ValidationMessageFor(expression)
    //             );
    //     }

    //     private static string GetLabel<TModel, TValue>(HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string label)
    //     {
    //         object labelProp = null;// new { @class = "col-sm-4 control-label" };
    //         return
    //             expression != null ?
    //                 (label != string.Empty ? helper.Label(expression.ToString(), label.ToString(), labelProp).ToHtmlString()
    //                     : helper.LabelFor(expression, labelProp).ToHtmlString())
    //                : GetLabel(label);
    //     }
    //     private static string GetLabel(string label)
    //     {
    //         return string.Format(" <label>{0}</label>", label);
    //     }

    //     public static MvcHtmlString FormEditorRow<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string label, InputType type = InputType.Default, IHtmlString control = null, dynamic list = null)
    //     {
    //         bool isBoolean = expression.ReturnType == typeof(bool); /*TODO: Put switch control instead of checkbox*/

    //         IHtmlString ctrl = control;

    //         if (control == null)
    //         {
    //             switch (type)
    //             {
    //                 case InputType.Default:
    //                 case InputType.Password:
    //                     ctrl = helper.EditorFor(expression);
    //                     break;
    //                 case InputType.TextArea:
    //                     ctrl = helper.TextAreaFor(expression, new { @class = "form-control", @rows = 4 });
    //                     break;
    //                 case InputType.DropDownList:
    //                     ctrl = helper.DropDownListFor(expression, (IEnumerable<SelectListItem>)list);

    //                     break;
    //                 default:
    //                     break;
    //             }
    //         }

    //         string str = FormEditorRowTemplate(helper, expression
    //            , label
    //            , ctrl)
    //            .Replace("class=\"k-textbox\"", "class=\"form-control\" type=\"text\"");

    //         if (type == InputType.Password)
    //             str = str.Replace("type=\"text\"", "type=\"password\"");

    //         return new MvcHtmlString(str);
    //     }
    //     public static MvcHtmlString FormEditorRowPassword<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
    //     {
    //         return FormEditorRow(helper, expression, string.Empty, InputType.Password);
    //     }
    //     public static MvcHtmlString FormEditorRowTextArea<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
    //     {
    //         return FormEditorRow(helper, expression, string.Empty, InputType.TextArea);
    //     }

    //     public static MvcHtmlString FormActionsRow<TModel>(this HtmlHelper<TModel> helper, string returnView = "Index", object returnViewRouteValue = null)
    //     {
    //         /*
    //         string str = string.Format(ActionRowFormat,
    //             helper.ActionLink(" Back", returnView, returnViewRouteValue, new { @class = "btn fa fa-arrow-left" }).ToHtmlString()
    //             );
    //         return new MvcHtmlString(str);*/
    //         return FormActionsRow(helper, FormActionButton.SubmitButton(), FormActionButton.ResetButton()
    //                 , new FormActionLink { Label = " Back", CssClass = "btn fa fa-arrow-left", ViewName = returnView, RouteValues = returnViewRouteValue });
    //     }
    //     private static string GetButtons(HtmlHelper helper, params FormActionButton[] buttons)
    //     {
    //         if (buttons == null || buttons.Length < 1) return "";
    //         System.Text.StringBuilder builder = new System.Text.StringBuilder();

    //         foreach (FormActionButton item in buttons)
    //             builder.Append(item.GetButtonString(helper));

    //         return builder.ToString();
    //     }
    //     public static MvcHtmlString FormActionsRow<TModel>(this HtmlHelper<TModel> helper, params FormActionButton[] buttons)
    //     {
    //         return new MvcHtmlString(string.Format(_actionRowFormat, GetButtons(helper, buttons)));
    //     }
    //     public static MvcHtmlString ClientIdFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    //     {
    //         return MvcHtmlString.Create(
    //             htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(ExpressionHelper.GetExpressionText(expression)));
    //     }
    // }

    public static class FormExtension {


        #region private HTML Properties
        private const string _formHeaderTemplate = " <div class=\"portlet-title {2}\">{0}{1}</div>";
        private const string _formActionTemplate = "<div class=\"form-actions\">" +
                        "<div class=\"row\">" +
                            "<div class=\"col-md-offset-3 col-md-9\">{0}" +
                            "</div>" +
                     "   </div>" +
                    "</div>";

        private const string _hiddenColumnTemplate = "#= {0} #" +
                                 "<input type='hidden' class='{2}' name='{1}[#= index('{1}',data)#].{0}' value='#= {0} #' />";

       
        #endregion


        #region Base Functions and Extensions
        public static MvcHtmlString FormHeader(this HtmlHelper helper,
            FormHeaderCaption header,
            FormHeaderButton[] buttons,
            FormHeaderSpecialTab[] tabs
            )
        {
            var isTabbed = tabs != null && tabs.Length > 0;
            return new MvcHtmlString(string.Format(_formHeaderTemplate,
                header,
                isTabbed ? FormHeaderSpecialTab.GetTabs(tabs) : FormHeaderButton.GetButtons(buttons),
                isTabbed ? "tabbable-line" : ""));
        }
        public static MvcHtmlString FormHeaderTabbed(this HtmlHelper helper,
            FormHeaderCaption header, params FormHeaderSpecialTab[] tabs)
        {
            return FormHeader(helper, header, null, tabs);
        }
        public static MvcHtmlString FormHeaderTabbed(this HtmlHelper helper,
            string header, params FormHeaderSpecialTab[] tabs) {
            return FormHeaderTabbed(helper, new FormHeaderCaption { Text= header, Color="font-green" }, tabs);
        }
        public static MvcHtmlString FormHeaderWithButtons(this HtmlHelper helper,
            FormHeaderCaption header, params FormHeaderButton[] buttons)
        {
            return FormHeader(helper, header, buttons, null);
        }
        public static MvcHtmlString FormHeaderWithButtons(this HtmlHelper helper,
            string header, params FormHeaderButton[] buttons)
        {
            return FormHeaderWithButtons(helper, new FormHeaderCaption { Text= header, Color= "font-green" }, buttons);
        }

        public static string AddHiddenColumn(this HtmlHelper helper, string column, string model, string addedClass = "")
        {
            return string.Format(_hiddenColumnTemplate, column, model, addedClass);
        }
        public static MvcHtmlString FormActionsRow(this HtmlHelper helper, string returnView = "Index", object returnViewRouteValue = null, string tabFragment = "")
        {
            return FormActionsRow(helper, FormActionButton.SubmitButton(), FormActionButton.ResetButton(), FormActionLink.BackButton(returnView, returnViewRouteValue, tabFragment));
        }
        public static MvcHtmlString FormActionsRow(this HtmlHelper helper, params FormActionButton[] buttons)
        {
            return new MvcHtmlString(string.Format(_formActionTemplate,FormActionButton.GetButtons(buttons)));
        }
        private static MvcHtmlString FormEditorRow(this HtmlHelper helper, string label, string control, string validation, bool disabled, bool required, string helptext)
        {
            return new MvcHtmlString( new FormControl { Label = label, Control = control,Validation=validation ,Required = required, HelperText = helptext, Disabled = disabled }.ToString());
        }
        public static MvcHtmlString FormEditorRow(this HtmlHelper helper, string label, IHtmlString control, IHtmlString validation = null, bool disabled = false, bool required = false, string helptext = "")
        {

            if (required == true)            {
                var sb = new StringBuilder();                sb.Append(label);
                sb.Append(" ");                sb.Append("<font color=\"red\">" + '*' + "</font>");
                label = sb.ToString();            }
            validation = validation != null ? helper.ValidationMessage(control.ToHtmlString()) : null;
            return FormEditorRow(helper, label, control==null?"": control.ToHtmlString(), validation == null ? "" : validation.ToHtmlString(), disabled, required, helptext);
        }

        public static MvcHtmlString FormMultiSelectRow<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, IEnumerable<SelectListItem> list, bool required = false, string label = "", bool disabled = false)
        {
            var lbl = helper.LabelFor(expression, new { @class = "col-md-2 control-label" });
            var msl = helper.Kendo().MultiSelectFor(expression)
                .DataTextField("Text")
                .DataValueField("Value")
                .AutoClose(false)
                .TagMode(TagMode.Single)
                .Filter(FilterType.Contains)
                .HtmlAttributes(new { style = "width: 100%" });
            if (list != null) msl.BindTo(list);
            if (!required) msl.Placeholder("Select ...");
            var validation = helper.ValidationMessageFor(expression);
            return FormEditorRow(helper, string.IsNullOrEmpty(label) ? lbl.ToHtmlString() : label, msl,validation ,disabled);
        }
        public static MvcHtmlString FormDropDownTreeRow<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, IEnumerable<DropDownTreeItemModel> list, bool required = false, string label = "")
        {
            var lbl = helper.LabelFor(expression, new { @class = "col-md-2 control-label" });
            var msl = helper.Kendo().DropDownTreeFor(expression)
                .Filter("contains")
                //.Value(expression)
                .HtmlAttributes(new { style = "width: 100%" });
            if (list != null) msl.BindTo(list);
            if (!required) msl.Placeholder("Select ...");

            return FormEditorRow(helper, string.IsNullOrEmpty(label) ? lbl.ToHtmlString() : label, msl);
        }

        public static MvcHtmlString FormDropDownListRow<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, IEnumerable<SelectListItem> list, bool required = false, string label = "", bool disabled = false) {
            var lbl = helper.LabelFor(expression, new { @class = "col-md-2 control-label" });
            var ddl = helper.Kendo().DropDownListFor(expression)
                .DataTextField("Text")
                .DataValueField("Value")
                .Filter(FilterType.Contains)
                .HtmlAttributes(new { style = "width: 100%" });
            if (list != null) ddl.BindTo(list);
            if (!required) ddl.OptionLabel("Select ...");
            var validation = helper.ValidationMessageFor(expression);
            return FormEditorRow(helper, string.IsNullOrEmpty(label) ? lbl.ToHtmlString() : label, ddl, validation,disabled);
        }
        public static MvcHtmlString FormDropDownListRow<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string valueMapper, string readAction, string controller = "Lookup", string textField = "Text", string valueField = "Value",string nameTemplate = "#= Text # ", bool required = false, string label = "")
        {
            var lbl = helper.LabelFor(expression, new { @class = "col-md-2 control-label" });
            var ddl = helper.Kendo().DropDownListFor(expression)
                .DataTextField(textField)
                .DataValueField(valueField)
                .MinLength(3)
                .Height(290)
                .Template(nameTemplate)
                .Filter(FilterType.Contains)
                .HtmlAttributes(new { style = "width: 100%" })
                .DataSource(source => {
                    source.Custom()
                        .ServerFiltering(true)
                        .ServerPaging(true)
                        .PageSize(80)
                        .Type("aspnetmvc-ajax") //Set this type if you want to use DataSourceRequest and ToDataSourceResult instances
                        .Transport(transport =>{transport.Read(readAction, controller);})
                        .Schema(schema =>
                        {
                            schema.Data("Data") //define the [data](http://docs.telerik.com/kendo-ui/api/javascript/data/datasource#configuration-schema.data) option
                                  .Total("Total"); //define the [total](http://docs.telerik.com/kendo-ui/api/javascript/data/datasource#configuration-schema.total) option
                  });
                })
                .Virtual(v => v.ItemHeight(26).ValueMapper(valueMapper));

            if (!required) ddl.OptionLabel("Select ...");

            return FormEditorRow(helper, string.IsNullOrEmpty(label) ? lbl.ToHtmlString() : label, ddl);
        }

        public static MvcHtmlString FormEditorRow<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string label = "", bool disabled = false, bool required = false, string helperText = "")
        {
            var ctrl = helper.EditorFor(expression).ToHtmlString();
            var lbl = helper.LabelFor(expression, new { @class = "col-md-2 control-label" });
            if (required == true)            {                var sb = new StringBuilder();                if (label == "")                {                    var express = ExpressionHelper.GetExpressionText(expression);                    sb.Append(express);                }                else                    sb.Append(label);                sb.Append(" ");                sb.Append("<font color=\"red\">" + '*' + "</font>");                label = sb.ToString();            }
            var validation = helper.ValidationMessageFor(expression).ToHtmlString();
            return FormEditorRow(helper, string.IsNullOrEmpty(label)? lbl.ToHtmlString(): label, ctrl,validation ,disabled, required, helperText);
        }

        public static MvcHtmlString AddButtonForChild(this HtmlHelper helper, string actioName, object routevalues = null)
        {
            System.Web.Routing.RequestContext requestContext = HttpContext.Current.Request.RequestContext;
            var controller = helper.ViewContext.RouteData.Values["controller"].ToString();
            var urlHelper = new UrlHelper(requestContext);
            var createUrl = urlHelper.Action(actioName, controller, routevalues);
            return new MvcHtmlString(string.Format("<a class=\"btn sbold green\" href=\"{0}\">Add New <i class=\"fa fa-plus\"></i></a><p></p>", createUrl));
        }

        #endregion

        #region aux extensions
        public class FormControl
        {
            private const string _template = " <div class=\"form-group form-md-line-input {4}\">" +
            "        {0}" +
            "      <div class=\"col-md-10\"> {1}" +
                "<div class=\"form-control-focus\"> </div>" +
                "<font color=\"red\" size=\"-1.5\"> {2} </font> " +
                "        {3}" +
                "</div>" +
                "</div>";

            public string Label { get; set; }
            public string Control { get; set; }
            public string Validation { get; set; }
            public bool Required { get; set; }
            public bool Disabled { get; set; }
            public string HelperText { get; set; }
            public int ControlSize { get; set; }

            public FormControl()
            {
                Label = Control = HelperText = "";
                ControlSize = 12;
            }

            public override string ToString() {
                if (!Label.Contains("<label"))
                    Label = string.Format("<label class=\"col-md-2 control-label\" >{0}</label>", Label);

                if (Control.Contains("text"))
                {
                    Control = Control.Replace("k-textbox", "form-control");
                    Control = Control.Replace("text-box single-line", "form-control");
                }
                if(Disabled)
                  Control = Control.Replace("<input", "<input readonly=\"readonly\"");

                return string.Format(_template,
                    Label,
                    Control,
                    Validation,
                    string.IsNullOrEmpty(HelperText) ? "" : string.Format("<span class=\"help-block\">{0}</span>", HelperText),
                    ""//Required ? "has-error" : ""
                    );
            }
            public static string DrawControls(params FormControl[] controls)
            {
                ///prepare the template
                
                /// if controls does not have size then the size should be 12/controls count
                /// if some have size then the rest is 12-(sizes)/rest
                
                return "";
            }
        }
        public static MvcHtmlString FormRow(this HtmlHelper html, params FormControl[] controls) {
            return new MvcHtmlString(FormControl.DrawControls(controls));
        }
        #endregion
    }
}