﻿using Services.Mobile.Business;
using Services.Mobile.Business.MerchantQRScan;
using System.Web.Http;
using static Services.Helper.EnumHelpers;

namespace Mobile.APIsCashier
{
    public class MerchantQRScanController : ApiController
    {
        MerchantQRScanService merchantQRScanService = new MerchantQRScanService();

        [HttpPost]
        public IHttpActionResult ScanQR([FromBody]ConsumerContract<string> code)
        {
            var userToken = Request.Headers.Authorization.Parameter;

            if (ModelState.IsValid)
                return Json(merchantQRScanService.scanQR(code.wcObject, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }


        [HttpPost]
        public IHttpActionResult SubmitInvoiceAmount([FromBody]InvoiceDetailsDto details)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(merchantQRScanService.submitInvoiceAndRedeemPoints(details,userToken));
            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });

        }
    }

   
}
