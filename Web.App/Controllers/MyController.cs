﻿using AutoMapper;
using DB.ORM.DB;
using Services.Base;
using Services.Base.Base;
using Services.Web.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Models;
using Web.Helpers.File;
using static DB.ORM.DB.Merchant;
using Profile = DB.ORM.DB.Profile;

namespace Web.App.Controllers
{
    public class MyController : BaseController
    {
        MyProfileService service;
        public MyController():base()
        {
            ApplySecurity = false;
          
        }
        // GET: My Profile
        public override ActionResult Index()
        {
            return RedirectToAction("Profile");
        }
        public ActionResult Profile() {
            service = new MyProfileService(UserId);
            return View(Mapper.Map<Profile>(service.GetOne(c=>c.MerchantId == UserId)));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile(Profile model)
        {
            var err = "";
            service = new MyProfileService(UserId);

            if (ModelState.IsValid) {
                var PropertiesToUpdate = new Expression<Func<Merchant, object>>[]
                     { x=>x.Email, x=>x.Facebook, x=>x.Mobile, x=>x.Address };
                var res = service.Update(Mapper.Map<Merchant>(model), PropertiesToUpdate);
                if (res)
                    ShowSuccess(ZM.Message.SuccessEditCode);
                else
                    ShowError(ZM.Message.ErrorCode);
            }
            err += GetModelStateErrors(ModelState);
            if (!string.IsNullOrEmpty(err))
                ShowError(err);

            return View(Mapper.Map<Profile>(new BaseService<Merchant>().GetOne(c => c.MerchantId == UserId)));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeImage(HttpPostedFileBase PictureFile)
        {
            service = new MyProfileService(UserId);
            RunResult result = service.SavePicture(PictureFile);
            if (result.Succeeded)
                ShowSuccess(result.SuccessDesc, true);
            else
                ShowError(result.ErrorDesc, true);

            return RedirectToAction("Profile");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePass(VMChangePassword model)
        {
            if (!new MyProfileService(UserId).SavePassword(model.CurrentPassword, model.NewPassword))
                ShowError(ZM.Message.ErrorwhileSavingPass, true);
            else
                ShowSuccess(ZM.Message.SuccessEditCode, true);
            return RedirectToAction("Profile");
        }
    }
}