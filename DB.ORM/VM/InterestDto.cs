﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DB.ORM.DB
{
    [MetadataType(typeof(InterestMetaDataDto))]
    public partial class Interest 
    {
        class InterestMetaDataDto
        {
            [Required(ErrorMessageResourceName = "RequiredInterestName", ErrorMessageResourceType = typeof(ZM.InterestAdmin))]
            public string InterestName { get; set; }
        }
        [NotMapped]
        public class InterestDto : Interest
        {
            public int Id { get; set; }
            public string Logo { get; set; }
            public HttpPostedFileBase PictureFile { get; set; }
        }
    }
}
