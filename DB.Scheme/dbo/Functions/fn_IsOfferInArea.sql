﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION fn_IsOfferInArea
(	
 @PromotionRunId int,
 @Lat  nvarchar(20),
 @Lng  nvarchar(20)
)
RETURNS TABLE 
AS
RETURN 
(
	 select Max(cast([dbo].[fn_InGeoArea](b.Lat, b.Lng, @Lat, @Lng) as int)) as InArea from PromotionRunBranch r join
     MerchantBranch b on b.BranchId = r.BranchId 
     where r.PromotionRunId = @PromotionRunId 
)