﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.App.Models
{
    public class VMCampaignBranch
    {
        public int BranchId { get; set; }
        public int MerchantId { get; set; }

        public string Address { get; set; }
        public string AreaName { get; set; }
        public int Selected { get; set; }

    }
}