﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Web.App.Admin.Controllers
{
    public class AreaController : BaseControllerTemplate<Area, Area, int>
    {
        public AreaController()
        {
            ViewNamePlural = "Areas";
            ViewNameSingular = "Area";

            PropertiesToUpdate = new Expression<Func<Area, object>>[]
                {
                    x=>x.AreaId,
                    x=>x.AreaName,
                    x=>x.Lat,
                    x=>x.Lng
                };
        }
        protected override Expression<Func<Area, bool>> GetOneExpression(int? id) => id.HasValue ? (x => x.AreaId == id.Value) : (Expression<Func<Area, bool>>)(x => 1 == 0);

        public override ActionResult Create()
        {
            FillViewBag(true);

            decimal lat = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["DefaultLat"]);
            decimal lng = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["DefaultLng"]);
            
            return View(new Area { Lat = lat, Lng = lng });
        }
    }
}