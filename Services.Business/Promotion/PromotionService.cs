﻿using DB.ORM.DB;
using Services.Web.Business.Base;
using Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Services.Base;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using static Services.Helper.EnumHelpers;

namespace Services.Web.Business
{
    public class PromotionService : BaseService<Promotion>
    {
        public PromotionService()
        {
        } 
        public UnitOfWork InitiateUnitOfWork()
        {
            DBModel db = new DBModel();
            var unitOfWork = new UnitOfWork(db, new GenericRepository<Promotion>(db),
                                                      new GenericRepository<PromotionImage>(db));
            return unitOfWork;
        }

        public bool SavePromotionImages(List<string> files, string essentialPath, int promotionId, UnitOfWork unitOfWork)
        {
            try
            {
                int ImgCounter = 1;
                foreach (var file in files)
                {
                    var filePath = essentialPath + file;
                    unitOfWork.AddEntity(new PromotionImage
                    {
                        PromotionId = promotionId, ImageUrl = filePath,
                        SortOrder = ImgCounter,
                        IsLogo = ImgCounter == 1 ? true : false
                    }) ;
                    ImgCounter++;
                }
            }
            catch (Exception e)
            {

                return false;
            }
            return true;
        }
        public bool SetPromotionalLogo(int id, UnitOfWork unitOfWork)
        {
            try
            {
                var promotionImages = unitOfWork.GetEntities<PromotionImage>();
                foreach (var image in promotionImages)
                {
                    image.IsLogo = image.PromotionImageId == id ? true : false;
                    unitOfWork.AddOrUpdateEntity(image);
                }
            }
            catch (Exception e) { return false; }
            return true;
        }

        public bool RemovePromotionImages(int promotionId, string fileName, UnitOfWork unitOfWork)
        {
            try
            {
                var imgId = unitOfWork.GetEntities<PromotionImage>().AsQueryable().FirstOrDefault(x => x.PromotionId == promotionId && x.ImageUrl.Contains(fileName)).PromotionImageId;
                if (deleteImage(imgId))
                     unitOfWork.RemoveOneEntity<PromotionImage>(x => x.PromotionId == promotionId && x.ImageUrl.Contains(fileName));

            }
            catch (Exception e) { return false; }

            return true;
        }
        public bool sendPromotion(int id)
        {
            try
            {
                var promotion = GetOne(x => x.PromotionId == id);
                promotion.IsDraft = promotion.IsDraft == true ? false : false;
                AddOrUpdate(promotion);
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }

        
        public bool deleteImage(int id, UnitOfWork unitOfWork = null)
        {
            try 
            {
                using (var client = new HttpClient())
                {
                    string baseUrl = GetBaseUrl;
                    string pathUrl = "";

                    if (unitOfWork != null)
                    {
                        var image = unitOfWork.FindEntity<PromotionImage>(c => c.PromotionImageId == id);
                        if (image != null)
                            pathUrl = image.ImageUrl;
                    }
                    else
                    {
                        var image = GetOne<PromotionImage>(c => c.PromotionImageId == id);
                        if (image != null)
                            pathUrl = image.ImageUrl;
                    }

                    var Data = new StringContent(pathUrl, Encoding.UTF8);
                    var requestUri = baseUrl + GetDeleteUrl;
                    var result = client.PostAsync(requestUri, Data).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        if(unitOfWork != null)
                            unitOfWork.RemoveOneEntity<PromotionImage>(c => c.PromotionImageId == id);
                        else
                            Delete<PromotionImage>(c => c.PromotionImageId == id);

                        return true;
                    }
                    else
                        return false;

                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
            
        }

        // Can delete in all status except approved
        public bool deletePromotion(int id) 
        {
            try 
            {
                var promotion = GetOne(x => x.PromotionId == id);
                if (promotion != null)
                {
                    if (promotion.ApprovedDate == null && !Any<PromotionRun>(a=>a.PromotionId==promotion.PromotionId))
                    {
                        var promotionImage = Get<PromotionImage>(a => a.PromotionId == id);
                        foreach (var item in promotionImage)
                        {
                            deleteImage(item.PromotionImageId);
                        }
                        Delete(a => a.PromotionId == id);
                        return true;
                    }
                    return false;
                 }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
            return false;
        }

        public bool uploadFiles(HttpPostedFileBase[] files, string section, int fileId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    foreach (var file in files)
                    {
                        using (var content = new MultipartFormDataContent())
                        {

                            byte[] Bytes = new byte[file.InputStream.Length + 1];
                            file.InputStream.Read(Bytes, 0, Bytes.Length);
                            var fileContent = new ByteArrayContent(Bytes);
                            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = file.FileName, Name = section + "\\" + fileId };
                            content.Add(fileContent);
                            var requestUri = GetBaseUrl + GetUploadUrl;
                            var result = client.PostAsync(requestUri, content).Result;

                            if (result.IsSuccessStatusCode)
                                return true;
                            else
                                return false;

                        }
                    }
                }
            }
            catch (Exception)
            {

                return false;
            }
            return false;
        }

        public bool uploadFiles(PromotionFiles[] files, string section, int fileId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    foreach (var file in files)
                    {
                        using (var content = new MultipartFormDataContent())
                        {
                            var Bytes = Convert.FromBase64String(file.fileContent.Substring(file.fileContent.IndexOf(',') + 1));
                            var fileContent = new ByteArrayContent(Bytes);
                            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = file.fileName, Name = section + "\\" + fileId };
                            content.Add(fileContent);
                            var requestUri = GetBaseUrl + GetUploadUrl;
                            var result = client.PostAsync(requestUri, content).Result;
                        }
                    }
                    return true;
                }
            }
            catch (Exception)
            {

                return false;
            }
            return false;
        }

        public List<PromotionImage> readImages(List<PromotionImage> Entites)
        {
            string baseUrl = GetBaseUrl;
        
            foreach (var item in Entites)
            {
                if (!item.ImageUrl.Contains(baseUrl))
                    item.ImageUrl = baseUrl + item.ImageUrl;
            }

            return Entites;
        }
      
    }
}
