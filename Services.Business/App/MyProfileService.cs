﻿using DB.ORM.DB;
using Services.Base;
using Services.Base.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Web.Helpers.File;

namespace Services.Web.Business
{
    public class MyProfileService : BaseService<Merchant>
    {
        private new int UserId { get; set; }
        public MyProfileService(int userid)
        {
            UserId = userid;
        }
        public bool SavePassword(string currentPassword, string newPassword)
        {
            if (currentPassword.IsEmpty() || newPassword.IsEmpty()) return false;

            using (var db = new DBModel())
            {
                db.UserId = UserId;
                var emp = db.Merchants.FirstOrDefault(w => w.MerchantId == UserId);

                if (emp.Password == LoginService.HashString(currentPassword))
                    emp.Password = LoginService.HashString(newPassword);
                else
                    return false;
                return db.SaveChanges() > 0;
            }
        }

        public RunResult SavePicture(HttpPostedFileBase PictureFile)
        {
            try
            {
                if (PictureFile != null && PictureFile.ContentLength >= 1)
                {
                    var logo = "Uploads" + '/' + ZM.Uploads.Merchant + '/' + UserId + '/' + PictureFile.FileName;
                    if (PictureFile != null && PictureFile.ContentLength > 0)
                    {
                        var oldPath = GetOne(a => a.MerchantId == UserId).MerchantLogo;
                        var isDeleted = FileHelper.DeleteFiles(oldPath, GetBaseUrl, GetDeleteUrl);
                        if (isDeleted)
                        {

                            var isUploaded = FileHelper.UploadFiles(ZM.Uploads.Merchant, UserId.ToString(),
                                                            GetBaseUrl, GetUploadUrl, PictureFile);
                            if (isUploaded)
                            {
                                var merchant = GetOne(c => c.MerchantId == UserId);
                                if (merchant != null)
                                {
                                    merchant.MerchantLogo = logo;
                                    AddOrUpdate(merchant);
                                }
                                return new RunResult { Succeeded = true, SuccessDesc = ZM.Message.SuccessEditCode };
                            }
                        }
                        return new RunResult { ErrorDesc = ZM.Message.Errorwhilesaving };
                    }
                }
                return new RunResult { Succeeded = true, SuccessDesc = ZM.Message.SuccessEditCode };

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            return new RunResult { ErrorDesc = ZM.Message.ErrorCode };
        }
    }
}
