namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PromotionRateType")]
    public partial class PromotionRateType
    {
        public int PromotionRateTypeId { get; set; }

        [Column("PromotionRateType")]
        [Required]
        public string PromotionRateType1 { get; set; }

        [Required]
        public string PromotionRateTypeAr { get; set; }
    }
}
