namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_SystemRole
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SystemRoleId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string RoleName { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool IsAdmin { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool Active { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UsersCount { get; set; }

        public DateTime? LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        public string UpdatedByLogonName { get; set; }
    }
}
