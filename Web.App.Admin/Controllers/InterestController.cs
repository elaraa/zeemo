﻿using DB.ORM.DB;
using Services.Base;
using Services.Web.Business.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using static DB.ORM.DB.Interest;

namespace Web.App.Admin.Controllers
{
    public class InterestController : BaseControllerTemplateDto<vw_Interests, Interest, InterestDto, int>
    {
        public InterestController()
        {
            ViewNamePlural = ZM.ViewPages.InterestViewNamePlural;
            ViewNameSingular = ZM.ViewPages.InterestViewNameSingular;

            PropertiesToUpdate = new Expression<Func<Interest, object>>[]
                {
                    x=>x.InterestName,
                    x=>x.InterestNameAr,
                    x=>x.Icon
                };
        }
        protected override void InitTemplateServices()
        {
            ListService = new BaseService<vw_Interests>(UserId);
            ActionService = new InterestService();
        }
        protected override Expression<Func<Interest, bool>> GetOneExpression(int? id) => id.HasValue ? (x => x.InterestId == id.Value) : (Expression<Func<Interest, bool>>)(x => 1 == 0);

    }
}