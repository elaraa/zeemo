﻿using Kendo.Mvc.UI;
using Services.Base;
using Services.Web.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.App.Controllers;

namespace Web.App
{
    public class LookupHelper
    {
        private int UserId { get; set; }
        public LookupHelper(BaseController controller)
        {
            UserId = controller.UserId;
        }
        public IQueryable<TModel> GetMyData<TModel>() where TModel : class, new()
        {
            return new BaseService<TModel>(UserId).Get(null, true);
        }
        public IEnumerable<SelectListItem> GetMyDropDownListData<TModel>(Func<TModel, SelectListItem> selector) where TModel : class, new()
        {
            return GetMyData<TModel>().ToDropDownList(selector);
        }
       
        public static IEnumerable<SelectListItem> GetGenders(BaseController controller) {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "Male", Text = "Male" },
                new SelectListItem { Value = "Female", Text = "Female" }
            };
        }
        public static IEnumerable<SelectListItem> GetAreas(BaseController controller) {
            return new BaseService<DB.ORM.DB.Area>(controller.UserId).Get(null, true).Select(s => new SelectListItem { Text = s.AreaName, Value = s.AreaId.ToString() }).ToList();
        }
        public static IEnumerable<SelectListItem> GetPromotionTypes(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.PromotionType>(controller.UserId)
                .Get(null, true).Select(s => new SelectListItem { Text = s.PromotionTypeName, Value = s.PromotionTypeId.ToString() }).ToList();
        }
        public static IEnumerable<SelectListItem> GetPromotions(BaseController controller)
        {
            return new BaseService<DB.ORM.DB.Promotion>(controller.UserId)
                .Get(c=>c.ApprovedDate != null && c.RejectedDate == null, true).Select(s => new SelectListItem { Text = s.PromotionName, Value = s.PromotionId.ToString() }).ToList();
        }

        public static IEnumerable<SelectListItem> GetMaritalStatuses(BaseController controller)
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "S", Text = "Single" },
                new SelectListItem { Value = "M", Text = "Married" }
            };
        }
    }
}