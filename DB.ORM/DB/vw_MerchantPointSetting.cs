namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_MerchantPointSetting
    {
        [Key]
        public bool HasLoyalty { get; set; }

        public int? PointsActivationDuration { get; set; }

        public int? PointsExpirationDuration { get; set; }

        public decimal? PointsAmount { get; set; }

        public decimal? Pounds { get; set; }
    }
}
