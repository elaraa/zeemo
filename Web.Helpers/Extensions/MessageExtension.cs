﻿using System.Web.Mvc;

namespace Web.Helpers
{
    public static class MessageExtension
    {
        private const string MessageUITemplate = "<div class=\"alert alert-big alert-{0} alert-dismissable fade in\">" +
                      "<button type = \"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>" +
                      "<h4>{1}</h4>" +
                      "<p>{2}</p>" +
                    "</div>";
        public static MvcHtmlString RenderMessage(this HtmlHelper helper)
        {
            var tmsg = helper.ViewContext.Controller.TempData["Message"] as ViewBagMessage;
            var vmsg = helper.ViewBag.Message as ViewBagMessage;
            var msg = tmsg ?? vmsg;
            if (msg == null)
                return new MvcHtmlString("");
            string stype = "";
            switch (msg.MessageType)
            {
                case ViewBagMessage.ViewBagMessageType.Success:
                    stype += "success";
                    break;
                case ViewBagMessage.ViewBagMessageType.Error:
                    stype += "danger";
                    break;
                case ViewBagMessage.ViewBagMessageType.Warning:
                case ViewBagMessage.ViewBagMessageType.SuccessWithError:
                    stype += "warning";
                    break;
                case ViewBagMessage.ViewBagMessageType.Info:
                default:
                    stype += "info";
                    break;
            }

            return new MvcHtmlString(string.Format(MessageUITemplate, stype, msg.MessageTitle, msg.Message));
        }
    }
    public class ViewBagMessage
    {
        public enum ViewBagMessageType
        {
            Success,
            Error,
            SuccessWithError,
            Info,
            Warning
        }
        public string Message { get; set; }
        public string MessageTitle { get; set; }
        public ViewBagMessageType MessageType { get; set; }
    }
}