﻿CREATE TABLE [dbo].[ConsumerWishList] (
    [ConsumerWishListOfferId] INT IDENTITY (1, 1) NOT NULL,
    [ConsumerId]              INT NOT NULL,
    [PromotionId]             INT NOT NULL,
    CONSTRAINT [PK_ConsumerWishList] PRIMARY KEY CLUSTERED ([ConsumerWishListOfferId] ASC)
);

