﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.MerchantQRScan
{
    public class InvoiceDetailsDto
    {
        public string type { get; set; }
        public int id { get; set; }
        public int consumerId { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal TotalAmount { get; set; }

    }
}
