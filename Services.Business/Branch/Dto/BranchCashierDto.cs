﻿using Abp.Auditing;
using Abp.Authorization.Users;
using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Web.Business.Branch.Dto
{
    public class BranchCashierDto
    {
        public int MerchantBranchCashierId { get; set; }

        public int BranchId { get; set; }
        [Required]
        public string CashierName { get; set; }

        [Required]
        public string LoginId { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        public string Password { get; set; }
    }
}
