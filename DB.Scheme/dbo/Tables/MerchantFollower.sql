﻿CREATE TABLE [dbo].[MerchantFollower] (
    [MerchantFollowerId] INT IDENTITY (1, 1) NOT NULL,
    [MerchantId]         INT NOT NULL,
    [ConsumerId]         INT NOT NULL,
    CONSTRAINT [PK_MerchantFollower] PRIMARY KEY CLUSTERED ([MerchantFollowerId] ASC)
);

