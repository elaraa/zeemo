﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Offer
{
   public class VMReportOfferDto
    {
        public int OfferId { get; set; }

        public int ConsumerId { get; set; }

        public int ReportReasonId { get; set; }

        public string ReasonText { get; set; }
    }
}
