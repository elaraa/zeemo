﻿using DB.ORM.DB;
using Newtonsoft.Json;
using Services.Base;
using Services.Helper;
using Services.Mobile.Business.Dto;
using Services.Repository;
using Services.Security;
using Services.Web.Business.Base;
using System;
using System.Linq;
using static Services.Helper.EnumHelpers;


namespace Services.Mobile.Business.MerchantQRScan
{
    public class MerchantQRScanService : BaseService<PromotionRun>
    {
        public RequestResult scanQR(string code, string userToken)
        {
            try
            {
                MerchantCashierDto merchantCashierdata = JsonConvert.DeserializeObject<MerchantCashierDto>(getMerchantCashierTokenId(userToken));
                if (verifayMerchantCashier(merchantCashierdata))                {                    QRDto qr = JsonConvert.DeserializeObject<QRDto>(StringCipher.Decrypt (code, CipherCode.QRCodePassword));
                    try                    {
                        if (qr.OfferCode != "")
                        {
                            var offerId = int.Parse(qr.OfferCode);
                            var consumerId = int.Parse(qr.ConsumerCode);
                            var offer = GetOne<PromotionRun>(c => c.PromotionRunId == offerId);
                            Add(new ConsumerRedeemedPromotionRun
                            {
                                ConsumerId = consumerId,
                                CreationDate = DateTime.Now,
                                PromotionRunId = offerId
                            });

                            if (offer.NumberOfPieces.HasValue && offer.NumberOfPieces.Value > 0)
                                return new RequestResult
                                {
                                    Failed = false,
                                    Data = new {
                                        type = QRReturnType.Offer.DescriptionAttr(),
                                        id = offerId,
                                        consumerId = consumerId
                                    }
                                }; //offer with points
                            else
                                return new RequestResult { Failed = false }; //offer without points
                        }
                        else if (qr.ConsumerCode != "" && qr.OfferCode == "") // consumer
                        {
                            var consumerId = int.Parse(qr.ConsumerCode);
                            return new RequestResult
                            {
                                Failed = false,
                                Data = new {
                                    type = QRReturnType.Customer.DescriptionAttr(),
                                    id = consumerId,
                                    consumerId = consumerId
                                }
                            };
                        }
                        else
                            return new RequestResult { Failed = false }; // default case
                    }
                    catch (Exception e)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                        return new RequestResult
                        {
                            Failed = true, ErrorCode = ReturnErrorCode.DBError.DescriptionAttr(), Data = (int)ReturnErrorCode.DBError
                        };
                    }
                }
                return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.CipherError.DescriptionAttr() };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.CipherError.DescriptionAttr() };
            };
        }

        public RequestResult submitInvoiceAndRedeemPoints(InvoiceDetailsDto details, string userToken)
        {
            try            {
                MerchantCashierDto data = JsonConvert.DeserializeObject<MerchantCashierDto>(getMerchantCashierTokenId(userToken));
                int merchantid = int.Parse(data.MerchantID);

                if (verifayMerchantCashier(data))                {
                    var merchant = GetOne<Merchant>(a => a.MerchantId == merchantid);
                    // alaa calculated before ;
                    if (details.type == QRReturnType.Customer.DescriptionAttr())
                    {
                        if (details.TotalAmount > 0)
                        {
                            Add(new ConsumerLoyaltyPoint
                            {
                                ConsumerId = details.id,
                                InvoiceTotalAmount = details.TotalAmount,
                                InvoiceNumber = details.InvoiceNumber,
                                PromotionRunId = null,
                                ActivationDate = DateTime.Today.AddDays(merchant.PointsActivationDuration != null ? (double)merchant.PointsActivationDuration : 0),    // add from merchant table or no
                                ExpiryDate = DateTime.Today.AddDays(merchant.PointsExpirationDuration != null ? (double)merchant.PointsExpirationDuration : 0),
                                CreationDate = DateTime.Now,
                                MerchantId = merchant.MerchantId,
                                Points = pointsCalculations(merchant, details.TotalAmount), // points as calculated below
                                Remain = pointsCalculations(merchant, details.TotalAmount), // points as calculated below
                            });
                            return new RequestResult { Failed = false };
                        }
                        else if (details.TotalAmount < 0)
                        {
                            UnitOfWork unitOfWork = new UnitOfWork(db, new GenericRepository<ConsumerLoyaltyPoint>(db), new GenericRepository<ConsumerRedeemedPoint>(db));
                            try
                            {
                                unitOfWork.Begin();
                                var invoice = unitOfWork.GetEntity<ConsumerLoyaltyPoint>(c => c.InvoiceNumber == details.InvoiceNumber, false); // result null
                                if (invoice != null)
                                {
                                    invoice.Remain = pointsCalculations(merchant, details.TotalAmount);
                                    unitOfWork.AddOrUpdateEntity(invoice);
                                    int totalPoint = pointsCalculations(merchant, details.TotalAmount) * -1; 
                                    var merchantPoint = Get<DB.ORM.DB.MerchantPoint>(a => a.MerchantId == merchant.MerchantId && a.PointValue >= totalPoint).OrderBy(a=>a.PointValue).FirstOrDefault();
                                    unitOfWork.AddEntity(new ConsumerRedeemedPoint
                                    {
                                        ConsumerId = details.id,
                                        MerchantId = merchant.MerchantId, // from token done by alaa
                                        PointId = merchantPoint.PointId, // merchantpointId calculated points
                                        Points = pointsCalculations(merchant, details.TotalAmount)
                                    });
                                    unitOfWork.Complete();
                                    return new RequestResult { Failed = false };
                                }
                                else
                                    return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.InvoiceNotFound.DescriptionAttr() };
                            }                            catch (Exception e)                            {                                Elmah.ErrorSignal.FromCurrentContext().Raise(e);                                unitOfWork.Rollback();                                return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.DBError.DescriptionAttr() };                            };
                        }                        else                            return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.InvalidAmount.DescriptionAttr() };                    }
                    else if (details.type == QRReturnType.Offer.DescriptionAttr())
                        return addNewConsumerLoyaltyPoint(details, merchant);                 
                    else                        return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.InvalidType.DescriptionAttr() };
                }                else                    return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.CipherError.DescriptionAttr() };                
            }
            catch (Exception e)            {                Elmah.ErrorSignal.FromCurrentContext().Raise(e);                return new RequestResult { Failed = true, Data = "", ErrorCode = ReturnErrorCode.DBError.DescriptionAttr() };            }
        }


        private RequestResult addNewConsumerLoyaltyPoint(InvoiceDetailsDto details, Merchant merchant)
        {
            var offer = GetOne<PromotionRun>(c => c.PromotionRunId == details.id);
            int points = offer.NumberOfPoints.HasValue ? offer.NumberOfPoints.Value : 0;
            if (offer != null)
            {
                if (points != 0)
                {
                    if (details.consumerId > 0)
                    {
                        Add(new ConsumerLoyaltyPoint
                        {
                            ConsumerId = details.consumerId,
                            InvoiceTotalAmount = details.TotalAmount,
                            InvoiceNumber = details.InvoiceNumber,
                            PromotionRunId = details.id,
                            ActivationDate = DateTime.Today.AddDays(merchant.PointsActivationDuration != null ? (double)merchant.PointsActivationDuration : 0),    // add from merchant table or no
                            ExpiryDate = DateTime.Today.AddDays(merchant.PointsExpirationDuration != null ? (double)merchant.PointsExpirationDuration : 0),
                            CreationDate = DateTime.Now,
                            MerchantId = merchant.MerchantId,
                            Points = points,
                            Remain = points
                        });
                        return new RequestResult { Failed = false };
                    }
                    else
                        return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.ConsumerNotFound.DescriptionAttr() };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.NoPoints.DescriptionAttr() };
            }
            else
                return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.OfferNotFound.DescriptionAttr() };
        }

        private int pointsCalculations(Merchant merchant, decimal TotalAmount)
        {
            var pointsPerPound = merchant.PointsAmount / merchant.Pounds;
            var Calculation = TotalAmount * pointsPerPound;
            int result = (int)Calculation;
            return result;
        }

        public bool verifayMerchantCashier(MerchantCashierDto data)
        {
            int merchantid = int.Parse(data.MerchantID);
            int Cashoerid = int.Parse(data.CashierID);
            if (Any<Merchant>(a => a.MerchantId == merchantid) && Any<MerchantBranchCashier>(a => a.MerchantBranchCashierId == Cashoerid))
                return true;
            else
                return false;
        }
    }
}      