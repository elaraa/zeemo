namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FAQQuestion")]
    public partial class FAQQuestion
    {
        public int FaqQuestionId { get; set; }

        [Required]
        public string QuestionText { get; set; }

        public int FaqCategoryId { get; set; }

        public string AnswerText01 { get; set; }

        public string AnswerImage { get; set; }

        public string AnswerText02 { get; set; }
    }
}
