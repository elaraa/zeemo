﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Dto
{
   public class LocationDto
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AreaId { get; set; }

        [Required]
        public string AreaName { get; set; }


        public decimal? Lat { get; set; }

        public decimal? Lng { get; set; }
    }
}
