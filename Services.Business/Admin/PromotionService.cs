﻿using DB.ORM.DB;
using Services.Web.Business.Base;
using Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Services.Base;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using static Services.Helper.EnumHelpers;

namespace Services.Web.Business.Admin
{
    public class PromotionService : BaseService<Promotion>
    {
        public PromotionService()
        {}
        public bool approve(int? id)
        {
            try
            {
                if (!id.HasValue)
                    return false;

                var entity = GetOne<Promotion>(a => a.PromotionId == id.Value);
                if (entity != null)
                {
                    entity.ApprovedDate = DateTime.Now;
                    AddOrUpdate(entity);

                }
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }
        public bool reject(int? id)
        {
            try
            {
                if (!id.HasValue)
                    return false;

                var entity = GetOne<Promotion>(a => a.PromotionId == id.Value);
                if (entity != null)
                {
                    entity.RejectedDate = DateTime.Now;
                    AddOrUpdate(entity);

                }
                return true;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return false;
            }
        }
    }
}
