﻿using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Web.App.Admin
{
    public class FormHeaderSpecialTab
    {
        private const string _formTabsHeaderTemplate = "<ul class=\"nav nav-pills nav-justified steps\"> {0}" +
                                                       " </ul>";

        private const string _tabTemplate = "  <li id=\"li-{0}\" class=\"{1}\">" +
                                                    " <a href = \"#tabs-{0}\" data-toggle=\"tab\" class=\"step {1}\">" +
                                                      " <span class=\"number\"> {0} </span>" +
                                                       "  <span class=\"desc\">" +
                                                         "  <i class=\"fa fa-check\"></i> {2}" +
                                                       "  </span>" +
                                                   "  </a>" +
                                              "  </li>";
        public string Name { get; set; }
        public string Id { get; set; }
        public bool Active { get; set; } = false;
        //public string Icon { get; set; }
        public override string ToString()
        {
            return string.Format(_tabTemplate, Id, Active ? "active" : "", Name);
        }
        public static string GetTabs(params FormHeaderSpecialTab[] tabs)
        {
            if (tabs == null || tabs.Length < 1) return "";
            var tabsHtml = new StringBuilder("<ul class=\"nav nav-pills nav-justified steps\">");
            var activeTab = tabs.FirstOrDefault(w => w.Active == true);
            activeTab = activeTab ?? tabs[0];
            activeTab.Active = true;

            foreach (var item in tabs)
                tabsHtml.Append(item.ToString());
            tabsHtml.Append("</ul>");
            return tabsHtml.ToString();
        }
    }
}