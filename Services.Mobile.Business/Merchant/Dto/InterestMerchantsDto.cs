﻿

namespace Services.Mobile.Business.Dto
{
    public class InterestMerchantsDto
    {
        public int MerchantId { get; set; }
        public string MerchantName { get; set; }
        public string MerchantLogo { get; set; }
    }
}
