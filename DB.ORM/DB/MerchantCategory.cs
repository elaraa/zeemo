namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MerchantCategory")]
    public partial class MerchantCategory
    {
        public int MerchantCategoryId { get; set; }

        public int MerchantId { get; set; }

        public int CategoryId { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? EndDate { get; set; }

        public virtual Category Category { get; set; }

        public virtual Merchant Merchant { get; set; }
    }
}
