﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.App.Models
{
    public class VMCampaign
    {
        public int PromotionRunId { get; set; }
        public int PromotionId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public int RedeemCountPerPerson { get; set; }
        public int NumberOfPoints { get; set; }
        public int NumberOfPieces { get; set; }
        public IList<VMCampaignCriteria> CampaignCriteria { get; set; }
        public IList<VMCampaignBranch> Branches { get; set; }

        public VMCampaign() {
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
        }
    }
}