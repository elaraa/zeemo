﻿
function reloadControl(targetGrid, gridType) {

    var grid = $(targetGrid).data(gridType);
    grid.dataSource.read();
    grid.refresh();
}

function reloadGrid(targetGrid) {

    return reloadGrid(targetGrid, "kendoGrid");
}

function DeleteRecord(gridId, gridType, recordId, deleteUrl) {
    if (confirm('Are you sure you want to delete the selected record?')) {
        $.ajax({
            url: deleteUrl,
            data: { id: recordId },
            async: false,
            type: "POST",
            success: function (data) {
                if ($.trim(data)) {
                    alert("Record deleted Successfully");
                    reloadControl("#" + gridId, gridType);
                }
                else {
                    alert("Record is not deleted !!!");
                }
            },
            error: function (errorThrown) {
                alert("There is an error while deleting!");
            }
        })
    }
}

function DeleteGridRecord(gridId, recordId, deleteUrl) {
    return DeleteRecord(gridId,"kendoGrid", recordId, deleteUrl);
}

