namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_FAQ
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FaqQuestionId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string QuestionText { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int FaqCategoryId { get; set; }

        [Key]
        [Column(Order = 3)]
        public string CategoryName { get; set; }
    }
}
