﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DB.ORM.DB
{
    [MetadataType(typeof(MerchantMetadataDto))]
    public partial class Merchant 
    {
        class MerchantMetadataDto
        {
            public MerchantMetadataDto()
            {
                Membership = "Golden";
            }

            [Required(ErrorMessageResourceName = "RequiredMerchantName", ErrorMessageResourceType = typeof(ZM.Merchant))]
            public string MerchantName { get; set; }

            [Required(ErrorMessageResourceName = "RequiredMerchantBrandName", ErrorMessageResourceType = typeof(ZM.Merchant))]
            public string MerchantBrandName { get; set; }

            [DataType(DataType.Url)]
            public string Website { get; set; }

            [DataType(DataType.PhoneNumber)]
            public string Mobile { get; set; }
            
            [DataType(DataType.EmailAddress)]
            public string Email { get; set; }
            public string Membership { get; set; }

            [DataType(DataType.Password)]
            public string Password { get; set; }
        }

        [NotMapped]
        public class MerchantDto : Merchant
        {
            public int Id { get; set; }

            [DataType(DataType.Password)]
            [Compare("Password", ErrorMessageResourceName = "MissMatching", ErrorMessageResourceType = typeof(ZM.UI))]
            public string ConfirmPassword { get; set; }
            public string Logo { get; set; }

            public HttpPostedFileBase PictureFile { get; set; }

            public string Pic { get; set; }
            public string FullName { get; set; }
            public DateTime? BirthDate { get; set; }
        }
    }
}
