namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PromotionRunBranch")]
    public partial class PromotionRunBranch
    {
        public int PromotionRunBranchId { get; set; }

        public int PromotionRunId { get; set; }

        public int BranchId { get; set; }

        public virtual MerchantBranch MerchantBranch { get; set; }

        public virtual PromotionRun PromotionRun { get; set; }
    }
}
