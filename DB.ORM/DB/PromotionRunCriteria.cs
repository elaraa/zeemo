namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PromotionRunCriteria")]
    public partial class PromotionRunCriteria
    {
        public int PromotionRunCriteriaId { get; set; }

        public int PromotionRunId { get; set; }

        public int? AgeFrom { get; set; }

        public int? AgeTo { get; set; }

        [StringLength(1)]
        public string Gender { get; set; }

        public int? PhoneType { get; set; }

        public virtual PromotionRun PromotionRun { get; set; }
    }
}
