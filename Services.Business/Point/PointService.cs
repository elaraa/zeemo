﻿using DB.ORM.DB;
using Services.Base;

namespace Services.Web.Business.PointSettings
{
    public class PointService : BaseService<Merchant>
    {
        public bool GetLoyality(int userid)
        {
            var loyality = GetOne(a => a.MerchantId == userid).HasLoyalty; 
            return loyality;
        }
    }
}
   