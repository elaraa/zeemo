namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Area")]
    public partial class Area
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Area()
        {
            MerchantBranches = new HashSet<MerchantBranch>();
        }

        public int AreaId { get; set; }

        [Required]
        public string AreaName { get; set; }

        public string AreaNameAr { get; set; }

        public decimal? Lat { get; set; }

        public decimal? Lng { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MerchantBranch> MerchantBranches { get; set; }
    }
}
