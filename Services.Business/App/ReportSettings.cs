﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Web.Business
{
    public class ReportSettings //: BaseService<Setting>
    {
        public string ReportServer { get; set; }
        public string ReportFolder { get; set; }
        public string ReportLogoUrl { get; set; }

        //    public ReportSettings(int userId):base(userId)
        //    {
        //        var rs = db.Settings.Where(w=>w.SettingId == 1 || w.SettingId == 2 || w.SettingId == 3).ToList();
        //        if (rs != null)
        //        {
        //            var rserver = rs.Find(w => w.SettingId == 1);
        //            ReportServer = rserver?.SettingValue;
        //            var rfolder = rs.Find(w => w.SettingId == 2);
        //            ReportFolder = rfolder?.SettingValue;
        //            var rlogo = rs.Find(w => w.SettingId == 3);
        //            ReportLogoUrl = rlogo?.SettingValue;
        //        }
        //    }
    }
}
