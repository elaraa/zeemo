﻿

namespace Services.Mobile.Business.Dto
{
    public class MerchantBranchesDto
    {
        public string Label { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
