﻿using Services.Mobile.Business;
using Services.Mobile.Business.Dto;
using System.Web.Http;

namespace Mobile.APIsConsumer
{
    public class AccountController : ApiController
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        AccountService accountService = new AccountService();
        [HttpPost]
        public RequestResult Register([FromBody]ConsumerRegistration registration)
        {
            if (ModelState.IsValid)
                return accountService.register(registration);
            return new RequestResult { Failed = true, ErrorCode = "DataInComplete" };
        }
        public class OTPRequestResult : RequestResult
        {
            public string token { get; set; }
        }
        [HttpPost]
        public RequestResult OTP_Verify([FromBody]ConsumerContract<string> otp)
        {
            if (ModelState.IsValid)
                return accountService.verifyingOTP(otp);
            return new RequestResult { Failed = true, ErrorCode = "DataInComplete" };
        }
        [HttpPost]
        public RequestResult Login([FromBody]ConsumerContract<string> phone)
        {
            if (ModelState.IsValid)
            {
                var returnData = accountService.login(phone);
                if (returnData != null) {
                    using (var wb = new System.Net.WebClient())
                    {
                        var data = new System.Collections.Specialized.NameValueCollection();
                        data["UserName"] = "Mazano";
                        data["Password"] = "t6Fv1A";
                        data["MessageType"] = "text";
                        data["Recipients"] = phone.wcObject;
                        data["SenderName"] = "Cequens";
                        data["MessageText"] = string.Format("your OTP number is {0}", (string)returnData.Data);

                        var response = wb.UploadValues("https://ht.cequens.com/Send.aspx", "POST", data);
                        string responseInString = System.Text.Encoding.UTF8.GetString(response);
                        return new RequestResult { Failed = false, ErrorCode = "", Data = (string)returnData.Data };
                    }
                }
            }
            return new RequestResult { Failed = true, ErrorCode = "DataInComplete" };
        }

        [HttpPost]
        public RequestResult Logout()
        {
            var userToken = Request.Headers.Authorization.Parameter;
            return accountService.logout(userToken);
        }
        [HttpPost]
        public IHttpActionResult UpdateFCToken([FromBody]ConsumerContract<string> fctoken)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(accountService.updateFCToken(fctoken, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = "DataInComplete" });
        }
        [HttpPost] //alaa
        public RequestResult UpdateNotifcation([FromBody]ConsumerStructContract<bool> enableNotification)
        {
            var userToken = Request.Headers.Authorization.Parameter;

            if (ModelState.IsValid)
                return accountService.updateNotifcation(enableNotification, userToken);

            return new RequestResult { Failed = true, ErrorCode = "DBError" };
        }

        [HttpPost]
        public RequestResult UpdateLanguage([FromBody]ConsumerContract<string> Language)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return accountService.updateLanguage(Language, userToken);

            return new RequestResult { Failed = true, ErrorCode = "DBError" };
        }
        [HttpPost]
        public RequestResult GetMyReferral(ReferralDto referralDto)
        {
            var userToken = Request.Headers.Authorization.Parameter;            if (ModelState.IsValid)                return accountService.getMyReferral(referralDto, userToken);
            return new RequestResult { Failed = true, ErrorCode = "DBError" };
        }

        [HttpPost]
        public IHttpActionResult GetRedeemedOffers([FromBody] ListParams @params)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(accountService.getRedeemedOffers(@params, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = "DataInComplete" });
        }

        [HttpPost] 
        public IHttpActionResult GetWishList([FromBody] ListParams @params)
        {
            
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(accountService.getWishList(@params, userToken));
            return Json(new RequestResult { Failed = true, ErrorCode = "DataInComplete" });
        }
        [HttpPost]
        public IHttpActionResult UpdatePicture(ConsumerContract<string> photo)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(accountService.updatePicture(photo ,userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = "DataInComplete" });
        }
    }
}
