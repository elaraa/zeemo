﻿using Services.Mobile.Business;
using System.Web.Http;
using Services.Mobile.Business.Interests;
using static Services.Helper.EnumHelpers;

namespace Mobile.APIsConsumer
{
    public class InterestController : ApiController
    {
        InterestService interestService = new InterestService();

        [HttpPost]
        public IHttpActionResult GetAll()
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(interestService.allInterst(userToken));
            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }
        [HttpPost]
        public IHttpActionResult GetMy()
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(interestService.allInterestIds(userToken));
            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }
        [HttpPost]
        public IHttpActionResult GetMyForHome()
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(interestService.getInterstBasedOnConsumerInterst(userToken));
            return Json(new RequestResult { Failed = true,ErrorCode= ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }
        [HttpPost]
        public IHttpActionResult SaveMyInterest(int[] interests)
        {
            //using (var db = new DBModel())
            //{
            //    var dlist = db.ConsumerInterests.Where(w => w.ConsumerId == 4);
            //    db.ConsumerInterests.RemoveRange(dlist);

            //    foreach (var item in interests)
            //    {
            //        db.ConsumerInterests.Add(new ConsumerInterest { InterestId = item, ConsumerId = 4 });
            //    }
            //    return Json(new RequestResult{
            //        Failed = db.SaveChanges() < 1
            //    });
            //}
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)     
                return Json(interestService.saveInterest(interests, userToken));
            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }
    }
}
