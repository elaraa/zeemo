﻿using AutoMapper;
using DB.ORM.DB;
using Services.Base;
using Services.Web.Business;
using Services.Web.Business.Admin;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;
using Web.App.Admin.Models;
using Web.App.Models;
using static DB.ORM.DB.SystemUser;

namespace Web.App.Admin.Controllers
{
    public class UserController : BaseControllerTemplateDto<vw_User, SystemUser, UserDto, int>
    {
        UserService userService = new UserService();
        public UserController()
        {
            ViewNamePlural = ZM.ViewPages.UserViewNamePlural;
            ViewNameSingular = ZM.ViewPages.UserViewNameSingular;
        }
        protected override Expression<Func<SystemUser, bool>> GetOneExpression(int? id) => id.HasValue ? (x => x.UserId == id.Value) : (Expression<Func<SystemUser, bool>>)(x => 1 == 0);
        protected override void InitTemplateServices()
        {
            ListService = new BaseService<vw_User>(UserId);
            ActionService = new UserService();
        }
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.Roles = LookupHelper.GetMyRoles(this);
            base.FillViewBag(isCreate);
        }

        //public ActionResult Edit(int? id) {
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    FillViewBag(false);
        //    var user = Mapper.Map<SystemUser>(new BaseService<SystemUser>().GetOne(c => c.UserId == id)); 
        //    if (user == null)
        //        return HttpNotFound();

        //    return View(user);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(UserDto model)
        //{
        //    return AddOrUpdate(model);
        //}
        //public override ActionResult Create()
        //{
        //    FillViewBag(true);
        //    return View(new UserDto { });
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(UserDto model)
        //{
        //    return AddOrUpdate(model);
        //}

        //private ActionResult AddOrUpdate(UserDto model) {
        //    bool isCreate = model.Id > 0 ? true : false;
        //    string modelErrors = "";

        //    if (ModelState.IsValid)
        //    {
        //        var result = userService.addOrUpdate(model, isCreate);
        //        if (result.Succeeded)
        //        {
        //            ShowSuccess(result.SuccessDesc, true);
        //            return RedirectToAction("Index");
        //        }

        //        ShowError(result.ErrorDesc);
        //    }
        //    modelErrors += GetModelStateErrors(ModelState);
        //    if (!string.IsNullOrEmpty(modelErrors))
        //        ShowError(modelErrors);
        //    FillViewBag(isCreate);
        //    return View(model);
        //}
    }
}