namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Promotion
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MerchantId { get; set; }

        [StringLength(500)]
        public string ImageUrl { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(8)]
        public string Status { get; set; }

        [Key]
        [Column(Order = 3)]
        public string PromotionName { get; set; }

        public string PromotionNameAr { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionTypeId { get; set; }

        [Key]
        [Column(Order = 5)]
        public string PromotionTypeName { get; set; }
    }
}
