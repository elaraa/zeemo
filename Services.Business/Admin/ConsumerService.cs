﻿using DB.ORM.DB; 
using System;
using Services.Base;
using System.Web;
using System.Linq;
using static Services.Helper.EnumHelpers;
using Web.Helpers.File;
using static DB.ORM.DB.SystemUser;
using Services.Base.Base;
using AutoMapper;
using static DB.ORM.DB.Consumer;

namespace Services.Web.Business.Admin
{
    public class ConsumerService : BaseService<ConsumerDto>
    {
        public ConsumerService(){}

        public override RunResult AddOrUpdate(ConsumerDto consumer, bool isCreate)
        {
            try
            {
                if (Any<Consumer>(c => c.ConsumerId != consumer.Id && c.Phone == consumer.Phone))
                    return new RunResult { ErrorDesc= ZM.Message.ExistPhone };


                var consumerDb = AddOrUpdate(Mapper.Map<Consumer>(consumer));
                if (consumerDb != null)
                {
                    var baseUploadUrl = "uploads/" + ZM.Uploads.Consumer + '/' + consumerDb.ConsumerId.ToString() + '/';
                    if (consumer.PictureFile != null && consumer.PictureFile.ContentLength > 0)
                    {
                        var isUploaded = FileHelper.UploadFiles(ZM.Uploads.Consumer, consumerDb.ConsumerId.ToString()
                                                                             , GetBaseUrl, GetUploadUrl, consumer.PictureFile);
                        if (isUploaded)
                        {
                            FileHelper.DeleteFiles(consumerDb.Photo, GetBaseUrl, GetDeleteUrl);
                            consumerDb.Photo = baseUploadUrl + consumer.PictureFile.FileName;
                            AddOrUpdate(consumerDb);
                        }
                    }
                    return MessageService.ShowSuccessMessage(isCreate);
                }

            }
            catch (Exception e) {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            return new RunResult { ErrorDesc = ZM.Message.ErrorCode };
        }

       
    }
}
