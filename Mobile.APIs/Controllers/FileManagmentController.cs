﻿using Services.Mobile.Business.FileManagment;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;


namespace Mobile.APIs.Controllers
{
    public class FileManagmentController : ApiController
    {
        FileManagmentService service = new FileManagmentService();

        [HttpPost]
        [System.Web.Http.Route("api/FileManagment/UploadFile")]
        public async Task<string> UploadFile()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            var root = HttpContext.Current.Server.MapPath("~/Uploads");
            var provider = new MultipartFormDataStreamProvider(root);
            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);
                service.uploadFile(provider, root);
            }
            catch (Exception e)
            {
               //Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return $"Error: {e.Message}";
            }

            return "";
        }

        [HttpPost]
        [System.Web.Http.Route("api/FileManagment/DeleteFile")]
        public async Task<string> DeleteFile() 
        {
            try
            {
                var res = await Request.Content.ReadAsStringAsync();
                service.deleteFile(res);
            }
            catch (Exception e)
            {
               // Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return $"Error: {e.Message}";
            }

            return "";
        }
    }
}

