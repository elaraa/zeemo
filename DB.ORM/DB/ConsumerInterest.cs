namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsumerInterest")]
    public partial class ConsumerInterest
    {
        public int ConsumerInterestId { get; set; }

        public int ConsumerId { get; set; }

        public int InterestId { get; set; }
    }
}
