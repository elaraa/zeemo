﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.Dto
{
   public class MerchantDto
    {
        public string Interest { get; set; }
        public string MerchantName { get; set; }
        public int MerchantId { get; set; }
        public string ImageUrl { get; set; }
        public bool IsFollowed { get; set; }
        public DateTime? FirstPointsToExpire { get; set; }
        public int Points { get; set; }
        public decimal? Savings { get; set; }
    }
}
