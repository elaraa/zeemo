namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PromotionImage")]
    public partial class PromotionImage
    {
        public int PromotionImageId { get; set; }

        public int PromotionId { get; set; }

        [Required]
        [StringLength(500)]
        public string ImageUrl { get; set; }

        public bool IsLogo { get; set; }

        public int SortOrder { get; set; }

        public virtual Promotion Promotion { get; set; }
    }
}
