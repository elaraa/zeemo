﻿using AutoMapper;
using DB.ORM.DB;
using Services.Base;
using Services.Base.Base;
using Services.Web.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Web.App.Admin.Models;
using Web.App.Models;
using Web.Helpers.File;
using static DB.ORM.DB.Merchant;
using Profile = DB.ORM.DB.Profile;

namespace Web.App.Admin.Controllers
{
    public class MyController : BaseController
    {
        MyProfileAdminService service;
        public MyController() : base()
        {
            ApplySecurity = false;

        }
        // GET: My Profile
        public override ActionResult Index()
        {
            return RedirectToAction("Profile");
        }
        public ActionResult Profile()
        {
            service = new MyProfileAdminService(UserId);
            return View(Mapper.Map<Profile>(service.GetOne(c => c.UserId == UserId)));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile(Profile model)
        {
            var err = "";
            service = new MyProfileAdminService(UserId);

            if (ModelState.IsValid)
            {
                var PropertiesToUpdate = new Expression<Func<SystemUser, object>>[]
                     {x=>x.Facebook, x=>x.Website, x=>x.Address,x=>x.Email,x=>x.Mobile,x=>x.UserName };
                var res = service.Update(Mapper.Map<SystemUser>(model), PropertiesToUpdate);
                if (res)
                    ShowSuccess(ZM.Message.SuccessEditCode);
                else
                    ShowError(ZM.Message.ErrorCode);
            }
            err += GetModelStateErrors(ModelState);
            if (!string.IsNullOrEmpty(err))
                ShowError(err);

            return View(Mapper.Map<Profile>(new BaseService<SystemUser>().GetOne(c => c.UserId == UserId)));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeImage(HttpPostedFileBase PictureFile)
        {
            service = new MyProfileAdminService(UserId);
            RunResult result = service.SavePicture(PictureFile);
            if (result.Succeeded)
                ShowSuccess(result.SuccessDesc, true);
            else
                ShowError(result.ErrorDesc, true);

            return RedirectToAction("Profile");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePass(VMChangePassword model)
        {
            if (!new MyProfileAdminService(UserId).SavePassword(model.CurrentPassword, model.NewPassword))
                ShowError(ZM.Message.ErrorwhileSavingPass, true);
            else
                ShowSuccess(ZM.Message.SuccessEditCode, true);
            return RedirectToAction("Profile");
        }
    }
} 