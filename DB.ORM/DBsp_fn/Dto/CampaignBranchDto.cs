﻿
namespace DB.ORM.Dto
{
   public class CampaignBranchDto
    {
        public int BranchId { get; set; }
        public int MerchantId { get; set; }

        public string Address { get; set; }
        public string AreaName { get; set; }
        public int Selected { get; set; }
    }
}
