﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Offer
{
   public class ConsumerRateOfferDto
    {
        public int Id { get; set; }
        public int Rate { get; set; }
        public int RateTypeId { get; set; }
        public string RateComment { get; set; }
    }
}
