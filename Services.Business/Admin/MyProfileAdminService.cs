﻿using DB.ORM.DB;
using Services.Base;
using Services.Base.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Web.Helpers.File;

namespace Services.Web.Business
{
    public class MyProfileAdminService : BaseService<SystemUser>
    {
        private new int UserId { get; set; }
        public MyProfileAdminService(int userid)
        {
            UserId = userid; 
        }
        public bool SavePassword(string currentPassword, string newPassword)
        {
            if (currentPassword.IsEmpty() || newPassword.IsEmpty()) return false;

            using (var db = new DBModel())
            {
                db.UserId = UserId;
                var user = db.SystemUsers.FirstOrDefault(w => w.UserId == UserId);

                if (user.Password == LoginService.HashString(currentPassword))
                    user.Password = LoginService.HashString(newPassword);
                else
                    return false;
                return db.SaveChanges() > 0;
            }
        }

        public RunResult SavePicture(HttpPostedFileBase PictureFile)
        {
            try
            {
                if (PictureFile != null && PictureFile.ContentLength >= 1)
                {
                    var logo = "Uploads" + '/' + ZM.Uploads.User + '/' + UserId + '/' + PictureFile.FileName;
                    if (PictureFile != null && PictureFile.ContentLength > 0)
                    {
                        var oldPath = GetOne(a => a.UserId == UserId).UserLogo;
                        var isDeleted = FileHelper.DeleteFiles(oldPath, GetBaseUrl, GetDeleteUrl);
                        if (isDeleted)
                        {
                            var isUploaded = FileHelper.UploadFiles(ZM.Uploads.User, UserId.ToString(),
                                                            GetBaseUrl, GetUploadUrl, PictureFile);
                            if (isUploaded)
                            {
                                var user = GetOne(c => c.UserId == UserId);
                                if (user != null)
                                {
                                    user.UserLogo = logo;
                                    AddOrUpdate(user);
                                }
                                return new RunResult { Succeeded = true, SuccessDesc = ZM.Message.SuccessEditCode };
                            }
                        }
                        

                        return new RunResult { ErrorDesc = ZM.Message.Errorwhilesaving };
                    }
                }
                return new RunResult { ErrorDesc = ZM.Message.ErrorSelectImage};

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            return new RunResult { ErrorDesc = ZM.Message.ErrorCode };
        }
    }
}
