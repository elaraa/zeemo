namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Interests
    {
        [Key]
        [Column(Order = 0)]
        public int InterestId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string InterestName { get; set; }

        public string InterestNameAr { get; set; }

        public string Logo { get; set; }
    }
}
