﻿var data = [];
async function fillData(content) {
    for (var i = 0; i < data.length; i++)
        if (data[i].fileContent == undefined) {
            data[i].fileContent = content;
            break;
        }
    return;
}
async function onSelect(e) {
    await readFiles(e);
    renderTemplate();
}
const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms))
async function readFiles(e) {
    for (var i = 0; i < e.files.length; i++) {
        var file = e.files[i].rawFile;
        data.push({ fileName: file.name, Selected: i == 0 && data.length == 0 ? true : false });
        if (file) {
            var reader = new FileReader();
            reader.onloadend = async function () {
                await fillData(this.result, i);
            };
            reader.readAsDataURL(file);
        }
    }
    await sleep(1000);
    return;
}

$(document).on("click", "#rightBtn", function () {
    var imageId = $("div.k-state-selected").prop("id");
    if (imageId < data.length - 1) {
        var tmpData = data[parseInt(imageId) + 1];
        data[parseInt(imageId) + 1] = data[imageId];
        data[imageId] = tmpData;
        renderTemplate();
    }
});

$(document).on("click", "#leftBtn", function () {
    var imageId = $("div.k-state-selected").prop("id");
    if (imageId > 0) {
        var tmpData = data[imageId - 1];
        data[imageId - 1] = data[imageId];
        data[imageId] = tmpData;
        renderTemplate();
    }
});
$(document).on("click", ".selectedImg", function () {
    for (var i = 0; i < data.length; i++)
        data[i].Selected = false;
    data[$(this).closest('div').attr('id')].Selected = true;
    renderTemplate();
});

$(document).on("click", ".removeimage", function () {
    data.splice($(this).closest('div').attr('id'), 1);
    renderTemplate();
});
function renderTemplate() {
    var template = kendo.template($("#template").html());
    var result = template(data);
    $("#images").html(result);
}


