﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Services.Mobile.Business
{
    public class OTPRequestResult : RequestResult
    {
        public string token { get; set; }
    }
}