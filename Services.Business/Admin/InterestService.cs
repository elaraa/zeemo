﻿using DB.ORM.DB; 
using System;
using Services.Base;
using System.Web;
using System.Linq;
using static Services.Helper.EnumHelpers;
using Web.Helpers.File;
using static DB.ORM.DB.SystemUser;
using Services.Base.Base;
using AutoMapper;
using static DB.ORM.DB.Interest;

namespace Services.Web.Business.Admin
{
    public class InterestService : BaseService<InterestDto>
    {
        public InterestService(){}

        public override RunResult AddOrUpdate(InterestDto interest, bool isCreate)
        {
            try
            {
                var interestDb = AddOrUpdate(Mapper.Map<Interest>(interest));
                if (interestDb != null)
                {
                    var baseUploadUrl = "uploads/" + ZM.Uploads.Interest + '/' + interestDb.InterestId.ToString() + '/';
                    if (interest.PictureFile != null && interest.PictureFile.ContentLength > 0)
                    {
                        var isUploaded = FileHelper.UploadFiles(ZM.Uploads.Interest, interestDb.InterestId.ToString(), GetBaseUrl, GetUploadUrl, interest.PictureFile);
                        if (isUploaded)
                        {
                            FileHelper.DeleteFiles(interestDb.Icon, GetBaseUrl, GetDeleteUrl);
                            interestDb.Icon = baseUploadUrl + interest.PictureFile.FileName;
                            AddOrUpdate(interestDb);
                        }
                    }
                    return MessageService.ShowSuccessMessage(isCreate);
                }

            }
            catch (Exception e) {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            return new RunResult { ErrorDesc = ZM.Message.ErrorCode };
        }
    }
}
