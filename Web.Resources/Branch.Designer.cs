﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZM {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Branch {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Branch() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Web.Resources.Branch", typeof(Branch).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address.
        /// </summary>
        public static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Arabic Address.
        /// </summary>
        public static string AddressAr {
            get {
                return ResourceManager.GetString("AddressAr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Area.
        /// </summary>
        public static string Area {
            get {
                return ResourceManager.GetString("Area", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Area Name.
        /// </summary>
        public static string AreaName {
            get {
                return ResourceManager.GetString("AreaName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Branch Info.
        /// </summary>
        public static string BranchInfo {
            get {
                return ResourceManager.GetString("BranchInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cahiers.
        /// </summary>
        public static string Cahiers {
            get {
                return ResourceManager.GetString("Cahiers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cashier Name.
        /// </summary>
        public static string CashierName {
            get {
                return ResourceManager.GetString("CashierName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cashiers.
        /// </summary>
        public static string Cashiers {
            get {
                return ResourceManager.GetString("Cashiers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to #Cashiers.
        /// </summary>
        public static string CashiersNumber {
            get {
                return ResourceManager.GetString("CashiersNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The Selected Location Outside Selected Area.
        /// </summary>
        public static string InvalidLocation {
            get {
                return ResourceManager.GetString("InvalidLocation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Language.
        /// </summary>
        public static string Language {
            get {
                return ResourceManager.GetString("Language", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Latitude.
        /// </summary>
        public static string Latitude {
            get {
                return ResourceManager.GetString("Latitude", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Name.
        /// </summary>
        public static string LoginId {
            get {
                return ResourceManager.GetString("LoginId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Longitude.
        /// </summary>
        public static string Longitude {
            get {
                return ResourceManager.GetString("Longitude", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address is Required.
        /// </summary>
        public static string RequiredAddress {
            get {
                return ResourceManager.GetString("RequiredAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Branches.
        /// </summary>
        public static string ViewNamePlural {
            get {
                return ResourceManager.GetString("ViewNamePlural", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Branch.
        /// </summary>
        public static string ViewNameSingular {
            get {
                return ResourceManager.GetString("ViewNameSingular", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to warning.
        /// </summary>
        public static string warning {
            get {
                return ResourceManager.GetString("warning", resourceCulture);
            }
        }
    }
}
