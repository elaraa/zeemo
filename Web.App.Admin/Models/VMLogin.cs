﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.App.Admin.Models
{
    public class VMLogin
    {
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string RememberMe { get; set; }
        public string RedirectUrl { get; set; }
    }
}