﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Offer
{
   public class OfferTypeDto
    {
        public int OfferTypeId { get; set; }
        public string OfferTypeName { get; set; }
    }
}
