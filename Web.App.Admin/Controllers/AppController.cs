﻿using Services.Base;
using Services.Web.Business;
using System.Web.Mvc;
using Web.App.Admin.Models;
using Web.App.Models;
using Web.Helpers;

namespace Web.App.Admin.Controllers
{
    public class AppController : Controller
    {
        // GET: App
        public ActionResult Index(string redirectUrl)
        {
            return View(new VMLogin { RedirectUrl = redirectUrl });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(VMLogin model)
        {
            if (ModelState.IsValid)
            {
                var res = LoginService.AuthenticateAdmin(model.LoginName, model.Password);
                if (res.Result == LoginService.AuthenticationResult.UserFound)
                {
                        Session["SystemUserId"] = res.UserId;
                        Session["UserFullName"] = res.FullName;
                        Session["UserFirstName"] = res.FirstName;
                        Session["UserLastName"] = res.LastName;
                        Session["pic"] = res.Picture;

                        if (!string.IsNullOrEmpty(model.RedirectUrl))
                            return Redirect(model.RedirectUrl);
                        return RedirectToAction("Index", "Home");
                }
            }
            ViewBag.Message = new ViewBagMessage
            {
                MessageType = ViewBagMessage.ViewBagMessageType.Error,
                MessageTitle = "Error",
                Message = "Invalid username or password"
            };
            return View(model);
        }
        public ActionResult UnAuthorized() { return View(); }
        public ActionResult Logout() {
            Session.Clear();
            Session.Abandon();
            return RedirectToActionPermanent("Index");
        }
        public ActionResult NotFound(HandleErrorInfo exception)
        {
            return View(exception);
        }
        public ActionResult GenericError(HandleErrorInfo exception) {
            return RedirectToAction("NotFound",new { exception });
        }
    }
}