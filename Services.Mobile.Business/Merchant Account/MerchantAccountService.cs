﻿using AutoMapper;
using DB.ORM.DB;
using Newtonsoft.Json;
using Services.Base;
using Services.Mobile.Business.Dto;
using Services.Mobile.Business.Merchant_Account.Dto;
using Services.Security;
using Services.Web.Business;
using System;
using System.Security.Cryptography;
using System.Text;
using static Services.Helper.EnumHelpers;

namespace Services.Mobile.Business
{
    public class MerchantAccountService : BaseService<MerchantBranchCashier>
    {
        public static string HashString(string str)
        {
            string hashedString = string.Empty;

            using (var sha512 =SHA512.Create())
            {
               
                var hashedBytes = sha512.ComputeHash(Encoding.UTF8.GetBytes(str));
                
               hashedString = BitConverter.ToString(hashedBytes).Replace("-", "");
            }
            return hashedString;
        }

        public RequestResult login (LoginCredentialsDto login)
        {
            try
            {
                var cashier = GetUser(login.UserName, login.Password);
                if (cashier != null) 
                {
                    var merchant = GetMerchant(GetMerchantId(cashier.BranchId));
                    MerchantCashierDto token = new MerchantCashierDto
                    {
                        CashierID=cashier.MerchantBranchCashierId.ToString(),
                        MerchantID=merchant.MerchantId.ToString()
                    };
                    string Token = "";
                    var code = JsonConvert.SerializeObject(token);
                    Token = StringCipher.Encrypt(code, CipherCode.MerchantTokenPassword);
                    return new OTPRequestResult { Failed = false, Data = merchant ,token= Token };
                }
                return new RequestResult { Failed = true, ErrorCode = "InvalidLogin" };
            }
            catch (Exception e)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, ErrorCode = "DBError" };

            }
        }


        //public RequestResult merchantIDVerify(MerchantCashierDto token)
        //{
        //    try 
        //    {
        //        int merchantId = int.Parse(token.MerchantID);
        //        var merchant = GetOne<Merchant>(a=>a.MerchantId== merchantId);
        //        if (merchant == null)
        //            return new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DBError.DescriptionAttr() };
        //        else
        //        {
        //            string Token = "";
        //            var code = JsonConvert.SerializeObject(token);
        //            Token = StringCipher.Encrypt(code, CipherCode.MerchantTokenPassword);
              

        //            return new OTPRequestResult { Failed = false, ErrorCode = "", Data = "", token = Token };
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(e);
        //        return new RequestResult { Failed = true, ErrorCode = "DBError" };
        //    }
        //}

        public MerchantBranchCashier GetUser(string name, string pass)
        {
            var encryptedPassword = LoginService.Encrypt(pass);
            var cashier = GetOne(c => c.LoginId.ToLower().Trim() == name.ToLower().Trim() && c.Password == encryptedPassword);
            return cashier;
        }

        public int GetMerchantId(int id)
        {
            var merchantId = GetSingle<MerchantBranch>(c => c.BranchId == id).MerchantId;
            return merchantId;
        }


        public MerchantAccountDto GetMerchant(int id)
        {
            var merchant = GetSingle<Merchant>(c => c.MerchantId == id);
            return Mapper.Map<MerchantAccountDto>(merchant);
        }


        public RequestResult logout(string userToken)
        {
            try
            {
               if(verifayMerchantCashier(userToken))
                    return new RequestResult { Failed = false };
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = userToken };
            }
        }


        public RequestResult updateLanguage(ConsumerContract<string> Language, string userToken)
        {
            try
            {
                if (verifayMerchantCashier(userToken))
                {
                    MerchantCashierDto data = JsonConvert.DeserializeObject<MerchantCashierDto>(getMerchantCashierTokenId(userToken));
                    int Cashoerid = int.Parse(data.CashierID);
                    var BranchCashire = GetOne(a => a.MerchantBranchCashierId== Cashoerid);
                    BranchCashire.Language = Language.wcObject == "ar" ? "ar" : "en";
                    AddOrUpdate(BranchCashire);
                    return new RequestResult { Failed = false};
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = "" };
            }
        }

        public bool verifayMerchantCashier(string userToken)
        {
            MerchantCashierDto data = JsonConvert.DeserializeObject<MerchantCashierDto>(getMerchantCashierTokenId(userToken));
            int merchantid = int.Parse(data.MerchantID);
            int Cashoerid = int.Parse(data.CashierID);
            if (Any<Merchant>(a => a.MerchantId == merchantid) && Any<MerchantBranchCashier>(a=>a.MerchantBranchCashierId==Cashoerid))
                return true;
            else
                return false;
        }

       
    }
}
