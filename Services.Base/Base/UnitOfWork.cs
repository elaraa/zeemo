﻿using DB.ORM.DB;
using Services.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services.Web.Business.Base
{
    public class UnitOfWork
    {
        DbContextTransaction _scope;
        protected readonly DBModel _db;
        List<GenericRepo> repos;
        public UnitOfWork(DBModel db, params GenericRepo[] rs) {
            _db = db;
            repos = new List<GenericRepo>();
            repos.AddRange(rs);

        }

        public void Begin() => _scope = _db.Database.BeginTransaction();
        public T AddOrUpdateEntity<T>(T entity) 
            where T:class {
            try
            {
                var repo = GetRepo<T>();
                if (repo != null)
                    return repo.AddOrUpdate(entity);
            }
            catch (Exception e) { }
            return null;
        }
        public T AddEntity<T>(T entity)
        where T: class
        {
            try
            {
                var repo = GetRepo<T>();
                if (repo != null)
                    return repo.Add(entity);
            }
            catch (Exception e) { }
            return null;
        }
        public T GetEntity<T>(Expression<Func<T, bool>> whereClause, bool local = true, params string[] childrenToInclude)
            where T : class
        {
           try
            {
                var repo = GetRepo<T>();
                if (repo != null)
                    return local ? repo.GetSingleLocalDS<T>() : repo.GetSingle<T>(whereClause, false, childrenToInclude);
            }
            catch (Exception e) { }
            return null;
        }

        public T FindEntity<T>(Expression<Func<T, bool>> whereClause, params string[] childrenToInclude)
            where T : class
        {
            try
            {
                var repo = GetRepo<T>();
                if (repo != null)
                    return repo.GetSingleDS(whereClause);
            }
            catch (Exception e) { }
            return null;
        }
        public IQueryable<T> GetEntities<T>()
            where T : class
        {
            try
            {
                var repo = GetRepo<T>();
                if (repo != null)
                    return repo.GetLocalDS<T>();
            }
            catch (Exception e) { }
            return null;
        }
        public bool EntityHasValue<T>()
           where T : class
        {
            try
            {
                var repo = GetRepo<T>();
                if (repo != null)
                    return repo.HasLocalDS<T>();
            }
            catch (Exception e) { }
            return false;
        }
        public int RemoveEntity<T>(Expression<Func<T, bool>> expression)
          where T : class
        {
            try
            {
                var repo = GetRepo<T>();
                if (repo != null)
                    return repo.Delete<T>(expression);
            }
            catch (Exception e) { }
            return 0;
        }
        public int RemoveOneEntity<T>(Expression<Func<T, bool>> expression)
          where T : class
        {
            try
            {
                var repo = GetRepo<T>();
                if (repo != null)
                    return repo.DeleteOne<T>(expression);
            }
            catch (Exception e) { }
            return 0;
        }
        public bool Complete() {
            try
            {
                _scope.Commit();
                return true;
            }
            catch (Exception e) { }
            return false;
        }
        public bool Rollback() {
            try
            {
                _scope.Rollback();
                return true;
            }
            catch (Exception e) {}
            return false;
        }
        public GenericRepo GetRepo<T>() where T: class {
            return repos.FirstOrDefault(a => a.GetType() == typeof(T));
        }
    }
}
