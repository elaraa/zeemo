﻿using System;

namespace Services.Mobile.Business.Dto
{
   public class QRDto
    {
        public string ConsumerCode { get; set; }
        public string OfferCode { get; set; }
        public DateTime CurrentDate { get; set; }
    }
}
