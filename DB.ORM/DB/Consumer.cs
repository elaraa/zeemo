namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Consumer")]
    public partial class Consumer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Consumer()
        {
            ConsumerRedeemedPoints = new HashSet<ConsumerRedeemedPoint>();
            ConsumerReportPromotionRuns = new HashSet<ConsumerReportPromotionRun>();
        }

        public int ConsumerId { get; set; }

        [StringLength(50)]
        public string ConsumerCode { get; set; }

        [Required]
        public string ConsumerName { get; set; }

        [Required]
        public string Phone { get; set; }

        [Column(TypeName = "date")]
        public DateTime BirthDate { get; set; }

        [Required]
        [StringLength(1)]
        public string Gender { get; set; }

        public int? ReferralId { get; set; }

        public string Photo { get; set; }

        [Required]
        [StringLength(5)]
        public string Language { get; set; }

        public bool ShowNotifications { get; set; }

        [StringLength(50)]
        public string OTP { get; set; }

        public bool PendingOTP { get; set; }

        public bool IsActive { get; set; }

        public DateTime? LoginDate { get; set; }

        public string FCToken { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerRedeemedPoint> ConsumerRedeemedPoints { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsumerReportPromotionRun> ConsumerReportPromotionRuns { get; set; }
    }
}
