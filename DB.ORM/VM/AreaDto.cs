﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(AreaMetaDataDto))]
    public partial class Area 
    {
        class AreaMetaDataDto
        {
            [Required(ErrorMessageResourceName = "RequiredAreaName", ErrorMessageResourceType = typeof(ZM.AreaAdmin))]
            public string AreaName { get; set; }
        }
    }
}
