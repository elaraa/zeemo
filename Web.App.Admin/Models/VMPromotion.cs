﻿using DB.ORM.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.App.Admin.Models
{
    public class VMPromotion
    {
        public int PromotionId { get; set; }
        public string PromotionName { get; set; }
        public string OfferTypeName { get; set; }
        public string MerchantName { get; set; }
        public List<PromotionImage> Images { get; set; }
    }
}