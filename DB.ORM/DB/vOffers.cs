namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vOffers
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionRunId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string PromotionName { get; set; }

        [Key]
        [Column(Order = 3)]
        public string PromotionTypeName { get; set; }

        [StringLength(500)]
        public string ImageUrl { get; set; }

        [Key]
        [Column(Order = 4)]
        public string BrandName { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MerchantId { get; set; }

        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InterestId { get; set; }

        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PromotionTypeId { get; set; }

        public DateTime? EndDate { get; set; }

        public int? NumberOfPieces { get; set; }

        public decimal? Savings { get; set; }
    }
}
