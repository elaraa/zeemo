namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_Merchant
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MerchantId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string MerchantName { get; set; }

        [Key]
        [Column(Order = 2)]
        public string MerchantBrandName { get; set; }

        public string Website { get; set; }

        public string Facebook { get; set; }

        [Key]
        [Column(Order = 3)]
        public string InterestName { get; set; }

        public string Logo { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InterestId { get; set; }
    }
}
