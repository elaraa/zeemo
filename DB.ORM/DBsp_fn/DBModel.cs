﻿using DB.ORM.DBsp_fn.Dto;
using DB.ORM.Dto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DB.ORM.DB
{
    public partial class DBModel
    {
        public IQueryable<SystemMenu> fn_GetAccessMenu(int userId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<SystemMenu>("select * from [fn_GetAccessMenu](@UserId)",
                new SqlParameter("@UserId", userId)).AsQueryable();
        }
        public bool fn_CanAccessController(int userId, string controllerName)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<int>("select * from [fn_CanAccessController]( @UserId, @Controller)",
                new SqlParameter("@UserId", userId), new SqlParameter("@Controller", controllerName)).FirstOrDefault() > 0;
        }
        public bool fn_InGeoArea(string lat1, string lng1, string lat2, string lng2)
        { 
            Database.CommandTimeout = 0;
            return Database.SqlQuery<bool>("select [dbo].[fn_InGeoArea](@Lat1,@Lng1,@Lat2,@Lng2)",
                new SqlParameter("@Lat1", lat1), new SqlParameter("@Lng1", lng1), new SqlParameter("@Lat2", lat2)
                , new SqlParameter("@Lng2", lng2)).FirstOrDefault();
        }

        public IQueryable<CampaignBranchDto> fn_GetSelectedBranches(int PromotionRunId, int merchantId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<CampaignBranchDto>("select * from [fn_GetSelectedBranches](@PromotionRunId, @MerchantId)"
               , new SqlParameter("@PromotionRunId", PromotionRunId)
               , new SqlParameter("@MerchantId", merchantId)).AsQueryable();
        }
        public IQueryable<MerchantDto> fn_GetMerchants(int consumerId)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<MerchantDto>("select * from [fn_GetMerchants](@ConsumerId)"
               , new SqlParameter("@ConsumerId", consumerId)).AsQueryable();
        }
        public IQueryable<LastOfferToRateDto> fn_GetLastOfferToRate(int consumerId)
        { 
            Database.CommandTimeout = 0;
            return Database.SqlQuery<LastOfferToRateDto>("select * from [fn_GetLastOfferToRate](@ConsumerId)"
               , new SqlParameter("@ConsumerId", consumerId)).AsQueryable();
        }

        public IQueryable<OffersDto> fn_GetOffers(int consumerId, string lat, string lng)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<OffersDto>("select * from [fn_GetOffers](@ConsumerId, @Lat, @Lng)"
               , new SqlParameter("@ConsumerId", consumerId)
               , new SqlParameter("@Lat", (object) lat ?? DBNull.Value)
               , new SqlParameter("@Lng", (object) lng ?? DBNull.Value)).AsQueryable();
        }
        
        public IQueryable<MerchantPointMapDto> fn_GetMerchantPoints(int merchantId, int points)
        {
            Database.CommandTimeout = 0;
            return Database.SqlQuery<MerchantPointMapDto>("select * from [dbo].fn_GetMerchantPoints(@merchantId,@points)"
               , new SqlParameter("@merchantId", merchantId)
               , new SqlParameter("@points", points)).AsQueryable();
        }
    }
}
