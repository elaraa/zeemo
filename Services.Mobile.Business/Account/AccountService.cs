﻿using DB.ORM.DB;
using Services.Base;
using Services.Mobile.Business.Dto;
using System;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using Services.Security;
using static Services.Helper.EnumHelpers;
using Newtonsoft.Json;
using System.Net;
using Services.Mobile.Business.Account.Dto;

namespace Services.Mobile.Business
{
    public class AccountService : BaseService<Consumer>
    {
        private int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }
        private int GenerateRandomCQ() 
        {
            int _min = 10000;
            int _max = 99999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }
        private string getCQ() 
        { 
            var CQ = "";
            while (true)
            {
                CQ = GenerateRandomCQ().ToString();
                if (!Any<Consumer>(c => c.ConsumerCode == CQ))
                    break;
            }
            return CQ;
        }
        private string getNewOTP()
        {
            var otp = "";
            while (true)
            {
                otp = GenerateRandomNo().ToString();
                if (!Any<Consumer>(c => c.OTP == otp))
                    break;
            }
            return otp;
        }

        private string getConsumerCode(int consumerId)
        {
            try
            {
                return StringCipher.Encrypt(JsonConvert.SerializeObject(new RequestResult
                   {
                       Data = new QRDto
                       {
                           ConsumerCode = consumerId.ToString()
                       },
                       Failed = false
                   }), CipherCode.QRCodePassword);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }
        }

        public RequestResult register(ConsumerRegistration registration)
        {
            var otp = "";
            var Cq = ""; 
            try
            {
                if (Any<Consumer>(w => w.Phone.Trim() == registration.Phone.Trim()))
                    return new RequestResult { Failed = true, ErrorCode = "AccountExists" };

                var referral = GetOne(w => w.ConsumerCode.ToLower().Trim() == registration.ReferralCode.ToLower().Trim());

                otp = getNewOTP();
                Cq = getCQ();
                Add(new Consumer
                {
                    Phone = registration.Phone,
                    ConsumerName = registration.Name,
                    ReferralId = referral != null ? referral.ConsumerId : 0,
                    BirthDate = registration.Birtdate,
                    Gender = registration.Gender == "M" ? "M" : "F",
                    OTP = otp,
                    IsActive = false,
                    PendingOTP = true,
                    ShowNotifications = true,
                    Language = registration.Language ?? "en",
                    ConsumerCode = Cq
                });
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, ErrorCode = "DBError" };
            }
            return new RequestResult { Failed = false, ErrorCode = "", Data = otp };
        }

        public RequestResult login(ConsumerContract<string> phone)
        {
            try
            {
                var consumer = GetOne(w => w.Phone.Trim() == phone.wcObject.Trim());
                if (consumer == null)
                    return new RequestResult { Failed = true, ErrorCode = "InvalidPhone" };

                consumer.OTP = getNewOTP();
                consumer.PendingOTP = true;
                AddOrUpdate(consumer);
                return new RequestResult { Failed = false, ErrorCode = "", Data = consumer.OTP };
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, ErrorCode = "DBError" };
            }
        }

        public RequestResult logout(string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                    return new RequestResult { Failed = false };
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = userToken };
            }
        }

        public RequestResult updateFCToken(ConsumerContract<string> fctoken, string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);
                var consumer = GetOne(w => w.ConsumerId == consumerId);

                if (consumer != null)
                {
                    consumer.FCToken = fctoken.wcObject;
                    AddOrUpdate(consumer);
                    return new RequestResult { Failed = false };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = userToken };
            }
        }

        public RequestResult verifyingOTP(ConsumerContract<string> otp)
        {
            try
            {
                var consumer = GetOne(w => (w.IsActive == false || w.LoginDate != null) && w.OTP == otp.wcObject && w.PendingOTP == true);
                if (consumer == null)
                    return new RequestResult { Failed = true, ErrorCode = "WrongOTP" };
                else
                {
                    string consumerToken = "", input = string.Format("{0}", consumer.ConsumerId);
                    consumerToken = StringCipher.Encrypt(consumer.ConsumerId.ToString(), CipherCode.UserTokenPassword);
                    consumer.PendingOTP = false;
                    consumer.IsActive = true;
                    consumer.LoginDate = DateTime.Now;
                    AddOrUpdate(consumer);
                    return new OTPRequestResult { Failed = false, ErrorCode = "", Data = consumer, token = consumerToken };
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, ErrorCode = "DBError" };
            }
        }

        //public string getRedeemedOffers(ListParams @params, string userToken)
        //{
        //    int consumerId = -1;
        //    try
        //    {
        //        consumerId = getTokenId(userToken);

        //        if (Any<Consumer>(w => w.ConsumerId == consumerId))
        //        {
        //            var totalCount = db.vOffers.Count();
        //            return JsonConvert.SerializeObject(new ListResult<vOffer>
        //            {
        //                page = @params.@params.page,
        //                pageCount = totalCount / @params.@params.limit,
        //                totalCount = totalCount,
        //                data = db.vOffers.OrderBy(o => o.promotionId).Skip((@params.@params.page - 1) * @params.@params.limit).Take(@params.@params.limit).ToList(),
        //                limit = @params.@params.limit
        //            });
        //        }
        //        else
        //            return JsonConvert.SerializeObject(new RequestResult { Failed = true, ErrorCode = "InvalidRequest" });

        //    }
        //    catch (Exception e)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(e);
        //        return JsonConvert.SerializeObject((new RequestResult { Failed = true, Data = userToken }));
        //    }

        //}

        public ListResult<vOfferDto> getRedeemedOffers(ListParams @params, string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var totalCount = db.vOffers.Count();
                    var lang = getLanguageByConsumer(consumerId);
                    return new ListResult<vOfferDto>
                    {
                        page = @params.@params.page,
                        pageCount = totalCount / @params.@params.limit,
                        totalCount = totalCount,
                        data = Get<vOffer>().OrderBy(o => o.PromotionId)
                                 .Select(a => new vOfferDto
                                 {
                                     OfferId = a.PromotionId,
                                     BrandName = a.BrandName,
                                     Image = a.ImageUrl,
                                     NoOfPieces = a.NumberOfPieces,
                                     OfferName = lang == "en" ? a.PromotionName : a.PromotionNameAr,
                                     OfferTypeName = a.PromotionTypeName,
                                     Duration = a.Duration
                                 }).Skip((@params.@params.page - 1) * @params.@params.limit).Take(@params.@params.limit),
                        limit = @params.@params.limit
                    };
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }

        }

        public RequestResult updateNotifcation(ConsumerStructContract<bool> enableNotification, string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);
                var consumer = GetOne(w => w.ConsumerId == consumerId);

                if (consumer != null)
                {
                    consumer.ShowNotifications =enableNotification.wcObject;
                    AddOrUpdate(consumer);
                    consumer.ConsumerCode = getConsumerCode(consumer.ConsumerId);
                    return new RequestResult { Failed = false , Data= consumer};
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = userToken };
            }
        }

        public RequestResult updateLanguage(ConsumerContract<string> Language, string userToken)
        { 
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);
                var consumer = GetOne(w => w.ConsumerId == consumerId);

                if (consumer != null)
                {
                    consumer.Language = Language.wcObject == "ar" ? "ar" : "en";
                    AddOrUpdate(consumer);
                    consumer.ConsumerCode = getConsumerCode(consumer.ConsumerId);
                    return new RequestResult { Failed = false, ErrorCode = "", Data = consumer };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = userToken };
            }
        }

        public RequestResult getMyReferral(ReferralDto referralDto, string userToken)
        {
            int consumerId = -1;
            try 
            {
                consumerId = getTokenId(userToken);
                if (consumerId > 0)
                {
                    var consumer = GetOne(a => a.ConsumerId == consumerId);
                    var referalCode = consumer != null ?  consumer.ConsumerCode : "";
                    var frinds = Get(a => a.ReferralId == consumerId).Count();
                    var consumerLoyaltyPoints = Get<ConsumerLoyaltyPoint>(a => a.ConsumerId == consumerId && a.ExpiryDate >= DateTime.Now)
                        .Select(c=>c.Remain);
                    var consumerPoints = consumerLoyaltyPoints.Any() ? consumerLoyaltyPoints.Sum(c=> c) : 0;

                    return new RequestResult { Failed = false, Data = new ReferralDto
                     {
                        ReferralCode = referalCode,
                        Friends = frinds,
                        Points = consumerPoints,
                        ShareLink= "Share Zeemo with your friends on <a href=\"http://zeemo.mazanocorp.com/C=" 
                         + referalCode + "\"></a>"
                      }
                    };
                }
                else
                    return new RequestResult { Failed = true, ErrorCode = "InvalidRequest" };

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return new RequestResult { Failed = true, Data = userToken };
            }
        }

        public RequestResult updatePicture(ConsumerContract<string> photo, string userToken)
        {
            int consumerId = -1;
            try
            {
                consumerId = getTokenId(userToken);
                
                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var p = Convert.FromBase64String(photo.wcObject);
                    var ms = new MemoryStream(p.Select(s => (byte)s).ToArray());
                    var img = Image.FromStream(ms);
                    var essentialPath = "Uploads/" + "Consumer" + "/";
                    var rootFolder = HttpContext.Current.Server.MapPath("~/"+essentialPath);
                
                    if (!Directory.Exists(rootFolder))
                        Directory.CreateDirectory(rootFolder);

                    var imgName = essentialPath + string.Format("{0}_{1}.png", consumerId, DateTime.Now.Millisecond/*Guid.NewGuid().ToString().Replace("-","")*/);

                    

                    var consumer = GetOne(w => w.ConsumerId == consumerId);
                    if (consumer.Photo!=null)
                    {
                        var oldPath = HttpContext.Current.Server.MapPath(consumer.Photo);
                        File.Delete(oldPath);
                        //img.Save(HttpContext.Current.Server.MapPath(imgName));
                    }
                    ///TODO: delete old photo
                    img.Save(HttpContext.Current.Server.MapPath("~/" + imgName));
                    consumer.Photo = imgName;
                    AddOrUpdate(consumer);


                    return new RequestResult { Failed = false, ErrorCode = "", Data = consumer };

                }
                return new RequestResult { Failed = true, ErrorCode = "DBError" };

            }
            catch (Exception ex)
            {
                return new RequestResult { Failed = true, ErrorCode = "DBError", Data = ex };
            }
        }

        public ListResult<vOfferDto> getWishList(ListParams @params, string userToken)
        {
            int consumerId = -1;
            try
            { 
                consumerId = getTokenId(userToken);

                if (Any<Consumer>(w => w.ConsumerId == consumerId))
                {
                    var wishList = Get<ConsumerWishList>(w => w.ConsumerId == consumerId);
                 
                    var lang = getLanguageByConsumer(consumerId);
                    var totalCount = wishList.Count() > 0 ? wishList.Count() : 0;
                    var wishListPromotions = totalCount > 0 ? wishList.Select(s => s.PromotionId).Distinct() : null;
                    return new ListResult<vOfferDto>
                    {
                        page = @params.@params.page,
                        pageCount = totalCount > 0 ? totalCount / @params.@params.limit : 0,
                        totalCount = totalCount,
                        data = totalCount > 0 ? Get<vOffer>(w =>
                                         wishListPromotions.Contains(w.PromotionId) == true).OrderBy(o => o.PromotionId)
                                         .Select(a => new vOfferDto
                                         {
                                             OfferId = a.PromotionRunId,
                                             BrandName =a.BrandName,
                                             Image = a.ImageUrl,
                                             NoOfPieces = a.NumberOfPieces,
                                             OfferName = lang == "en" ? a.PromotionName : a.PromotionNameAr,
                                             OfferTypeName = a.PromotionTypeName,
                                             Duration=a.Duration
                                         }).Skip((@params.@params.page - 1) * @params.@params.limit).Take(@params.@params.limit) : null,
                        limit = @params.@params.limit
                    };
                }
                else
                    return null;

            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return null;
            }

        }
    }
}