﻿using DB.ORM.DB;
using Services.Base;
using Services.Mobile.Business;
using Services.Mobile.Business.Notification;
using System;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using static Services.Helper.EnumHelpers;

namespace Mobile.APIsConsumer
{
    public class NotificationController : ApiController
    {
        NotificationService notification = new NotificationService();
        [HttpPost]
        public IHttpActionResult GetList([FromBody] ListParams @params)
        {
            //using (var db = new DBModel())
            //{
            //    var totalCount = db.vConsumerNotifications.Count();
            //    
            //    return Json(new ListResult<vConsumerNotification>
            //    {
            //        dataHeader = Request.Headers,
            //        page = @params.@params.page,
            //        pageCount = totalCount / @params.@params.limit,
            //        totalCount = totalCount,
            //        data = db.vConsumerNotifications.OrderBy(o => o.CreationDate).Skip((@params.@params.page - 1) * @params.@params.limit).Take(@params.@params.limit).ToList(),
            //        limit = @params.@params.limit
            //    });
            //}
            var userToken = Request.Headers.Authorization.Parameter;
            var header = Request.Headers;
            if (ModelState.IsValid)
                return Json(notification.GetConsumerNotifications(@params, header, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }
        [HttpPost]
        public IHttpActionResult GetUnreadCount()
        {
            //using (var db = new DBModel())
            //{
            //    return Json(
            //        new RequestResult
            //        {
            //            Failed = false,
            //            Data = /*Convert.ToInt32( new Random(DateTime.Now.Millisecond).NextDouble())*/db.vConsumerNotifications.Where(w => w.IsRead == false).Count()
            //        });
            //}
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(new RequestResult { Failed=false,Data=notification.GetCount(userToken) });

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }

    }
}
