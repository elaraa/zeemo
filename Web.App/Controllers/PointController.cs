﻿using DB.ORM.DB;
using Kendo.Mvc.UI;
using Services.Web.Business.Dto;
using Services.Web.Business.PointSettings;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class PointController : BaseControllerTemplate<vw_MerchantPoint, MerchantPoint, int>
    {
        PointService pointService;
        public PointController()
        {
            ViewNamePlural = ZM.PointSettings.ViewNamePlural; 
            ViewNameSingular = ZM.PointSettings.ViewNameSingular;
            pointService = new PointService();
            PropertiesToUpdate = new Expression<Func<MerchantPoint, object>>[]
                {
                    x=>x.PointValue,
                    x=>x.PointDescription,
                    x=>x.PointDescriptionAr
            };
            
        }
       protected override Expression<Func<MerchantPoint, bool>> GetOneExpression(int? id) =>
           id.HasValue ? (x => x.PointId == id.Value) : (Expression<Func<MerchantPoint, bool>>)(x => 1 == 0);
        protected override void FillViewBag(bool isCreate)
        {
            ViewBag.loyality= pointService.GetLoyality(UserId);
            base.FillViewBag(isCreate);
        }
        public override ActionResult Index()
        {
            FillViewBag(true);
            return View();
        }

        [HttpPost]
        public ActionResult SaveMerchantPoint(vw_MerchantPoint model)
        {
            FillViewBag(false);

            model.MerchantId = UserId;
            return model.PointId == 0 ?
                base.CreateChildEntity<MerchantPoint>(AutoMapper.Mapper.Map< MerchantPoint>(model), "Index", null, "Index", ActionService):
                    base.EditChildEntity<MerchantPoint>(AutoMapper.Mapper.Map<MerchantPoint>(model), PropertiesToUpdate, "Index", null, "Index", ActionService);
        }

        [HttpPost]
        public ActionResult DeleteMerchantPoint([DataSourceRequest] DataSourceRequest request, vw_MerchantPoint model)
        {
            if (model.PointId == 0)
                return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));

            return Json(ActionService.Delete(GetOneExpression(model.PointId)) > 0);
        }
    }
}