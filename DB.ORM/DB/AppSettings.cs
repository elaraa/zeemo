namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AppSettings
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AppSettingId { get; set; }

        [Required]
        public string AppSettingCode { get; set; }

        public string AppSettingValue { get; set; }

        public string AppSettingDefaultValue { get; set; }
    }
}
