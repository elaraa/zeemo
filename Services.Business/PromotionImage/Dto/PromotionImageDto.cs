﻿using Abp.Authorization.Users;
using Abp.AutoMapper;
using DB.ORM.DB;
using System.ComponentModel.DataAnnotations;

namespace Services.Web.Business.Dto
{
    [AutoMapTo(typeof(PromotionImage))]
    public class PromotionImageDto
    {
        public int PromotionImageId { get; set; }
        public int PromotionId { get; set; }
        public string ImageUrl { get; set; }
        public bool IsLogo { get; set; }
        public int SortOrder { get; set; }
    }
}
