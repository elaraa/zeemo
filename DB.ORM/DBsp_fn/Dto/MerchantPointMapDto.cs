﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DBsp_fn.Dto
{
    public class MerchantPointMapDto
    {
        public long PointOrder { get; set; }
        public int PointValue { get; set; }
        public string PointDescription { get; set; }
        public string PointDescriptionAr { get; set; }
        public Boolean IsChecked { get; set; }
    }
}
