﻿using Services.Base;
using DB.ORM.DB;
using Services.Mobile.Business.Dto;
using System;
using Newtonsoft.Json;
using Services.Security;
using static Services.Helper.EnumHelpers;
using System.Linq;
using System.Collections.Generic;
using Services.Mobile.Business.Offer;

namespace Services.Mobile.Business.CashierMerchantOffer
{
    public class MerchantOfferService : BaseService<vOffer>
    {
        public ListResult<viewOfferDto> getList(ListParams @params,string userToken , object Header )
        {
            try
            {
                MerchantCashierDto merchantCashierdata = JsonConvert.DeserializeObject<MerchantCashierDto>(getMerchantCashierTokenId(userToken));
                if (verifayMerchantCashier(merchantCashierdata))
                {
                    int cashierid = int.Parse(merchantCashierdata.CashierID);
                    int merchantid = int.Parse(merchantCashierdata.MerchantID);
                    var promotionid = GetOne<Promotion>(a => a.MerchantId == merchantid).PromotionId;//check condition
                    var lang = getCashierLanguage(cashierid);
                    var totalCount = Get().Count();
                    return new ListResult<viewOfferDto>
                    {
                        dataHeader = Header,
                        page = @params.@params.page,
                        pageCount = totalCount / @params.@params.limit,
                        totalCount = totalCount,
                        data = Get(a=>a.PromotionId==promotionid).OrderBy(o => o.PromotionId).Select(c => new viewOfferDto
                        {
                            BrandName = c.BrandName,
                            Duration = c.Duration,
                            Image = c.ImageUrl,
                            NoOfPieces = c.NumberOfPieces,
                            OfferId = c.PromotionId,
                            OfferName = lang == "en" ? c.PromotionName : c.PromotionNameAr,
                            OfferTypeName = c.PromotionTypeName
                        }).Skip((@params.@params.page - 1) * @params.@params.limit).Take(@params.@params.limit),
                        limit = @params.@params.limit
                    };
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);                return null;
            }



        }

        public bool verifayMerchantCashier(MerchantCashierDto data)
        {
            int merchantid = int.Parse(data.MerchantID);
            int Casheirid = int.Parse(data.CashierID); 
            if (Any<Merchant>(a => a.MerchantId == merchantid) && Any<MerchantBranchCashier>(a => a.MerchantBranchCashierId == Casheirid))
                return true;
            else
                return false;
        }
    }
}
