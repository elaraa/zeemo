﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.ORM.DB
{
    [MetadataType(typeof(FAQQuestionMetaDataDto))]
    public partial class FAQQuestion 
    {
        class FAQQuestionMetaDataDto
        {
            [Required(ErrorMessageResourceName = "RequiredQuestionText", ErrorMessageResourceType = typeof(ZM.FAQAdmin))]
            public string QuestionText { get; set; }
        }
    }
}
