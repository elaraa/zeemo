﻿CREATE VIEW [dbo].[vConsumerNotification]
	AS 
	SELECT cn.IsRead, cn.NotificationMessage, cn.NotificationId, isnull(m.MerchantLogo ,'') NotificationImage, m.MerchantBrandName
	, cn.CreationDate
	from ConsumerNotification cn 
		left join Merchant m on cn.MerchantId = m.MerchantId