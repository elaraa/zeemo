﻿using Microsoft.Build.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Mobile.Business.Dto
{

    public class MerchantAccountDto
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MerchantId { get; set; }

        [Required]
        public string MerchantName { get; set; }

        [Required]
        public string MerchantBrandName { get; set; }

        public string MerchantLogo { get; set; }

        public int InterestId { get; set; }
    }
}
