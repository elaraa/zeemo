﻿using AutoMapper;
using DB.ORM.DB;
using DB.ORM.Dto;
using Services.Base;
using Services.Web.Business.Branch.Dto;
using Services.Web.Business.Campaign.Dto;
using Services.Web.Business.Dto;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Web.App.Models;
using CampaignBranchDto = Services.Web.Business.Campaign.Dto.CampaignBranchDto;
using Profile = DB.ORM.DB.Profile;

namespace Web.App
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            try { 
                AreaRegistration.RegisterAllAreas();
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);
                //SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
                //Task.Factory.StartNew(() => { ScheduleJobService.Run(); });
                
                InitMapper();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                HttpContext httpContext = ((MvcApplication)sender).Context;
                string currentController = " ";
                string currentAction = " ";
                RouteData currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

                if (currentRouteData != null)
                {
                    if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                        currentController = currentRouteData.Values["controller"].ToString();

                    if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                        currentAction = currentRouteData.Values["action"].ToString();
                }

                Exception ex = Server.GetLastError();
                //var controller = new ErrorController();
                var routeData = new RouteData();
                string action = "GenericError";

                if (ex is HttpException)
                {
                    var httpEx = ex as HttpException;

                    switch (httpEx.GetHttpCode())
                    {
                        case 404: action = "NotFound"; break;
                            // others if any
                    }
                }

                httpContext.ClearError();
                httpContext.Response.Clear();
                httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
                httpContext.Response.TrySkipIisCustomErrors = true;

                routeData.Values["controller"] = "App";
                routeData.Values["action"] = action;
                routeData.Values["exception"] = new HandleErrorInfo(ex, currentController, currentAction);

                IController errormanagerController = new Controllers.AppController();
                var wrapper = new HttpContextWrapper(httpContext);
                var rc = new RequestContext(wrapper, routeData);
                errormanagerController.Execute(rc);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        protected void Application_BeginRequest()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
        }

        private void InitMapper() {
            var baseUrl = new BaseService<SystemUser>().GetBaseUrl;

            Mapper.Initialize(cfg => {
                cfg.CreateMap<PromotionRun, VMCampaign>();
                cfg.CreateMap<VMCampaign, PromotionRun>();
                cfg.CreateMap<PromotionRun, CampaignDto>();
                cfg.CreateMap<CampaignDto, PromotionRun>();
                cfg.CreateMap<VMCampaignCriteria, PromotionRunCriteria>();
                cfg.CreateMap<PromotionRunCriteria, VMCampaignCriteria>();
                cfg.CreateMap<CampaignCriteriaDto, PromotionRunCriteria>();
                cfg.CreateMap<PromotionRunCriteria, CampaignCriteriaDto>();
                cfg.CreateMap<VMCampaignBranch, PromotionRunBranch>();
                cfg.CreateMap<PromotionRunBranch, VMCampaignBranch>();
                cfg.CreateMap<CampaignBranchDto, VMCampaignBranch>();
                cfg.CreateMap<VMCampaignBranch, CampaignBranchDto>();
                cfg.CreateMap<DB.ORM.Dto.CampaignBranchDto, CampaignBranchDto>();
                cfg.CreateMap<PromotionImage, PromotionImageDto>();
                cfg.CreateMap<PromotionImageDto, PromotionImage>();
                cfg.CreateMap<vw_MerchantPoint, MerchantPoint>();
                cfg.CreateMap<MerchantPoint, vw_MerchantPoint>();
                cfg.CreateMap<MerchantBranchCashier, BranchCashierDto>();

                cfg.CreateMap<Profile, Merchant>().ForMember(d => d.MerchantId, s => s.MapFrom(src => src.Id));
                cfg.CreateMap<Merchant, Profile>()
                     .ForMember(d => d.Id, s => s.MapFrom(src => src.MerchantId))
                     .ForMember(d => d.Pic, s => s.MapFrom(src => baseUrl + src.MerchantLogo))
                     .ForMember(d => d.FullName, s => s.MapFrom(src => string.Format("{0} - {1}", src.MerchantName, src.MerchantBrandName)));
            });
        }
    }
}
