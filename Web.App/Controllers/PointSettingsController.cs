﻿using DB.ORM.DB;
using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Web.App.Controllers
{
    public class PointSettingsController : BaseControllerTemplate<vw_MerchantPointSetting, Merchant, int>
    {
        public PointSettingsController()
        {
            ViewNamePlural = ZM.PointSettings.ViewNamePlural;
            ViewNameSingular = ZM.PointSettings.ViewNameSingular;
            PropertiesToUpdate = new Expression<Func<Merchant, object>>[]
                {
                    x=>x.PointsActivationDuration,
                    x=>x.PointsExpirationDuration,
                    x=>x.PointsAmount,
                    x=>x.Pounds
                    
                };
            
        }
        protected override Expression<Func<Merchant, bool>> GetOneExpression(int? id) =>
            id.HasValue ? (x => x.MerchantId == id.Value) : (Expression<Func<Merchant, bool>>)(x => 1 == 0);


      
        public override ActionResult Index()
        {
            //var model = GetOneExpression(UserId);
            var model = ActionService.GetOne(GetOneExpression(UserId));
            return View("Index", model);
        }

        [HttpPost]
        public override ActionResult Edit(Merchant model)
        {
            // service
            return EditChildEntity(model, PropertiesToUpdate, "Index", null, "Edit", ActionService); 
        }
      

    }
}