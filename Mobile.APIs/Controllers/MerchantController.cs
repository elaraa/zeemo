﻿using Services.Mobile.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using static Services.Helper.EnumHelpers;

namespace Mobile.APIsConsumer
{
    public class MerchantController : ApiController
    {
        MerchantService merchantService = new MerchantService();
        [HttpPost]
        public IHttpActionResult GetList([FromBody] ListParams @params)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(merchantService.getMechants(@params, Request.Headers, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }

        [HttpPost]
        public IHttpActionResult GetMerchant([FromBody]ConsumerStructContract<int> id)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(merchantService.getMechant(id.wcObject, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }

        [HttpPost]
        public IHttpActionResult GetMerchantPointsMap([FromBody]ConsumerStructContract<int> id)
        {
            
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(merchantService.getMerchantPointsMap(id.wcObject, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }

        [HttpPost]
        public IHttpActionResult FollowMerchant([FromBody]ConsumerStructContract<int> merchantId)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(merchantService.followMerchant(merchantId.wcObject, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }

        [HttpPost]
        public IHttpActionResult GetMerchantBranches([FromBody]ConsumerStructContract<int> merchantId)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(merchantService.getMerchantBranches(merchantId.wcObject, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }

        [HttpPost]
        public IHttpActionResult GetMerchantByInterest([FromBody]ConsumerStructContract<int> interestId)
        {
            var userToken = Request.Headers.Authorization.Parameter;
            if (ModelState.IsValid)
                return Json(merchantService.getMerchantsByInterest(interestId.wcObject, userToken));

            return Json(new RequestResult { Failed = true, ErrorCode = ReturnErrorCode.DataInComplete.DescriptionAttr() });
        }

    }
}
