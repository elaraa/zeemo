namespace DB.ORM.DB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_User
    {
        public string Logo { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        public string UserName { get; set; }

        [Key]
        [Column(Order = 2)]
        public string LoginId { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public bool? IsAdmin { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool Active { get; set; }

        [Key]
        [Column(Order = 5)]
        public DateTime LastUpdate { get; set; }

        public int? UpdatedBy { get; set; }

        public string UpdatedByLogonName { get; set; }
    }
}
